USE [master]
GO
/****** Object:  Database [eCars]    Script Date: 9/21/2014 3:24:55 PM ******/
CREATE DATABASE [eCars]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'eCars', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\eCars.mdf' , SIZE = 3264KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'eCars_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\eCars_log.ldf' , SIZE = 1088KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [eCars] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [eCars].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [eCars] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [eCars] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [eCars] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [eCars] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [eCars] SET ARITHABORT OFF 
GO
ALTER DATABASE [eCars] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [eCars] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [eCars] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [eCars] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [eCars] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [eCars] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [eCars] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [eCars] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [eCars] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [eCars] SET  DISABLE_BROKER 
GO
ALTER DATABASE [eCars] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [eCars] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [eCars] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [eCars] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [eCars] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [eCars] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [eCars] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [eCars] SET RECOVERY FULL 
GO
ALTER DATABASE [eCars] SET  MULTI_USER 
GO
ALTER DATABASE [eCars] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [eCars] SET DB_CHAINING OFF 
GO
ALTER DATABASE [eCars] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [eCars] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [eCars] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'eCars', N'ON'
GO
USE [eCars]
GO
/****** Object:  Schema [App]    Script Date: 9/21/2014 3:24:55 PM ******/
CREATE SCHEMA [App]
GO
/****** Object:  Schema [Config]    Script Date: 9/21/2014 3:24:55 PM ******/
CREATE SCHEMA [Config]
GO
/****** Object:  Schema [User]    Script Date: 9/21/2014 3:24:55 PM ******/
CREATE SCHEMA [User]
GO
/****** Object:  Schema [Vehicle]    Script Date: 9/21/2014 3:24:55 PM ******/
CREATE SCHEMA [Vehicle]
GO
/****** Object:  UserDefinedTableType [Vehicle].[VehicleIdTableType]    Script Date: 9/21/2014 3:24:55 PM ******/
CREATE TYPE [Vehicle].[VehicleIdTableType] AS TABLE(
	[Id] [int] NULL
)
GO
/****** Object:  Table [App].[Customer]    Script Date: 9/21/2014 3:24:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [App].[Customer](
	[user_id_performance] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [uniqueidentifier] NOT NULL,
	[CompanyName] [varchar](120) NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Lastname] [nvarchar](80) NOT NULL,
	[Identification] [varchar](16) NULL,
	[Birthday] [date] NULL,
	[Phone] [bigint] NULL,
	[Mobile] [bigint] NULL,
	[Email] [varchar](60) NOT NULL,
	[isDealer] [bit] NOT NULL,
	[Address] [varchar](120) NULL,
	[CreatedDate] [datetime] NOT NULL CONSTRAINT [DF_AppCustomer_Created]  DEFAULT (getdate()),
	[ModifiedDate] [datetime] NOT NULL CONSTRAINT [DF_AppCustomer_ModifiedDate]  DEFAULT (getdate()),
	[ModifiedBy] [smallint] NULL,
	[Username] [varchar](62) NOT NULL DEFAULT (' '),
	[PasswordHash] [varchar](max) NOT NULL DEFAULT (' '),
	[SecurityStamp] [varchar](max) NOT NULL,
	[ConfirmationToken] [varchar](200) NULL,
	[IsConfirmed] [bit] NOT NULL DEFAULT ((0)),
	[PlanId] [smallint] NULL,
PRIMARY KEY CLUSTERED 
(
	[user_id_performance] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Config].[Role]    Script Date: 9/21/2014 3:24:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Config].[Role](
	[RoleID] [smallint] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](60) NOT NULL,
	[Description] [varchar](200) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[ModifiedBy] [smallint] NOT NULL,
	[Active] [bit] NOT NULL,
 CONSTRAINT [PK_Roles] PRIMARY KEY CLUSTERED 
(
	[RoleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Config].[User]    Script Date: 9/21/2014 3:24:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Config].[User](
	[AppUserID] [smallint] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[LastName] [varchar](100) NOT NULL,
	[CreatedDate] [datetime] NOT NULL CONSTRAINT [def_createdDateConfigUsers]  DEFAULT (getdate()),
	[ModifiedDate] [datetime] NOT NULL CONSTRAINT [def_modifiedDateConfigUsers]  DEFAULT (getdate()),
	[ModifiedBy] [smallint] NULL,
	[Active] [bit] NOT NULL,
	[Role] [smallint] NOT NULL,
 CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
(
	[AppUserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [User].[Photo]    Script Date: 9/21/2014 3:24:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [User].[Photo](
	[PhotoID] [int] IDENTITY(1,1) NOT NULL,
	[CustomerID] [int] NOT NULL,
	[PhotoThumbnail] [varchar](60) NOT NULL,
	[ActualPhoto] [varchar](60) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[PhotoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [User].[Preference]    Script Date: 9/21/2014 3:24:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [User].[Preference](
	[UserPreferencesID] [int] IDENTITY(1,1) NOT NULL,
	[CustomerID] [int] NOT NULL,
	[PublicationID] [int] NOT NULL,
	[Createddate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[ModifiedBy] [smallint] NULL,
PRIMARY KEY CLUSTERED 
(
	[UserPreferencesID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [Vehicle].[Color]    Script Date: 9/21/2014 3:24:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Vehicle].[Color](
	[ColorID] [smallint] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](40) NOT NULL,
	[Value] [varchar](12) NOT NULL,
	[Description] [varchar](120) NULL,
	[Active] [bit] NOT NULL CONSTRAINT [DF_VehicleColor_Active]  DEFAULT ((1)),
	[Created] [datetime] NOT NULL CONSTRAINT [DF_VehicleColor_Created]  DEFAULT (getdate()),
	[Modified] [datetime] NOT NULL CONSTRAINT [DF_VehicleColor_Modified]  DEFAULT (getdate()),
	[ModifiedBy] [smallint] NULL,
PRIMARY KEY CLUSTERED 
(
	[ColorID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Vehicle].[Condition]    Script Date: 9/21/2014 3:24:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Vehicle].[Condition](
	[ConditionID] [smallint] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](40) NOT NULL,
	[Value] [decimal](5, 2) NOT NULL,
	[Description] [varchar](120) NULL,
	[Active] [bit] NOT NULL CONSTRAINT [DF_VehicleCondition_Active]  DEFAULT ((1)),
	[Created] [datetime] NOT NULL CONSTRAINT [DF_VehicleCondition_Created]  DEFAULT (getdate()),
	[Modified] [datetime] NOT NULL CONSTRAINT [DF_VehicleCondition_Modified]  DEFAULT (getdate()),
	[ModifiedBy] [smallint] NULL,
PRIMARY KEY CLUSTERED 
(
	[ConditionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Vehicle].[Currency]    Script Date: 9/21/2014 3:24:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Vehicle].[Currency](
	[CurrencyId] [smallint] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](40) NOT NULL,
	[Value] [decimal](5, 2) NOT NULL,
	[Description] [varchar](120) NULL,
	[Created] [datetime] NOT NULL CONSTRAINT [DF_VehicleCurrency_CreatedDate]  DEFAULT (getdate()),
	[Modified] [datetime] NOT NULL CONSTRAINT [DF_VehicleCurrency_ModifiedDate]  DEFAULT (getdate()),
	[Active] [bit] NULL,
	[ModifiedBy] [smallint] NULL,
PRIMARY KEY CLUSTERED 
(
	[CurrencyId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Vehicle].[CustomPlan]    Script Date: 9/21/2014 3:24:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Vehicle].[CustomPlan](
	[CustomPlanId] [int] IDENTITY(1,1) NOT NULL,
	[PlanId] [int] NOT NULL,
	[NotificationModelID] [smallint] NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Price] [decimal](10, 4) NULL,
	[NumberOfImages] [smallint] NOT NULL,
	[SearchRankingMinutes] [smallint] NULL,
	[AdvertisingDurationHours] [smallint] NOT NULL,
	[NumberVehiclesAllowed] [smallint] NOT NULL,
	[SearchRankVehiclesAllowed] [smallint] NOT NULL,
	[Created] [datetime] NOT NULL CONSTRAINT [DF_CustomPlan_Created]  DEFAULT (getdate()),
	[Modified] [datetime] NOT NULL CONSTRAINT [DF_CustomPlan_Modified]  DEFAULT (getdate()),
 CONSTRAINT [PK_CustomPlan] PRIMARY KEY CLUSTERED 
(
	[CustomPlanId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Vehicle].[DriveTrain]    Script Date: 9/21/2014 3:24:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Vehicle].[DriveTrain](
	[DriveTrainID] [smallint] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](40) NOT NULL,
	[Value] [decimal](5, 2) NOT NULL,
	[Description] [varchar](120) NULL,
	[Created] [datetime] NOT NULL CONSTRAINT [DF_VehicleDriveTrain_CreatedDate]  DEFAULT (getdate()),
	[Modified] [datetime] NOT NULL CONSTRAINT [DF_VehicleDriveTrain_ModifiedDate]  DEFAULT (getdate()),
	[Active] [bit] NULL,
	[ModifiedBy] [smallint] NULL,
PRIMARY KEY CLUSTERED 
(
	[DriveTrainID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Vehicle].[Fuel]    Script Date: 9/21/2014 3:24:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Vehicle].[Fuel](
	[FuelID] [smallint] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](40) NOT NULL,
	[Value] [decimal](5, 2) NOT NULL,
	[Description] [varchar](120) NULL,
	[Created] [datetime] NOT NULL CONSTRAINT [DF_VehicleFuel_CreatedDate]  DEFAULT (getdate()),
	[Modified] [datetime] NOT NULL CONSTRAINT [DF_VehicleFuel_ModifiedDate]  DEFAULT (getdate()),
	[Active] [bit] NULL,
	[ModifiedBy] [smallint] NULL,
PRIMARY KEY CLUSTERED 
(
	[FuelID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Vehicle].[Highlights]    Script Date: 9/21/2014 3:24:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Vehicle].[Highlights](
	[HighlightsId] [smallint] IDENTITY(1,1) NOT NULL,
	[vehicleId] [int] NOT NULL,
	[Created] [datetime] NOT NULL CONSTRAINT [DF_VehicleHighlights_Created]  DEFAULT (getdate()),
	[Modified] [datetime] NOT NULL CONSTRAINT [DF_VehicleHighlights_Modified]  DEFAULT (getdate()),
	[Active] [bit] NOT NULL DEFAULT ((0)),
	[ModifiedBy] [smallint] NULL,
PRIMARY KEY CLUSTERED 
(
	[HighlightsId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [Vehicle].[MakeAndModel]    Script Date: 9/21/2014 3:24:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Vehicle].[MakeAndModel](
	[MakeAndModelID] [int] IDENTITY(1,1) NOT NULL,
	[Make] [varchar](15) NOT NULL,
	[Model] [varchar](50) NULL,
	[VehicleTypeID] [smallint] NOT NULL,
	[Year] [smallint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL CONSTRAINT [DF_MakeAndModel_CreatedDate]  DEFAULT (getdate()),
	[ModifiedDate] [datetime] NOT NULL CONSTRAINT [DF_MakeAndModel_ModifiedDate]  DEFAULT (getdate()),
	[ModifiedBy] [smallint] NOT NULL,
	[Active] [bit] NOT NULL CONSTRAINT [DF_MakeAndModel_Active]  DEFAULT ((1)),
	[Description] [varchar](300) NULL,
 CONSTRAINT [PK_MakeAndModel] PRIMARY KEY CLUSTERED 
(
	[MakeAndModelID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Vehicle].[Motor]    Script Date: 9/21/2014 3:24:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Vehicle].[Motor](
	[MotorId] [smallint] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](40) NOT NULL,
	[Value] [decimal](5, 2) NOT NULL,
	[Description] [varchar](120) NULL,
	[CreatedDate] [datetime] NOT NULL CONSTRAINT [DF_VehicleMotor_CreatedDate]  DEFAULT (getdate()),
	[ModifiedDate] [datetime] NOT NULL CONSTRAINT [DF_VehicleMotor_ModifiedDate]  DEFAULT (getdate()),
	[ModifiedBy] [smallint] NULL,
PRIMARY KEY CLUSTERED 
(
	[MotorId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Vehicle].[NotificatioModel]    Script Date: 9/21/2014 3:24:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Vehicle].[NotificatioModel](
	[NotificatioModelID] [smallint] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](40) NOT NULL,
	[Value] [decimal](5, 2) NOT NULL,
	[Description] [varchar](120) NULL,
	[Created] [datetime] NOT NULL CONSTRAINT [DF_VehicleNotificationModel_Created]  DEFAULT (getdate()),
	[Modified] [datetime] NOT NULL CONSTRAINT [DF_VehicleNotificationModel_Modified]  DEFAULT (getdate()),
	[ModifiedBy] [smallint] NULL,
PRIMARY KEY CLUSTERED 
(
	[NotificatioModelID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Vehicle].[Notification]    Script Date: 9/21/2014 3:24:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Vehicle].[Notification](
	[NotificatioID] [int] IDENTITY(1,1) NOT NULL,
	[CustomerID] [int] NOT NULL,
	[PlanID] [smallint] NOT NULL,
	[Sent] [bit] NOT NULL,
	[Description] [varchar](120) NULL,
	[Created] [datetime] NOT NULL,
	[Modified] [datetime] NOT NULL,
	[ModifiedBy] [smallint] NULL,
PRIMARY KEY CLUSTERED 
(
	[NotificatioID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Vehicle].[Photo]    Script Date: 9/21/2014 3:24:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Vehicle].[Photo](
	[PhotoID] [int] IDENTITY(1,1) NOT NULL,
	[VehicleID] [int] NOT NULL,
	[PhotoThumbnail] [varchar](200) NULL,
	[ActualPhoto] [varchar](200) NULL,
	[CreatedDate] [datetime] NOT NULL CONSTRAINT [DF_vehiclePhoto_CreatedDate]  DEFAULT (getdate()),
	[ModifiedDate] [datetime] NOT NULL CONSTRAINT [DF_VehiclePhoto_ModifiedDate]  DEFAULT (getdate()),
	[MainPhoto] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[PhotoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Vehicle].[Plan]    Script Date: 9/21/2014 3:24:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Vehicle].[Plan](
	[PlanID] [int] IDENTITY(1,1) NOT NULL,
	[NotificationModelID] [smallint] NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Price] [decimal](10, 4) NULL,
	[NumberOfImages] [smallint] NOT NULL,
	[SearchRankingMinutes] [smallint] NULL,
	[AdvertisingDurationHours] [smallint] NOT NULL,
	[NumberVehiclesAllowed] [smallint] NOT NULL,
	[SearchRankVehiclesAllowed] [smallint] NOT NULL,
	[Created] [datetime] NOT NULL CONSTRAINT [DF_VehiclePlan_Created]  DEFAULT (getdate()),
	[Modified] [datetime] NOT NULL CONSTRAINT [DF_VehiclePlan_Modified]  DEFAULT (getdate()),
 CONSTRAINT [PK_Plan] PRIMARY KEY CLUSTERED 
(
	[PlanID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Vehicle].[Publication]    Script Date: 9/21/2014 3:24:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Vehicle].[Publication](
	[PublicationID] [int] IDENTITY(1,1) NOT NULL,
	[UserIdPerformance] [int] NOT NULL,
	[VehicleID] [int] NOT NULL,
	[Active] [bit] NOT NULL,
	[Expired] [bit] NOT NULL,
	[Created] [datetime] NOT NULL CONSTRAINT [DF_VehiclePublication_Created]  DEFAULT (getdate()),
	[Modified] [datetime] NOT NULL CONSTRAINT [DF_VehiclePublication_Modified]  DEFAULT (getdate()),
PRIMARY KEY CLUSTERED 
(
	[PublicationID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [Vehicle].[State]    Script Date: 9/21/2014 3:24:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Vehicle].[State](
	[StateId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](80) NOT NULL,
	[Value] [smallint] NULL,
	[Active] [bit] NOT NULL,
	[Created] [datetime] NOT NULL DEFAULT (getdate()),
	[Modified] [datetime] NOT NULL DEFAULT (getdate()),
PRIMARY KEY CLUSTERED 
(
	[StateId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [Vehicle].[Transmission]    Script Date: 9/21/2014 3:24:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Vehicle].[Transmission](
	[TransmissionID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Value] [decimal](5, 2) NOT NULL,
	[Description] [varchar](120) NULL,
	[Created] [datetime] NOT NULL CONSTRAINT [DF_VehicleTransmission_Created]  DEFAULT (getdate()),
	[Modified] [datetime] NOT NULL CONSTRAINT [DF_VehicleTransmission_Modified]  DEFAULT (getdate()),
	[Active] [bit] NOT NULL CONSTRAINT [DF_VehicleTransmission_Active]  DEFAULT ((1)),
 CONSTRAINT [PK_Transmission] PRIMARY KEY CLUSTERED 
(
	[TransmissionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Vehicle].[Type]    Script Date: 9/21/2014 3:24:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Vehicle].[Type](
	[VehicleTypeID] [smallint] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](60) NOT NULL,
	[Description] [varchar](200) NULL,
	[CreatedDate] [datetime] NOT NULL CONSTRAINT [def_Vehicle_Type_CreatedDate]  DEFAULT (getdate()),
	[ModifiedDate] [datetime] NOT NULL CONSTRAINT [def_Vehicle_Type_ModifiedDate]  DEFAULT (getdate()),
	[ModifiedBy] [smallint] NOT NULL,
	[Active] [bit] NOT NULL,
 CONSTRAINT [PK_Roles] PRIMARY KEY CLUSTERED 
(
	[VehicleTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Vehicle].[Vehicle]    Script Date: 9/21/2014 3:24:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Vehicle].[Vehicle](
	[vehicleID] [int] IDENTITY(1,1) NOT NULL,
	[MakeAndModelID] [int] NOT NULL,
	[MotorID] [smallint] NOT NULL,
	[TransmissionID] [smallint] NOT NULL,
	[FuelID] [smallint] NOT NULL,
	[DriveTrainID] [smallint] NOT NULL,
	[VehicleConditionID] [smallint] NOT NULL,
	[Cilinders] [smallint] NOT NULL,
	[CurrencyId] [smallint] NOT NULL,
	[Price] [decimal](14, 2) NOT NULL,
	[TotalSeating] [smallint] NOT NULL,
	[ColorInteriorId] [smallint] NULL,
	[ColorExteriorId] [smallint] NULL,
	[Year] [smallint] NOT NULL,
	[Created] [datetime] NOT NULL CONSTRAINT [DF_VehicleVehicle_Created]  DEFAULT (getdate()),
	[Modified] [datetime] NOT NULL CONSTRAINT [DF_VehicleVehicle_Modified]  DEFAULT (getdate()),
	[Tags] [varchar](600) NULL,
	[Description] [varchar](800) NULL,
	[StateId] [smallint] NOT NULL CONSTRAINT [DF_vehicle_StateId]  DEFAULT ((1)),
PRIMARY KEY CLUSTERED 
(
	[vehicleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [App].[Customer] ON 

INSERT [App].[Customer] ([user_id_performance], [UserId], [CompanyName], [Name], [Lastname], [Identification], [Birthday], [Phone], [Mobile], [Email], [isDealer], [Address], [CreatedDate], [ModifiedDate], [ModifiedBy], [Username], [PasswordHash], [SecurityStamp], [ConfirmationToken], [IsConfirmed], [PlanId]) VALUES (1, N'3050e8bd-4f28-44f1-8317-1f23456e99df', N'EnAutos', N'fagro', N'vizcaino', NULL, CAST(N'1988-09-18' AS Date), 8094800860, 8094800860, N'miauto@enautos.com', 0, N'El brisal, manzana #21 casa 1.', CAST(N'2014-09-03 16:40:57.263' AS DateTime), CAST(N'2014-09-03 16:40:57.263' AS DateTime), NULL, N'fagro', N'AEeCvdGApkGMeCoQ+qomh1pJ8fFMkA2NpRoP0JzfM22gJkA3/LnEkUfHjeGzV5N33g==', N'a693eeb3-d696-4c7d-987e-d65ff01b1c36', N'F6B352A1-7E02-4464-B9AA-AF0870A56F65', 1, 1)
INSERT [App].[Customer] ([user_id_performance], [UserId], [CompanyName], [Name], [Lastname], [Identification], [Birthday], [Phone], [Mobile], [Email], [isDealer], [Address], [CreatedDate], [ModifiedDate], [ModifiedBy], [Username], [PasswordHash], [SecurityStamp], [ConfirmationToken], [IsConfirmed], [PlanId]) VALUES (2, N'8299e261-2093-48db-ade3-faad2338eedc', N'Ciber Dealer', N'Deivid W.', N'Santiago S.', NULL, CAST(N'1987-01-04' AS Date), 8092216663, 8294218338, N'deividx5@hotmail.com', 1, N'Av. Independencia 410, Esq. Socorro Sanchez, Sto. Dgo. Distrito Nacional.', CAST(N'2014-09-11 09:49:21.760' AS DateTime), CAST(N'2014-09-11 09:49:21.760' AS DateTime), NULL, N'CiberD', N'AD0mmkOTO0rx7I0mD+bPmzCs6+kIXxzfPGH+/srZHgFm7gR81japISI95DYeE4mQRQ==', N'52c890c7-d434-4ae2-acfc-e3f1691f2adc', N'f52a6461-d385-4592-8a5d-fa045e1a7fd5', 1, NULL)
SET IDENTITY_INSERT [App].[Customer] OFF
SET IDENTITY_INSERT [Config].[User] ON 

INSERT [Config].[User] ([AppUserID], [Name], [LastName], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Role]) VALUES (1, N'Fagro', N'Vizcaino', CAST(N'2014-04-13 13:51:00.627' AS DateTime), CAST(N'2014-04-13 13:51:00.627' AS DateTime), 1, 1, 1)
SET IDENTITY_INSERT [Config].[User] OFF
SET IDENTITY_INSERT [Vehicle].[Color] ON 

INSERT [Vehicle].[Color] ([ColorID], [Name], [Value], [Description], [Active], [Created], [Modified], [ModifiedBy]) VALUES (1, N'Blanco', N'#FFFFFF', NULL, 1, CAST(N'2014-09-17 12:57:54.567' AS DateTime), CAST(N'2014-09-17 12:57:54.567' AS DateTime), NULL)
INSERT [Vehicle].[Color] ([ColorID], [Name], [Value], [Description], [Active], [Created], [Modified], [ModifiedBy]) VALUES (2, N'Negro', N'#000000', NULL, 1, CAST(N'2014-09-17 12:58:39.173' AS DateTime), CAST(N'2014-09-17 12:58:39.173' AS DateTime), NULL)
INSERT [Vehicle].[Color] ([ColorID], [Name], [Value], [Description], [Active], [Created], [Modified], [ModifiedBy]) VALUES (3, N'Azul', N'#0066FF', NULL, 1, CAST(N'2014-09-17 12:59:01.583' AS DateTime), CAST(N'2014-09-17 12:59:01.583' AS DateTime), NULL)
INSERT [Vehicle].[Color] ([ColorID], [Name], [Value], [Description], [Active], [Created], [Modified], [ModifiedBy]) VALUES (4, N'Rojo', N'#FF0000', NULL, 1, CAST(N'2014-09-17 13:01:10.650' AS DateTime), CAST(N'2014-09-17 13:01:10.650' AS DateTime), NULL)
INSERT [Vehicle].[Color] ([ColorID], [Name], [Value], [Description], [Active], [Created], [Modified], [ModifiedBy]) VALUES (5, N'Gris', N'#B2B2B2', NULL, 1, CAST(N'2014-09-17 13:01:58.147' AS DateTime), CAST(N'2014-09-17 13:01:58.147' AS DateTime), NULL)
INSERT [Vehicle].[Color] ([ColorID], [Name], [Value], [Description], [Active], [Created], [Modified], [ModifiedBy]) VALUES (6, N'Dorado', N'#996633', NULL, 1, CAST(N'2014-09-17 13:02:31.840' AS DateTime), CAST(N'2014-09-17 13:02:31.840' AS DateTime), NULL)
INSERT [Vehicle].[Color] ([ColorID], [Name], [Value], [Description], [Active], [Created], [Modified], [ModifiedBy]) VALUES (7, N'Verde', N'#009900', NULL, 1, CAST(N'2014-09-17 13:03:25.530' AS DateTime), CAST(N'2014-09-17 13:03:25.530' AS DateTime), NULL)
INSERT [Vehicle].[Color] ([ColorID], [Name], [Value], [Description], [Active], [Created], [Modified], [ModifiedBy]) VALUES (8, N'Gris Oscuro', N'#4C4C4C', NULL, 1, CAST(N'2014-09-17 13:13:11.630' AS DateTime), CAST(N'2014-09-17 13:13:19.277' AS DateTime), NULL)
INSERT [Vehicle].[Color] ([ColorID], [Name], [Value], [Description], [Active], [Created], [Modified], [ModifiedBy]) VALUES (9, N'Amarillo', N'#FFCC00', NULL, 1, CAST(N'2014-09-17 13:50:43.367' AS DateTime), CAST(N'2014-09-17 13:50:43.367' AS DateTime), NULL)
INSERT [Vehicle].[Color] ([ColorID], [Name], [Value], [Description], [Active], [Created], [Modified], [ModifiedBy]) VALUES (10, N'Beige', N'#F5F5DC', NULL, 1, CAST(N'2014-09-17 19:19:08.937' AS DateTime), CAST(N'2014-09-17 19:19:08.937' AS DateTime), NULL)
SET IDENTITY_INSERT [Vehicle].[Color] OFF
SET IDENTITY_INSERT [Vehicle].[Condition] ON 

INSERT [Vehicle].[Condition] ([ConditionID], [Name], [Value], [Description], [Active], [Created], [Modified], [ModifiedBy]) VALUES (1, N'Nuevo', CAST(0.00 AS Decimal(5, 2)), NULL, 1, CAST(N'2014-07-15 12:21:42.800' AS DateTime), CAST(N'2014-07-15 12:21:42.800' AS DateTime), NULL)
INSERT [Vehicle].[Condition] ([ConditionID], [Name], [Value], [Description], [Active], [Created], [Modified], [ModifiedBy]) VALUES (2, N'Usuado', CAST(1.00 AS Decimal(5, 2)), NULL, 1, CAST(N'2014-07-15 12:21:50.340' AS DateTime), CAST(N'2014-07-15 12:21:50.340' AS DateTime), NULL)
SET IDENTITY_INSERT [Vehicle].[Condition] OFF
SET IDENTITY_INSERT [Vehicle].[Currency] ON 

INSERT [Vehicle].[Currency] ([CurrencyId], [Name], [Value], [Description], [Created], [Modified], [Active], [ModifiedBy]) VALUES (1, N'DOP', CAST(1.00 AS Decimal(5, 2)), N'Peso Dominicano', CAST(N'2014-08-29 08:39:35.780' AS DateTime), CAST(N'2014-08-29 08:39:35.780' AS DateTime), NULL, 1)
INSERT [Vehicle].[Currency] ([CurrencyId], [Name], [Value], [Description], [Created], [Modified], [Active], [ModifiedBy]) VALUES (2, N'USD', CAST(2.00 AS Decimal(5, 2)), N'Dólar Estadounidense', CAST(N'2014-08-29 08:40:56.150' AS DateTime), CAST(N'2014-08-29 08:40:56.150' AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [Vehicle].[Currency] OFF
SET IDENTITY_INSERT [Vehicle].[CustomPlan] ON 

INSERT [Vehicle].[CustomPlan] ([CustomPlanId], [PlanId], [NotificationModelID], [Name], [Price], [NumberOfImages], [SearchRankingMinutes], [AdvertisingDurationHours], [NumberVehiclesAllowed], [SearchRankVehiclesAllowed], [Created], [Modified]) VALUES (5, 6, 1, N'Enterprise', CAST(1500.0000 AS Decimal(10, 4)), 16, 300, 720, 100, 0, CAST(N'2014-09-15 08:28:07.397' AS DateTime), CAST(N'2014-09-15 08:28:07.397' AS DateTime))
SET IDENTITY_INSERT [Vehicle].[CustomPlan] OFF
SET IDENTITY_INSERT [Vehicle].[DriveTrain] ON 

INSERT [Vehicle].[DriveTrain] ([DriveTrainID], [Name], [Value], [Description], [Created], [Modified], [Active], [ModifiedBy]) VALUES (2, N'(FWD) Tracción Delantera', CAST(0.00 AS Decimal(5, 2)), NULL, CAST(N'2014-07-15 12:18:00.623' AS DateTime), CAST(N'2014-07-15 12:18:00.623' AS DateTime), NULL, NULL)
INSERT [Vehicle].[DriveTrain] ([DriveTrainID], [Name], [Value], [Description], [Created], [Modified], [Active], [ModifiedBy]) VALUES (3, N'(RWD) Tracción trasera', CAST(1.00 AS Decimal(5, 2)), NULL, CAST(N'2014-07-15 12:19:44.540' AS DateTime), CAST(N'2014-07-15 12:19:44.540' AS DateTime), NULL, NULL)
INSERT [Vehicle].[DriveTrain] ([DriveTrainID], [Name], [Value], [Description], [Created], [Modified], [Active], [ModifiedBy]) VALUES (4, N'(4WD) Tracción 4 ruedas', CAST(2.00 AS Decimal(5, 2)), NULL, CAST(N'2014-07-15 12:20:24.780' AS DateTime), CAST(N'2014-07-15 12:20:24.780' AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [Vehicle].[DriveTrain] OFF
SET IDENTITY_INSERT [Vehicle].[Fuel] ON 

INSERT [Vehicle].[Fuel] ([FuelID], [Name], [Value], [Description], [Created], [Modified], [Active], [ModifiedBy]) VALUES (1, N'Gasolina', CAST(0.00 AS Decimal(5, 2)), NULL, CAST(N'2014-07-15 12:14:32.613' AS DateTime), CAST(N'2014-07-15 12:14:32.613' AS DateTime), NULL, NULL)
INSERT [Vehicle].[Fuel] ([FuelID], [Name], [Value], [Description], [Created], [Modified], [Active], [ModifiedBy]) VALUES (2, N'Diesel', CAST(1.00 AS Decimal(5, 2)), NULL, CAST(N'2014-07-15 12:15:43.447' AS DateTime), CAST(N'2014-07-15 12:15:43.447' AS DateTime), NULL, NULL)
INSERT [Vehicle].[Fuel] ([FuelID], [Name], [Value], [Description], [Created], [Modified], [Active], [ModifiedBy]) VALUES (3, N'GLP', CAST(2.00 AS Decimal(5, 2)), NULL, CAST(N'2014-09-15 13:46:51.900' AS DateTime), CAST(N'2014-09-15 13:46:51.900' AS DateTime), NULL, NULL)
INSERT [Vehicle].[Fuel] ([FuelID], [Name], [Value], [Description], [Created], [Modified], [Active], [ModifiedBy]) VALUES (4, N'Gas Natural', CAST(3.00 AS Decimal(5, 2)), NULL, CAST(N'2014-09-15 13:46:59.370' AS DateTime), CAST(N'2014-09-15 13:46:59.370' AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [Vehicle].[Fuel] OFF
SET IDENTITY_INSERT [Vehicle].[Highlights] ON 

INSERT [Vehicle].[Highlights] ([HighlightsId], [vehicleId], [Created], [Modified], [Active], [ModifiedBy]) VALUES (4, 11, CAST(N'2014-09-21 14:57:48.633' AS DateTime), CAST(N'2014-09-21 14:57:48.633' AS DateTime), 1, NULL)
INSERT [Vehicle].[Highlights] ([HighlightsId], [vehicleId], [Created], [Modified], [Active], [ModifiedBy]) VALUES (5, 12, CAST(N'2014-09-21 14:58:52.373' AS DateTime), CAST(N'2014-09-21 14:58:52.373' AS DateTime), 1, NULL)
INSERT [Vehicle].[Highlights] ([HighlightsId], [vehicleId], [Created], [Modified], [Active], [ModifiedBy]) VALUES (6, 13, CAST(N'2014-09-21 15:16:36.007' AS DateTime), CAST(N'2014-09-21 15:16:36.007' AS DateTime), 1, NULL)
SET IDENTITY_INSERT [Vehicle].[Highlights] OFF
SET IDENTITY_INSERT [Vehicle].[MakeAndModel] ON 

INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1833, N'Acura', NULL, 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1834, N'Acura', N'1.6 EL', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1835, N'Acura', N'1.7 EL', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1836, N'Acura', N'2.2 CL', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1837, N'Acura', N'2.3 CL', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1838, N'Acura', N'2.5 TL', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1839, N'Acura', N'3.0 CL', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1840, N'Acura', N'3.2 CL', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1841, N'Acura', N'3.2 TL', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1842, N'Acura', N'3.5 RL', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1843, N'Acura', N'CL', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1844, N'Acura', N'CSX', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1845, N'Acura', N'EL', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1846, N'Acura', N'Integra', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1847, N'Acura', N'Legend', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1848, N'Acura', N'MDX', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1849, N'Acura', N'NSX', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1850, N'Acura', N'NSX-T', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1851, N'Acura', N'RDX', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1852, N'Acura', N'RL', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1853, N'Acura', N'RSX', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1854, N'Acura', N'SLX', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1855, N'Acura', N'TL', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1856, N'Acura', N'TSX', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1857, N'Acura', N'Vigor', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1858, N'Alfa Romeo', NULL, 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1859, N'Alfa Romeo', N'145', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1860, N'Alfa Romeo', N'146', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1861, N'Alfa Romeo', N'147', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1862, N'Alfa Romeo', N'155', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1863, N'Alfa Romeo', N'156', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1864, N'Alfa Romeo', N'159', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1865, N'Alfa Romeo', N'164', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1866, N'Alfa Romeo', N'166', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1867, N'Alfa Romeo', N'33', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1868, N'Alfa Romeo', N'75', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1869, N'Alfa Romeo', N'Alfetta', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1870, N'Alfa Romeo', N'GT', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1871, N'Alfa Romeo', N'GTV', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1872, N'Alfa Romeo', N'GTV-6', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1873, N'Alfa Romeo', N'Guiletta', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1874, N'Alfa Romeo', N'Milano', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1875, N'Alfa Romeo', N'Spider', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1876, N'AMC', NULL, 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1877, N'AMC', N'Ambassador', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1878, N'AMC', N'AMX', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1879, N'AMC', N'Concord', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1880, N'AMC', N'Eagle', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1881, N'AMC', N'Gremlin', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1882, N'AMC', N'Hornet', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1883, N'AMC', N'Javelin', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1884, N'AMC', N'Marlin', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1885, N'AMC', N'Matador', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1886, N'AMC', N'Pacer', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1887, N'AMC', N'Rambler', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1888, N'AMC', N'Rebel', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1889, N'AMC', N'Spirit', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1890, N'Audi', NULL, 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1891, N'Audi', N'100', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1892, N'Audi', N'100 C4 Series', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1893, N'Audi', N'100 V6', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1894, N'Audi', N'200', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1895, N'Audi', N'4000', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1896, N'Audi', N'5000', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1897, N'Audi', N'80 Cabriolet', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1898, N'Audi', N'90', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1899, N'Audi', N'A3', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1900, N'Audi', N'A4', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1901, N'Audi', N'A4 Cabriolet', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1902, N'Audi', N'A6', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1903, N'Audi', N'A8', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1904, N'Audi', N'Allroad', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1905, N'Audi', N'Cabriolet', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1906, N'Audi', N'Compact', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1907, N'Audi', N'Coupé', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1908, N'Audi', N'Coupé 20V', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1909, N'Audi', N'Fox', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1910, N'Audi', N'Q7', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1911, N'Audi', N'Quattro', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1912, N'Audi', N'R8', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1913, N'Audi', N'RS 4', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1914, N'Audi', N'RS 6', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1915, N'Audi', N'S4', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1916, N'Audi', N'S4 Cabriolet', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1917, N'Audi', N'S6', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1918, N'Audi', N'S8', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1919, N'Audi', N'TT Roadster', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1920, N'Audi', N'V8', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1921, N'Aston Martin', NULL, 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1922, N'Aston Martin', N'AMV8 Vantage', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1923, N'Aston Martin', N'Bulldog', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1924, N'Aston Martin', N'DB AR1', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1925, N'Aston Martin', N'DB2', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1926, N'Aston Martin', N'DB3 Spyder', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1927, N'Aston Martin', N'DB3S', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1928, N'Aston Martin', N'DB4', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1929, N'Aston Martin', N'DB5', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1930, N'Aston Martin', N'DB6', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1931, N'Aston Martin', N'DB7', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
GO
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1932, N'Aston Martin', N'DB9', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1933, N'Aston Martin', N'DB9 Sports Pack', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1934, N'Aston Martin', N'DBR1', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1935, N'Aston Martin', N'DBR4', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1936, N'Aston Martin', N'DBR4', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1937, N'Aston Martin', N'DBRS9', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1938, N'Aston Martin', N'DBS', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1939, N'Aston Martin', N'DP212', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1940, N'Aston Martin', N'Lagonda', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1941, N'Aston Martin', N'Le Mans', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1942, N'Aston Martin', N'MK II Ulster Le Mans', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1943, N'Aston Martin', N'Prodrive', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1944, N'Aston Martin', N'Rapide', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1945, N'Aston Martin', N'Type C Speed Model ', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1946, N'Aston Martin', N'Ulster', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1947, N'Aston Martin', N'V12 Vanquish', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1948, N'Aston Martin', N'V8', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1949, N'Aston Martin', N'V8 Coupe', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1950, N'Aston Martin', N'V8 Vantage', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1951, N'Aston Martin', N'V8 Volante', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1952, N'Aston Martin', N'Vanquish Roadster', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1953, N'Aston Martin', N'Vanquish S', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1954, N'Aston Martin', N'Vantage', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1955, N'Aston Martin', N'Virage', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1956, N'Aston Martin', N'Zagato DB4 GT', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1957, N'Bentley', NULL, 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1958, N'Bentley', N'Arnage', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1959, N'Bentley', N'Azure', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1960, N'Bentley', N'Brooklands', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1961, N'Bentley', N'Continental', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1962, N'Bentley', N'Continental Flying Spur', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1963, N'Bentley', N'Continental GT', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1964, N'Bentley', N'Continental GTC', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1965, N'Bentley', N'Continental R', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1966, N'Bentley', N'Eight', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1967, N'Bentley', N'Mulsanne', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1968, N'Bentley', N'Turbo R', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1969, N'BMW', NULL, 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1970, N'BMW', N'1 Series', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1971, N'BMW', N'2002', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1972, N'BMW', N'2800', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1973, N'BMW', N'3 Series', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1974, N'BMW', N'3.0', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1975, N'BMW', N'318', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1976, N'BMW', N'320', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1977, N'BMW', N'323', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1978, N'BMW', N'325', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1979, N'BMW', N'328', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1980, N'BMW', N'330', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1981, N'BMW', N'335', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1982, N'BMW', N'5 Series', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1983, N'BMW', N'525', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1984, N'BMW', N'528', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1985, N'BMW', N'530', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1986, N'BMW', N'533', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1987, N'BMW', N'535', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1988, N'BMW', N'540', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1989, N'BMW', N'545', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1990, N'BMW', N'550', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1991, N'BMW', N'6 Series', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1992, N'BMW', N'630', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1993, N'BMW', N'633', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1994, N'BMW', N'635', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1995, N'BMW', N'645', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1996, N'BMW', N'650', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1997, N'BMW', N'7 Series', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1998, N'BMW', N'733', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (1999, N'BMW', N'735', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2000, N'BMW', N'740', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2001, N'BMW', N'745', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2002, N'BMW', N'750', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2003, N'BMW', N'760', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2004, N'BMW', N'8 Series', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2005, N'BMW', N'840', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2006, N'BMW', N'850', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2007, N'BMW', N'Bavaria', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2008, N'BMW', N'Isetta', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2009, N'BMW', N'L6', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2010, N'BMW', N'L7', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2011, N'BMW', N'M Coupe', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2012, N'BMW', N'M Roadster', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2013, N'BMW', N'M3', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2014, N'BMW', N'M5', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2015, N'BMW', N'M6', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2016, N'BMW', N'Mini Cooper', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2017, N'BMW', N'X3', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2018, N'BMW', N'X5', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2019, N'BMW', N'Z1', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2020, N'BMW', N'Z3', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2021, N'BMW', N'Z4', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2022, N'BMW', N'Z8', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2023, N'Bricklin', NULL, 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2024, N'Bricklin', N'SV-1', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2025, N'Bugatti', NULL, 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2026, N'Bugatti', N'EB110', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2027, N'Bugatti', N'EB112', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2028, N'Bugatti', N'Royale', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2029, N'Bugatti', N'Veyron', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2030, N'Buick', NULL, 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2031, N'Buick', N'Allure', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
GO
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2032, N'Buick', N'Apollo', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2033, N'Buick', N'Centurion', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2034, N'Buick', N'Century', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2035, N'Buick', N'Electra', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2036, N'Buick', N'Enclave', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2037, N'Buick', N'Estate wagon', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2038, N'Buick', N'Gran Sport', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2039, N'Buick', N'Grand National', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2040, N'Buick', N'Invicta', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2041, N'Buick', N'Lacrosse', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2042, N'Buick', N'LeSabre', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2043, N'Buick', N'Lucerne', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2044, N'Buick', N'Park Avenue', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2045, N'Buick', N'Rainier', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2046, N'Buick', N'Reatta', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2047, N'Buick', N'Regal', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2048, N'Buick', N'Rendezvous', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2049, N'Buick', N'Riviera', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2050, N'Buick', N'Roadmaster', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2051, N'Buick', N'Skyhawk', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2052, N'Buick', N'Skylark', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2053, N'Buick', N'Somerset', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2054, N'Buick', N'Super', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2055, N'Buick', N'Terraza', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2056, N'Buick', N'Wildcat', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2057, N'Cadillac', NULL, 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2058, N'Cadillac', N'Allanté', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2059, N'Cadillac', N'Biarritz', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2060, N'Cadillac', N'Brougham', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2061, N'Cadillac', N'Calais', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2062, N'Cadillac', N'Catera', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2063, N'Cadillac', N'Cimarron', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2064, N'Cadillac', N'Concours', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2065, N'Cadillac', N'Coupe de Ville', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2066, N'Cadillac', N'CTS', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2067, N'Cadillac', N'Custom', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2068, N'Cadillac', N'Deluxe', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2069, N'Cadillac', N'DeVille', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2070, N'Cadillac', N'DHS', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2071, N'Cadillac', N'DTS', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2072, N'Cadillac', N'Eldorado', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2073, N'Cadillac', N'Escalade', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2074, N'Cadillac', N'ETC', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2075, N'Cadillac', N'Fleetwood', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2076, N'Cadillac', N'Fleetwood Brougham', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2077, N'Cadillac', N'Sedan de Ville', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2078, N'Cadillac', N' Seventy-Five', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2079, N'Cadillac', N'Seville', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2080, N'Cadillac', N'Sixty Special', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2081, N'Cadillac', N'SLS', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2082, N'Cadillac', N'SRX', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2083, N'Cadillac', N'STS', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2084, N'Cadillac', N'XLR', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2085, N'Checker', NULL, 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2086, N'Checker', N'Marathon', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2087, N'Chevrolet', NULL, 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2088, N'Chevrolet', N'Alero', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2089, N'Chevrolet', N'Astro', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2090, N'Chevrolet', N'Avalanche', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2091, N'Chevrolet', N'Aveo', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2092, N'Chevrolet', N'Aveo 5', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2093, N'Chevrolet', N'Beauville', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2094, N'Chevrolet', N'Bel Air', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2095, N'Chevrolet', N'Beretta', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2096, N'Chevrolet', N'Biscayne', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2097, N'Chevrolet', N'Blazer', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2098, N'Chevrolet', N'C/K 1500', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2099, N'Chevrolet', N'C/K 2500', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2100, N'Chevrolet', N'C/K 3500', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2101, N'Chevrolet', N'C1500', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2102, N'Chevrolet', N'C2500', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2103, N'Chevrolet', N'C3500', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2104, N'Chevrolet', N'Camaro', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2105, N'Chevrolet', N'Camionnette', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2106, N'Chevrolet', N'Caprice', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2107, N'Chevrolet', N'Cavalier', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2108, N'Chevrolet', N'Celebrity', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2109, N'Chevrolet', N'Chevelle', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2110, N'Chevrolet', N'Chevette', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2111, N'Chevrolet', N'Chevy II', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2112, N'Chevrolet', N'Chevy Van', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2113, N'Chevrolet', N'Citation', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2114, N'Chevrolet', N'Cobalt', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2115, N'Chevrolet', N'Colorado', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2116, N'Chevrolet', N'Corsica', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2117, N'Chevrolet', N'Corvair', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2118, N'Chevrolet', N'Corvette', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2119, N'Chevrolet', N'Deluxe', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2120, N'Chevrolet', N'El Camino', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2121, N'Chevrolet', N'Envoy', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2122, N'Chevrolet', N'Epica', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2123, N'Chevrolet', N'Equinox', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2124, N'Chevrolet', N'Express', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2125, N'Chevrolet', N'G1500', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2126, N'Chevrolet', N'G2500', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2127, N'Chevrolet', N'HHR', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2128, N'Chevrolet', N'Impala', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2129, N'Chevrolet', N'Impala SS', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2130, N'Chevrolet', N'K1500', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2131, N'Chevrolet', N'K2500', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
GO
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2132, N'Chevrolet', N'K3500', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2133, N'Chevrolet', N'Kalos', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2134, N'Chevrolet', N'Lacetti', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2135, N'Chevrolet', N'Lumina', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2136, N'Chevrolet', N'Lumina APV', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2137, N'Chevrolet', N'Malibu', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2138, N'Chevrolet', N'Malibu Maxx', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2139, N'Chevrolet', N'Metro', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2140, N'Chevrolet', N'Monte Carlo', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2141, N'Chevrolet', N'Nomad', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2142, N'Chevrolet', N'Nova', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2143, N'Chevrolet', N'Optra', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2144, N'Chevrolet', N'Optra 5', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2145, N'Chevrolet', N'Optra Wagon', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2146, N'Chevrolet', N'Pick-up', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2147, N'Chevrolet', N'Prizm', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2148, N'Chevrolet', N'Rezzo', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2149, N'Chevrolet', N'S-10', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2150, N'Chevrolet', N'Silverado', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2151, N'Chevrolet', N'Sprint', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2152, N'Chevrolet', N'SSR', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2153, N'Chevrolet', N'Styleline', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2154, N'Chevrolet', N'Stylemaster', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2155, N'Chevrolet', N'Suburban', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2156, N'Chevrolet', N'Tacuma', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2157, N'Chevrolet', N'Tahoe', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2158, N'Chevrolet', N'Tracker', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2159, N'Chevrolet', N'Trailblazer', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2160, N'Chevrolet', N'Trailblazer EXT', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2161, N'Chevrolet', N'Uplander', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2162, N'Chevrolet', N'Vega', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2163, N'Chevrolet', N'Venture', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2164, N'Chevrolet', N'X11', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2165, N'Chevrolet', N'Z24', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2166, N'Chevrolet', N'Z28', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2167, N'Chevrolet', N'Z34', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2168, N'Chrysler', NULL, 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2169, N'Chrysler', N'300', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2170, N'Chrysler', N'300C', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2171, N'Chrysler', N'300M', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2172, N'Chrysler', N'Aspen', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2173, N'Chrysler', N'Cirrus', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2174, N'Chrysler', N'Concorde', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2175, N'Chrysler', N'Cordoba', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2176, N'Chrysler', N'Crossfire', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2177, N'Chrysler', N'Daytona', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2178, N'Chrysler', N'Dynasty', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2179, N'Chrysler', N'E-Class', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2180, N'Chrysler', N'Fifth Avenue', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2181, N'Chrysler', N'Imperial', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2182, N'Chrysler', N'Intrepid', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2183, N'Chrysler', N'Laser', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2184, N'Chrysler', N'LeBaron', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2185, N'Chrysler', N'LHS', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2186, N'Chrysler', N'Neon', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2187, N'Chrysler', N'New Yorker', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2188, N'Chrysler', N'Newport', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2189, N'Chrysler', N'Pacifica', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2190, N'Chrysler', N'Prowler', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2191, N'Chrysler', N'PT Cruiser', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2192, N'Chrysler', N'Saratoga', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2193, N'Chrysler', N'Sebring', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2194, N'Chrysler', N'Stratus', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2195, N'Chrysler', N'SRT6', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2196, N'Chrysler', N'SRT8', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2197, N'Chrysler', N'TC', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2198, N'Chrysler', N'Town & Country', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2199, N'Chrysler', N'Vision', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2200, N'Chrysler', N'Voyager', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2201, N'Chrysler', N'Windsor', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2202, N'Citroën', NULL, 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2203, N'Citroën', N'2CV', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2204, N'Citroën', N'AX', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2205, N'Citroën', N'Berlingo', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2206, N'Citroën', N'Berlingo 2', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2207, N'Citroën', N'BX', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2208, N'Citroën', N'C1', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2209, N'Citroën', N'C2', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2210, N'Citroën', N'C25', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2211, N'Citroën', N'C3', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2212, N'Citroën', N'C3 II', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2213, N'Citroën', N'C4', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2214, N'Citroën', N'C5', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2215, N'Citroën', N'C5 II', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2216, N'Citroën', N'C8', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2217, N'Citroën', N'CX', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2218, N'Citroën', N'Dispatch', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2219, N'Citroën', N'DS', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2220, N'Citroën', N'Evasion', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2221, N'Citroën', N'ID', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2222, N'Citroën', N'Jumper', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2223, N'Citroën', N'Jumpy', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2224, N'Citroën', N'Jumpy II', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2225, N'Citroën', N'Pluriel', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2226, N'Citroën', N'Relay', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2227, N'Citroën', N'Saxo', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2228, N'Citroën', N'SM', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2229, N'Citroën', N'Synergie', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2230, N'Citroën', N'Van (C25)', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2231, N'Citroën', N'Xantia', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
GO
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2232, N'Citroën', N'Xantia II', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2233, N'Citroën', N'XM', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2234, N'Citroën', N'Xsara', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2235, N'Citroën', N'Xsara Picasso', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2236, N'Citroën', N'ZX', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2237, N'Dacia', NULL, 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2238, N'Dacia', N'1310', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2239, N'Dacia', N'1410', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2240, N'Dacia', N'Camionnette', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2241, N'Daewoo', NULL, 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2242, N'Daewoo', N'Cielo', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2243, N'Daewoo', N'Espero', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2244, N'Daewoo', N'Evanda', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2245, N'Daewoo', N'Kalos', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2246, N'Daewoo', N'Korando', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2247, N'Daewoo', N'Lacetti', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2248, N'Daewoo', N'Lanos', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2249, N'Daewoo', N'Leganza', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2250, N'Daewoo', N'Matiz', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2251, N'Daewoo', N'Musso', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2252, N'Daewoo', N'Nexia', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2253, N'Daewoo', N'Nubira', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2254, N'Daewoo', N'Rezzo', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2255, N'Daewoo', N'Tacuma', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2256, N'Daihatsu', NULL, 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2257, N'Daihatsu', N'Charade', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2258, N'Daihatsu', N'Cuore', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2259, N'Daihatsu', N'Grand Move', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2260, N'Daihatsu', N'Max', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2261, N'Daihatsu', N'Move', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2262, N'Daihatsu', N'Rocky', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2263, N'Daihatsu', N'Sirion', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2264, N'Daihatsu', N'Terios', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2265, N'Daihatsu', N'YRV', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2266, N'Datsun', NULL, 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2267, N'Datsun', N'1200', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2268, N'Datsun', N'200SX', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2269, N'Datsun', N'210', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2270, N'Datsun', N'240Z', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2271, N'Datsun', N'260Z', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2272, N'Datsun', N'280Z', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2273, N'Datsun', N'280ZX', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2274, N'Datsun', N'310', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2275, N'Datsun', N'510', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2276, N'Datsun', N'610', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2277, N'Datsun', N'620', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2278, N'Datsun', N'710', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2279, N'Datsun', N'810', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2280, N'Datsun', N'B210', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2281, N'Datsun', N'F10', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2282, N'Datsun', N'Maxima', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2283, N'Datsun', N'Micra', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2284, N'Datsun', N'Pickup', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2285, N'Datsun', N'PL720', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2286, N'Datsun', N'Pulsar', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2287, N'Datsun', N'Sentra', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2288, N'Datsun', N'Stanza', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2289, N'Dodge', NULL, 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2290, N'Dodge', N'2000 GTX', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2291, N'Dodge', N'600', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2292, N'Dodge', N'Aries', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2293, N'Dodge', N'Arrow', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2294, N'Dodge', N'Aspen', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2295, N'Dodge', N'Avenger', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2296, N'Dodge', N'Caliber', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2297, N'Dodge', N'Caravan', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2298, N'Dodge', N'Caravan C/V', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2299, N'Dodge', N'Challenger', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2300, N'Dodge', N'Charger', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2301, N'Dodge', N'Colt', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2302, N'Dodge', N'Coronet', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2303, N'Dodge', N'Dakota', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2304, N'Dodge', N'Dart', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2305, N'Dodge', N'Daytona', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2306, N'Dodge', N'Diplomat', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2307, N'Dodge', N'Durango', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2308, N'Dodge', N'Fargo', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2309, N'Dodge', N'Grand Caravan', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2310, N'Dodge', N'Intrepid', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2311, N'Dodge', N'Lancer', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2312, N'Dodge', N'Magnum', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2313, N'Dodge', N'Mini RamVan', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2314, N'Dodge', N'Neon', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2315, N'Dodge', N'Nitro', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2316, N'Dodge', N'Omni', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2317, N'Dodge', N'Omni 024', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2318, N'Dodge', N'Pace Arrow', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2319, N'Dodge', N'Pick-up', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2320, N'Dodge', N'Polara', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2321, N'Dodge', N'Power Ram', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2322, N'Dodge', N'Power Ram 50', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2323, N'Dodge', N'Raider', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2324, N'Dodge', N'Ram', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2325, N'Dodge', N'Ram 1500', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2326, N'Dodge', N'Ram 2500', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2327, N'Dodge', N'Ram 3500', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2328, N'Dodge', N'Ram 50', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2329, N'Dodge', N'Ram Van', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2330, N'Dodge', N'Ram Wagon', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2331, N'Dodge', N'RamCharger', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
GO
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2332, N'Dodge', N'Rampage', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2333, N'Dodge', N'Royal', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2334, N'Dodge', N'Saratoga', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2335, N'Dodge', N'Shadow', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2336, N'Dodge', N'Spirit', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2337, N'Dodge', N'Sprinter', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2338, N'Dodge', N'SRT4', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2339, N'Dodge', N'St-Regis', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2340, N'Dodge', N'Stealth', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2341, N'Dodge', N'Stratus', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2342, N'Dodge', N'Super Bee', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2343, N'Dodge', N'SX 2.0', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2344, N'Dodge', N'Van 250', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2345, N'Dodge', N'Van 2500', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2346, N'Dodge', N'Viper', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2347, N'Ferrari', NULL, 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2348, N'Ferrari', N'125', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2349, N'Ferrari', N'159', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2350, N'Ferrari', N'166', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2351, N'Ferrari', N'195', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2352, N'Ferrari', N'196', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2353, N'Ferrari', N'208', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2354, N'Ferrari', N'212', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2355, N'Ferrari', N'225', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2356, N'Ferrari', N'246', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2357, N'Ferrari', N'250', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2358, N'Ferrari', N'268', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2359, N'Ferrari', N'286', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2360, N'Ferrari', N'288', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2361, N'Ferrari', N'290', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2362, N'Ferrari', N'308', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2363, N'Ferrari', N'315', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2364, N'Ferrari', N'328', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2365, N'Ferrari', N'330', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2366, N'Ferrari', N'335', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2367, N'Ferrari', N'340', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2368, N'Ferrari', N'348', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2369, N'Ferrari', N'355', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2370, N'Ferrari', N'360', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2371, N'Ferrari', N'365', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2372, N'Ferrari', N'375', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2373, N'Ferrari', N'400', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2374, N'Ferrari', N'410', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2375, N'Ferrari', N'412', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2376, N'Ferrari', N'430', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2377, N'Ferrari', N'456', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2378, N'Ferrari', N'500', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2379, N'Ferrari', N'512', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2380, N'Ferrari', N'550', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2381, N'Ferrari', N'575', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2382, N'Ferrari', N'599', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2383, N'Ferrari', N'612', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2384, N'Ferrari', N'625', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2385, N'Ferrari', N'712', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2386, N'Ferrari', N'735', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2387, N'Ferrari', N'860', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2388, N'Ferrari', N'BB512', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2389, N'Ferrari', N'Daytona', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2390, N'Ferrari', N'Enzo', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2391, N'Ferrari', N'F 333 SP', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2392, N'Ferrari', N'F355', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2393, N'Ferrari', N'F360', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2394, N'Ferrari', N'F40', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2395, N'Ferrari', N'F430', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2396, N'Ferrari', N'F50', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2397, N'Ferrari', N'F550', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2398, N'Ferrari', N'GTB', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2399, N'Ferrari', N'GTO', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2400, N'Ferrari', N'GTS', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2401, N'Ferrari', N'Mondial', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2402, N'Ferrari', N'Spider', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2403, N'Ferrari', N'Superamerica', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2404, N'Ferrari', N'Testa Rossa', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2405, N'Fiat', NULL, 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2406, N'Fiat', N'125', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2407, N'Fiat', N'130', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2408, N'Fiat', N'131', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2409, N'Fiat', N'500', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2410, N'Fiat', N'Barchetta', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2411, N'Fiat', N'Brava/Bravo', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2412, N'Fiat', N'Cinquecento', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2413, N'Fiat', N'Coupé', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2414, N'Fiat', N'Croma', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2415, N'Fiat', N'Dino', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2416, N'Fiat', N'Doblò', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2417, N'Fiat', N'Ducato', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2418, N'Fiat', N'Idea', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2419, N'Fiat', N'Marea', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2420, N'Fiat', N'Multipla', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2421, N'Fiat', N'Palio', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2422, N'Fiat', N'Panda', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2423, N'Fiat', N'Punto', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2424, N'Fiat', N'Punto Grande', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2425, N'Fiat', N'Ritmo', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2426, N'Fiat', N'Scudo', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2427, N'Fiat', N'Seicento', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2428, N'Fiat', N'Siena', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2429, N'Fiat', N'Spider', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2430, N'Fiat', N'Stilo', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2431, N'Fiat', N'Strada', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
GO
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2432, N'Fiat', N'Tempra', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2433, N'Fiat', N'Tipo', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2434, N'Fiat', N'Tipo (Digital)', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2435, N'Fiat', N'Ulysse', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2436, N'Fiat', N'Ulysse II', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2437, N'Fiat', N'Uno', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2438, N'Fiat', N'X1/9', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2439, N'Ford', NULL, 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2440, N'Ford', N'Aerostar', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2441, N'Ford', N'Anglia', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2442, N'Ford', N'Aspire', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2443, N'Ford', N'Bronco II', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2444, N'Ford', N'Bronco', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2445, N'Ford', N'Capri', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2446, N'Ford', N'Club Wagon', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2447, N'Ford', N'Cobra', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2448, N'Ford', N'Contour', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2449, N'Ford', N'Cortina', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2450, N'Ford', N'Cougar', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2451, N'Ford', N'Country Squire', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2452, N'Ford', N'Crown Victoria', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2453, N'Ford', N'Curier', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2454, N'Ford', N'Custom', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2455, N'Ford', N'Cutaway', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2456, N'Ford', N'E-150', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2457, N'Ford', N'E-250', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2458, N'Ford', N'E-350', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2459, N'Ford', N'E-450', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2460, N'Ford', N'Econoline', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2461, N'Ford', N'Econovan', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2462, N'Ford', N'Edge', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2463, N'Ford', N'Elite', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2464, N'Ford', N'Escape Hybrid', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2465, N'Ford', N'Escape', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2466, N'Ford', N'Escort', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2467, N'Ford', N'Excursion', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2468, N'Ford', N'EXP', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2469, N'Ford', N'Expedition', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2470, N'Ford', N'Explorer Sport Trac', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2471, N'Ford', N'Explorer', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2472, N'Ford', N'F-100', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2473, N'Ford', N'F-150', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2474, N'Ford', N'F-250', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2475, N'Ford', N'F-350', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2476, N'Ford', N'F-450', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2477, N'Ford', N'F-550', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2478, N'Ford', N'F-650', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2479, N'Ford', N'F-750', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2480, N'Ford', N'Fairlane', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2481, N'Ford', N'Fairmont', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2482, N'Ford', N'Falcon', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2483, N'Ford', N'Festiva', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2484, N'Ford', N'Fiesta', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2485, N'Ford', N'Five Hundred', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2486, N'Ford', N'Focus', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2487, N'Ford', N'Forte', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2488, N'Ford', N'Freestar', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2489, N'Ford', N'Freestyle', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2490, N'Ford', N'Fusion', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2491, N'Ford', N'Futura XR', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2492, N'Ford', N'Galaxie', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2493, N'Ford', N'Gran Torino', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2494, N'Ford', N'Granada', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2495, N'Ford', N'Grand Marquis', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2496, N'Ford', N'GT40', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2497, N'Ford', N'GT', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2498, N'Ford', N'Ka', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2499, N'Ford', N'Laser', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2500, N'Ford', N'Lightning', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2501, N'Ford', N'LTD', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2502, N'Ford', N'Marauder', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2503, N'Ford', N'Maverick', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2504, N'Ford', N'Model A', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2505, N'Ford', N'Model T', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2506, N'Ford', N'Mondeo', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2507, N'Ford', N'Mustang', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2508, N'Ford', N'Orion', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2509, N'Ford', N'Pinto', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2510, N'Ford', N'Probe', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2511, N'Ford', N'Puma', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2512, N'Ford', N'Ranchero', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2513, N'Ford', N'Ranger', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2514, N'Ford', N'Scorpio', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2515, N'Ford', N'Shelby', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2516, N'Ford', N'Sierra', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2517, N'Ford', N'Skyliner', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2518, N'Ford', N'Sport Track', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2519, N'Ford', N'Sunliner', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2520, N'Ford', N'Super Duty', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2521, N'Ford', N'SVT', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2522, N'Ford', N'T-Bucket', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2523, N'Ford', N'Taurus', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2524, N'Ford', N'Tempo', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2525, N'Ford', N'Thunderbird', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2526, N'Ford', N'Torino', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2527, N'Ford', N'Transit', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2528, N'Ford', N'Tudor', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2529, N'Ford', N'Ute', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2530, N'Ford', N'Villager', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2531, N'Ford', N'Windstar', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
GO
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2532, N'Ford', N'Zephyr', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2533, N'Ford', N'ZX2', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2534, N'Galloper', NULL, 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2535, N'Galloper', N'Exceed', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2536, N'Geo', NULL, 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2537, N'Geo', N'Metro', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2538, N'Geo', N'Prizm', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2539, N'Geo', N'Storm', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2540, N'Geo', N'Tracker', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2541, N'Honda', NULL, 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2542, N'Honda', N'Accord', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2543, N'Honda', N'Accord Coupé', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2544, N'Honda', N'Civic', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2545, N'Honda', N'Civic DelSol', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2546, N'Honda', N'Civic Hybrid', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2547, N'Honda', N'Civic Sedan DX,LX,EX', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2548, N'Honda', N'Civic Si', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2549, N'Honda', N'Concerto', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2550, N'Honda', N'CR-V LX,EX', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2551, N'Honda', N'CRX', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2552, N'Honda', N'Element', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2553, N'Honda', N'Fit', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2554, N'Honda', N'FR-V', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2555, N'Honda', N'HR-V', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2556, N'Honda', N'Insight', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2557, N'Honda', N'Jazz', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2558, N'Honda', N'Legend', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2559, N'Honda', N'Logo', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2560, N'Honda', N'N600', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2561, N'Honda', N'Odyssey', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2562, N'Honda', N'Passport', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2563, N'Honda', N'Pilot', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2564, N'Honda', N'Prelude', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2565, N'Honda', N'Ridgeline', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2566, N'Honda', N'S2000', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2567, N'Honda', N'S600', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2568, N'Honda', N'S800', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2569, N'Honda', N'Shuttle', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2570, N'Honda', N'Stream', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2571, N'GMC', NULL, 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2572, N'GMC', N'Acadia', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2573, N'GMC', N'Blazer', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2574, N'GMC', N'Fullsize', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2575, N'GMC', N'Caballero', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2576, N'GMC', N'Canyon', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2577, N'GMC', N'Cyclone', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2578, N'GMC', N'Envoy', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2579, N'GMC', N'Envoy XL', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2580, N'GMC', N'Envoy XUV', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2581, N'GMC', N'Jimmy', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2582, N'GMC', N'Pickup', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2583, N'GMC', N'Rally', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2584, N'GMC', N'S-15', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2585, N'GMC', N'Safari', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2586, N'GMC', N'Savana', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2587, N'GMC', N'Sierra', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2588, N'GMC', N'Sonoma', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2589, N'GMC', N'Sprint', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2590, N'GMC', N'Suburban', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2591, N'GMC', N'Topkick', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2592, N'GMC', N'Tracker', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2593, N'GMC', N'Typhoon', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2594, N'GMC', N'Van', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2595, N'GMC', N'Vandura', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2596, N'GMC', N'Yukon', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2597, N'GMC', N'Yukon XL', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2598, N'Hummer', NULL, 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2599, N'Hummer', N'H1', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2600, N'Hummer', N'H2', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2601, N'Hummer', N'H3', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2602, N'Hyundai', NULL, 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2603, N'Hyundai', N'Accent', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2604, N'Hyundai', N'Amica', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2605, N'Hyundai', N'Atos', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2606, N'Hyundai', N'Azera', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2607, N'Hyundai', N'Coupé', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2608, N'Hyundai', N'Elantra', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2609, N'Hyundai', N'Entourage', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2610, N'Hyundai', N'Excel', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2611, N'Hyundai', N'Getz', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2612, N'Hyundai', N'Grandeur XG', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2613, N'Hyundai', N'H 100', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2614, N'Hyundai', N'H1', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2615, N'Hyundai', N'Matrix', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2616, N'Hyundai', N'Pony', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2617, N'Hyundai', N'Santa Fe', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2618, N'Hyundai', N'Scoupe', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2619, N'Hyundai', N'Sonata', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2620, N'Hyundai', N'Starex', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2621, N'Hyundai', N'Stellar', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2622, N'Hyundai', N'Terracan', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2623, N'Hyundai', N'Tiburon', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2624, N'Hyundai', N'Trajet', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2625, N'Hyundai', N'Tucson', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2626, N'Hyundai', N'Veracruz', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2627, N'Hyundai', N'XG', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2628, N'Hyundai', N'XG300', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2629, N'Hyundai', N'XG350', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2630, N'Infiniti', NULL, 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2631, N'Infiniti', N'FX35', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
GO
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2632, N'Infiniti', N'FX45', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2633, N'Infiniti', N'G20', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2634, N'Infiniti', N'G35', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2635, N'Infiniti', N'I30', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2636, N'Infiniti', N'I35', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2637, N'Infiniti', N'J30', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2638, N'Infiniti', N'M35', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2639, N'Infiniti', N'M45', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2640, N'Infiniti', N'Q45', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2641, N'Infiniti', N'QX4', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2642, N'Infiniti', N'QX56', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2643, N'Isuzu', NULL, 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2644, N'Isuzu', N'Amigo', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2645, N'Isuzu', N'Ascender', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2646, N'Isuzu', N'Axiom', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2647, N'Isuzu', N'Camionnette', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2648, N'Isuzu', N'Campo', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2649, N'Isuzu', N'D-Max', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2650, N'Isuzu', N'Hombre', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2651, N'Isuzu', N'I-Mark', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2652, N'Isuzu', N'Impulse', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2653, N'Isuzu', N'Oasis', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2654, N'Isuzu', N'Pickup', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2655, N'Isuzu', N'Pup', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2656, N'Isuzu', N'Rodeo', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2657, N'Isuzu', N'Rodeo Sport', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2658, N'Isuzu', N'Space Cab SLX 2500', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2659, N'Isuzu', N'Space Cab SLX 2800', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2660, N'Isuzu', N'Stylus', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2661, N'Isuzu', N'Trooper', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2662, N'Isuzu', N'Van/Midi', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2663, N'Isuzu', N'Vehicrosse', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2664, N'Jaguar', NULL, 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2665, N'Jaguar', N'E-type', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2666, N'Jaguar', N'MK', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2667, N'Jaguar', N'S-Type', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2668, N'Jaguar', N'Sovereign', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2669, N'Jaguar', N'Super V8', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2670, N'Jaguar', N'Vanden Plas', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2671, N'Jaguar', N'X-Type', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2672, N'Jaguar', N'X300', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2673, N'Jaguar', N'XJ', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2674, N'Jaguar', N'XJ Series', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2675, N'Jaguar', N'XJ-S', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2676, N'Jaguar', N'XJ-SC', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2677, N'Jaguar', N'XJ12', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2678, N'Jaguar', N'XJ40', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2679, N'Jaguar', N'XJ6', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2680, N'Jaguar', N'XJ6 Serie 2', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2681, N'Jaguar', N'XJ6 Serie 3', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2682, N'Jaguar', N'XJ8', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2683, N'Jaguar', N'XJ8 Vanden Plas', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2684, N'Jaguar', N'XJR', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2685, N'Jaguar', N'XJS', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2686, N'Jaguar', N'XK', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2687, N'Jaguar', N'XK Series', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2688, N'Jaguar', N'XK8', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2689, N'Jaguar', N'XKE', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2690, N'Jaguar', N'XKR', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2691, N'Jaguar', N'XR8', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2692, N'Jeep', NULL, 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2693, N'Jeep', N'Cherokee', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2694, N'Jeep', N'CJ', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2695, N'Jeep', N'Comanche', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2696, N'Jeep', N'Commander', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2697, N'Jeep', N'Compass', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2698, N'Jeep', N'Grand Cherokee', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2699, N'Jeep', N'Grand Wagoneer', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2700, N'Jeep', N'Liberty', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2701, N'Jeep', N'Patriot', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2702, N'Jeep', N'Renegade', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2703, N'Jeep', N'Sahara', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2704, N'Jeep', N'Scrambler', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2705, N'Jeep', N'TJ', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2706, N'Jeep', N'TJ Unlimited', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2707, N'Jeep', N'Wagoneer', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2708, N'Jeep', N'Wrangler', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2709, N'Jeep', N'Wrangler Unlimited', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2710, N'Jeep', N'YJ', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2711, N'Kia', NULL, 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2712, N'Kia', N'Amanti', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2713, N'Kia', N'Besta', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2714, N'Kia', N'Carens', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2715, N'Kia', N'Carens II', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2716, N'Kia', N'Carnival', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2717, N'Kia', N'Carstar', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2718, N'Kia', N'Cerato', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2719, N'Kia', N'Clarus', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2720, N'Kia', N'Credos', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2721, N'Kia', N'Joice', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2722, N'Kia', N'Magentis', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2723, N'Kia', N'Mentor', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2724, N'Kia', N'Opirus', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2725, N'Kia', N'Optima', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2726, N'Kia', N'Picanto', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2727, N'Kia', N'Pride', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2728, N'Kia', N'Rio', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2729, N'Kia', N'Rio 5', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2730, N'Kia', N'Rio RX-V', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2731, N'Kia', N'Rondo', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
GO
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2732, N'Kia', N'Sedona', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2733, N'Kia', N'Sephia', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2734, N'Kia', N'Sephia II', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2735, N'Kia', N'Sephia Sedan', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2736, N'Kia', N'Shuma', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2737, N'Kia', N'Shuma II', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2738, N'Kia', N'Sorento', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2739, N'Kia', N'Spectra', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2740, N'Kia', N'Spectra 5', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2741, N'Kia', N'Spectra EX', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2742, N'Kia', N'Spectra SX', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2743, N'Kia', N'Sportage', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2744, N'Kia', N'Visto', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2745, N'Lada', NULL, 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2746, N'Lada', N'Dennis', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2747, N'Lada', N'Niva', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2748, N'Lada', N'Samara', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2749, N'Lada', N'Signet', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2750, N'Lamborghini', NULL, 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2751, N'Lamborghini', N'350 GT', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2752, N'Lamborghini', N'Cheetah', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2753, N'Lamborghini', N'Countach', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2754, N'Lamborghini', N'Diablo', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2755, N'Lamborghini', N'Espada', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2756, N'Lamborghini', N'Gallardo', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2757, N'Lamborghini', N'Islero', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2758, N'Lamborghini', N'Jalpa', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2759, N'Lamborghini', N'Jarama', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2760, N'Lamborghini', N'LM', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2761, N'Lamborghini', N'Miura', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2762, N'Lamborghini', N'Murciélago', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2763, N'Lamborghini', N'Silhouette', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2764, N'Lamborghini', N'Urraco', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2765, N'Land Rover', NULL, 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2766, N'Land Rover', N'Defender', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2767, N'Land Rover', N'Discovery', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2768, N'Land Rover', N'Discovery II', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2769, N'Land Rover', N'Discovery 3', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2770, N'Land Rover', N'Freelander', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2771, N'Land Rover', N'LR2', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2772, N'Land Rover', N'LR3', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2773, N'Land Rover', N'Range Rover', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2774, N'Land Rover', N'Range Rover Sport', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2775, N'Lexus', NULL, 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2776, N'Lexus', N'ES 250', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2777, N'Lexus', N'ES 300', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2778, N'Lexus', N'ES 330', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2779, N'Lexus', N'ES 350', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2780, N'Lexus', N'GS 300', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2781, N'Lexus', N'GS 350', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2782, N'Lexus', N'GS 400', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2783, N'Lexus', N'GS 430', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2784, N'Lexus', N'GS 450h', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2785, N'Lexus', N'GX 470', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2786, N'Lexus', N'Harrier 4x4', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2787, N'Lexus', N'IS 220', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2788, N'Lexus', N'IS 250', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2789, N'Lexus', N'IS 300', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2790, N'Lexus', N'IS 350', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2791, N'Lexus', N'LS 400', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2792, N'Lexus', N'LS 430', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2793, N'Lexus', N'LS 460', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2794, N'Lexus', N'LS 600h L', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2795, N'Lexus', N'LX 450', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2796, N'Lexus', N'LX 470', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2797, N'Lexus', N'RX 300', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2798, N'Lexus', N'RX 330', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2799, N'Lexus', N'RX 350', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2800, N'Lexus', N'RX 400h', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2801, N'Lexus', N'SC 300', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2802, N'Lexus', N'SC 400', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2803, N'Lexus', N'SC 430', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2804, N'Lincoln', NULL, 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2805, N'Lincoln', N'Aviator', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2806, N'Lincoln', N'Blackwood', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2807, N'Lincoln', N'Continental', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2808, N'Lincoln', N'LS', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2809, N'Lincoln', N'Mark', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2810, N'Lincoln', N'Mark IV', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2811, N'Lincoln', N'Mark LT', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2812, N'Lincoln', N'Mark VII', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2813, N'Lincoln', N'Mark VIII', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2814, N'Lincoln', N'MKX', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2815, N'Lincoln', N'MKZ', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2816, N'Lincoln', N'Navigator', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2817, N'Lincoln', N'Town Car', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2818, N'Lincoln', N'Versailles', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2819, N'Lincoln', N'Zephyr', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2820, N'Mazda', NULL, 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2821, N'Mazda', N'121', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2822, N'Mazda', N'2', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2823, N'Mazda', N'3', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2824, N'Mazda', N'3 Sport', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2825, N'Mazda', N'323 F (BA)', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2826, N'Mazda', N'323 F (BJ)', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2827, N'Mazda', N'323', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2828, N'Mazda', N'5', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2829, N'Mazda', N'6', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2830, N'Mazda', N'6 Sport', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2831, N'Mazda', N'6 Sport Wagon', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
GO
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2832, N'Mazda', N'616', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2833, N'Mazda', N'626', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2834, N'Mazda', N'626 Avant', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2835, N'Mazda', N'626 Break', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2836, N'Mazda', N'626 Cronos', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2837, N'Mazda', N'929', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2838, N'Mazda', N'929 Serenia', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2839, N'Mazda', N'B2000', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2840, N'Mazda', N'B2200', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2841, N'Mazda', N'B2300', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2842, N'Mazda', N'B2500', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2843, N'Mazda', N'B2600', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2844, N'Mazda', N'B3000', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2845, N'Mazda', N'B4000', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2846, N'Mazda', N'Bravo 2600', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2847, N'Mazda', N'B-series', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2848, N'Mazda', N'Cosmo', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2849, N'Mazda', N'CX-7', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2850, N'Mazda', N'CX-9', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2851, N'Mazda', N'Demio', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2852, N'Mazda', N'E-series', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2853, N'Mazda', N'Familia', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2854, N'Mazda', N'GLC', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2855, N'Mazda', N'Mazdaspeed', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2856, N'Mazda', N'Metro', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2857, N'Mazda', N'Miata', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2858, N'Mazda', N'Millenia', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2859, N'Mazda', N'Mizer', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2860, N'Mazda', N'MPV', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2861, N'Mazda', N'MX 3', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2862, N'Mazda', N'MX-3 Precidia', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2863, N'Mazda', N'MX-5', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2864, N'Mazda', N'MX-6', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2865, N'Mazda', N'Navajo', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2866, N'Mazda', N'Precidia', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2867, N'Mazda', N'Premacy', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2868, N'Mazda', N'Protegé', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2869, N'Mazda', N'Protegé5', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2870, N'Mazda', N'RX-3', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2871, N'Mazda', N'RX-4', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2872, N'Mazda', N'RX-7', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2873, N'Mazda', N'RX-8', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2874, N'Mazda', N'Tribute', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2875, N'Mazda', N'Truck (B-series)', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2876, N'Mazda', N'Van', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2877, N'Mazda', N'Xedos 9', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2878, N'Mercedes Benz', NULL, 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2879, N'Mercedes Benz', N'100 Van', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2880, N'Mercedes Benz', N'103', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2881, N'Mercedes Benz', N'107', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2882, N'Mercedes Benz', N'123', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2883, N'Mercedes Benz', N'124', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2884, N'Mercedes Benz', N'126', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2885, N'Mercedes Benz', N'190', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2886, N'Mercedes Benz', N'207', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2887, N'Mercedes Benz', N'260', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2888, N'Mercedes Benz', N'280', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2889, N'Mercedes Benz', N'300', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2890, N'Mercedes Benz', N'380', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2891, N'Mercedes Benz', N'420', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2892, N'Mercedes Benz', N'450', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2893, N'Mercedes Benz', N'500', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2894, N'Mercedes Benz', N'560', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2895, N'Mercedes Benz', N'A-Class', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2896, N'Mercedes Benz', N'Actros', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2897, N'Mercedes Benz', N'Atego', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2898, N'Mercedes Benz', N'B-Class', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2899, N'Mercedes Benz', N'B200', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2900, N'Mercedes Benz', N'B200 Turbo', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2901, N'Mercedes Benz', N'C-Class', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2902, N'Mercedes Benz', N'C220', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2903, N'Mercedes Benz', N'C230 Coupe', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2904, N'Mercedes Benz', N'C230', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2905, N'Mercedes Benz', N'C240', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2906, N'Mercedes Benz', N'C280', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2907, N'Mercedes Benz', N'C32', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2908, N'Mercedes Benz', N'C320', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2909, N'Mercedes Benz', N'C350', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2910, N'Mercedes Benz', N'C36', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2911, N'Mercedes Benz', N'C43', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2912, N'Mercedes Benz', N'C55', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2913, N'Mercedes Benz', N'CL-Class', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2914, N'Mercedes Benz', N'CL500', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2915, N'Mercedes Benz', N'CL55', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2916, N'Mercedes Benz', N'CL600', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2917, N'Mercedes Benz', N'CL63', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2918, N'Mercedes Benz', N'CL65', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2919, N'Mercedes Benz', N'CLK-Class', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2920, N'Mercedes Benz', N'CLS', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2921, N'Mercedes Benz', N'CLS-Class', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2922, N'Mercedes Benz', N'E-Class', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2923, N'Mercedes Benz', N'E320', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2924, N'Mercedes Benz', N'E430', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2925, N'Mercedes Benz', N'E500', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2926, N'Mercedes Benz', N'E55', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2927, N'Mercedes Benz', N'E63', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2928, N'Mercedes Benz', N'G-Class', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2929, N'Mercedes Benz', N'G500', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2930, N'Mercedes Benz', N'G55', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2931, N'Mercedes Benz', N'Geländewagen', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
GO
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2932, N'Mercedes Benz', N'M-Class', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2933, N'Mercedes Benz', N'ML320', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2934, N'Mercedes Benz', N'ML350', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2935, N'Mercedes Benz', N'ML430', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2936, N'Mercedes Benz', N'ML500', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2937, N'Mercedes Benz', N'ML55', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2938, N'Mercedes Benz', N'ML63', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2939, N'Mercedes Benz', N'R-Class', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2940, N'Mercedes Benz', N'R350', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2941, N'Mercedes Benz', N'R500', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2942, N'Mercedes Benz', N'R63', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2943, N'Mercedes Benz', N'S-Class', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2944, N'Mercedes Benz', N'S320', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2945, N'Mercedes Benz', N'S420', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2946, N'Mercedes Benz', N'S430', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2947, N'Mercedes Benz', N'S500', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2948, N'Mercedes Benz', N'S55', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2949, N'Mercedes Benz', N'S600', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2950, N'Mercedes Benz', N'SK-Serie (Truck)', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2951, N'Mercedes Benz', N'SL320', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2952, N'Mercedes Benz', N'SL500', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2953, N'Mercedes Benz', N'SL55', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2954, N'Mercedes Benz', N'SL600', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2955, N'Mercedes Benz', N'SL65', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2956, N'Mercedes Benz', N'SL-Class', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2957, N'Mercedes Benz', N'SLK230', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2958, N'Mercedes Benz', N'SLK280', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2959, N'Mercedes Benz', N'SLK320', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2960, N'Mercedes Benz', N'SLK350', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2961, N'Mercedes Benz', N'SLK55', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2962, N'Mercedes Benz', N'SLK-Class', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2963, N'Mercedes Benz', N'SLR McLaren', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2964, N'Mercedes Benz', N'Sprinter', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2965, N'Mercedes Benz', N'Unimog', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2966, N'Mercedes Benz', N'Vaneo', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2967, N'Mercedes Benz', N'V-Class', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2968, N'Mercedes Benz', N'Vito', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2969, N'Mercedes Benz', N'Viano', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2970, N'Mitsubishi', NULL, 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2971, N'Mitsubishi', N'3000GT', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2972, N'Mitsubishi', N'Canter', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2973, N'Mitsubishi', N'Carisma', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2974, N'Mitsubishi', N'Challenger', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2975, N'Mitsubishi', N'Colt', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2976, N'Mitsubishi', N'Conquest', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2977, N'Mitsubishi', N'Cosmos', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2978, N'Mitsubishi', N'Diamante', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2979, N'Mitsubishi', N'Eclipse', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2980, N'Mitsubishi', N'Eclipse Spyder', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2981, N'Mitsubishi', N'Endeavor', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2982, N'Mitsubishi', N'Evolution', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2983, N'Mitsubishi', N'Expo', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2984, N'Mitsubishi', N'Express', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2985, N'Mitsubishi', N'Galant', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2986, N'Mitsubishi', N'Grandis', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2987, N'Mitsubishi', N'L200/Pick Up', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2988, N'Mitsubishi', N'L300', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2989, N'Mitsubishi', N'L400', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2990, N'Mitsubishi', N'Lancer', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2991, N'Mitsubishi', N'Lancer Evolution', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2992, N'Mitsubishi', N'Lancer Ralliart', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2993, N'Mitsubishi', N'Lancer Sportback', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2994, N'Mitsubishi', N'Magna', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2995, N'Mitsubishi', N'Mighty Max', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2996, N'Mitsubishi', N'Mirage', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2997, N'Mitsubishi', N'Montero', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2998, N'Mitsubishi', N'Montero Sport', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (2999, N'Mitsubishi', N'Outlander', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3000, N'Mitsubishi', N'Pajero /Wagon', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3001, N'Mitsubishi', N'Pajero i0', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3002, N'Mitsubishi', N'Pajero Pinin', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3003, N'Mitsubishi', N'Pajero Sport', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3004, N'Mitsubishi', N'Precis', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3005, N'Mitsubishi', N'Raider', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3006, N'Mitsubishi', N'Shogun', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3007, N'Mitsubishi', N'Shogun Pinin', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3008, N'Mitsubishi', N'Shogun Sport', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3009, N'Mitsubishi', N'Sigma', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3010, N'Mitsubishi', N'Space Gear', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3011, N'Mitsubishi', N'Space Runner', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3012, N'Mitsubishi', N'Space Star', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3013, N'Mitsubishi', N'Space Van', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3014, N'Mitsubishi', N'Space Wagon', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3015, N'Mitsubishi', N'Starion', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3016, N'Mitsubishi', N'Triton', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3017, N'Mitsubishi', N'Trojan', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3018, N'Mitsubishi', N'Verada', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3019, N'Nissan', NULL, 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3020, N'Nissan', N'100NX', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3021, N'Nissan', N'200SX', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3022, N'Nissan', N'240SX', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3023, N'Nissan', N'240Z', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3024, N'Nissan', N'260Z', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3025, N'Nissan', N'280Z', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3026, N'Nissan', N'280ZX', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3027, N'Nissan', N'300ZX', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3028, N'Nissan', N'350Z', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3029, N'Nissan', N'Almera Tino', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3030, N'Nissan', N'Almera', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3031, N'Nissan', N'Altima', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
GO
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3032, N'Nissan', N'Armada', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3033, N'Nissan', N'Axxess', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3034, N'Nissan', N'Bluebird Sedan', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3035, N'Nissan', N'Camionnette', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3036, N'Nissan', N'Cargo', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3037, N'Nissan', N'Cefiro 2000/3000', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3038, N'Nissan', N'Costaud', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3039, N'Nissan', N'Diesel MK/LK (Truck)', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3040, N'Nissan', N'Frontier', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3041, N'Nissan', N'Hustler', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3042, N'Nissan', N'Interstar', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3043, N'Nissan', N'King Cab', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3044, N'Nissan', N'King Van', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3045, N'Nissan', N'Kubistar', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3046, N'Nissan', N'L35', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3047, N'Nissan', N'L50', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3048, N'Nissan', N'L60', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3049, N'Nissan', N'Maxima', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3050, N'Nissan', N'Micra', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3051, N'Nissan', N'Multi', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3052, N'Nissan', N'Murano', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3053, N'Nissan', N'Navara', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3054, N'Nissan', N'Note', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3055, N'Nissan', N'NX', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3056, N'Nissan', N'Pathfinder Armada', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3057, N'Nissan', N'Pathfinder', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3058, N'Nissan', N'Patrol', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3059, N'Nissan', N'Pickup', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3060, N'Nissan', N'Primastar', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3061, N'Nissan', N'Primera', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3062, N'Nissan', N'Pulsar NX', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3063, N'Nissan', N'Pulsar', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3064, N'Nissan', N'Quest', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3065, N'Nissan', N'Roadster', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3066, N'Nissan', N'Sentra Classic', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3067, N'Nissan', N'Sentra', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3068, N'Nissan', N'Serena', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3069, N'Nissan', N'Skyline', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3070, N'Nissan', N'Stanza', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3071, N'Nissan', N'Sunny Combi', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3072, N'Nissan', N'Sunny Sedan', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3073, N'Nissan', N'Sunny Van', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3074, N'Nissan', N'Terrano II', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3075, N'Nissan', N'Terrano', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3076, N'Nissan', N'Titan', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3077, N'Nissan', N'Vanette', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3078, N'Nissan', N'Versa', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3079, N'Nissan', N'Xterra', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3080, N'Nissan', N'X-Trail', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3081, N'Peugeot', NULL, 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3082, N'Peugeot', N'1007', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3083, N'Peugeot', N'106', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3084, N'Peugeot', N'107', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3085, N'Peugeot', N'205', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3086, N'Peugeot', N'206', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3087, N'Peugeot', N'306', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3088, N'Peugeot', N'307', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3089, N'Peugeot', N'307 SW', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3090, N'Peugeot', N'309', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3091, N'Peugeot', N'405', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3092, N'Peugeot', N'406', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3093, N'Peugeot', N'407', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3094, N'Peugeot', N'504', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3095, N'Peugeot', N'505', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3096, N'Peugeot', N'604', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3097, N'Peugeot', N'605', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3098, N'Peugeot', N'607', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3099, N'Peugeot', N'806', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3100, N'Peugeot', N'807', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3101, N'Peugeot', N'Boxer', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3102, N'Peugeot', N'Expert', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3103, N'Peugeot', N'J5', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3104, N'Peugeot', N'Partner', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3105, N'Seat', NULL, 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3106, N'Seat', N'Alhambra', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3107, N'Seat', N'Altea', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3108, N'Seat', N'Arosa', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3109, N'Seat', N'Cordoba', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3110, N'Seat', N'Ibiza', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3111, N'Seat', N'Inca', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3112, N'Seat', N'Leon', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3113, N'Seat', N'Toledo', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3114, N'Seat', N'Vario', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3115, N'Skoda', NULL, 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3116, N'Skoda', N'120', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3117, N'Skoda', N'130', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3118, N'Skoda', N'Fabia', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3119, N'Skoda', N'Favorit', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3120, N'Skoda', N'Felicia', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3121, N'Skoda', N'Octavia', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3122, N'Skoda', N'Octavia Ambiente', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3123, N'Skoda', N'Octavia Classic', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3124, N'Skoda', N'Octavia Elegance', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3125, N'Skoda', N'Octavia LX', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3126, N'Skoda', N'Octavia RS', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3127, N'Skoda', N'Octavia SLX', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3128, N'Skoda', N'Pickup', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3129, N'Skoda', N'Rapid', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3130, N'Skoda', N'Superb', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3131, N'Suzuki', NULL, 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
GO
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3132, N'Suzuki', N'Aerio', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3133, N'Suzuki', N'Alto', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3134, N'Suzuki', N'Baleno', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3135, N'Suzuki', N'Esteem', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3136, N'Suzuki', N'Forsa', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3137, N'Suzuki', N'Grand Vitara', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3138, N'Suzuki', N'Ignis', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3139, N'Suzuki', N'Jimny', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3140, N'Suzuki', N'Liana', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3141, N'Suzuki', N'Reno', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3142, N'Suzuki', N'Samurai', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3143, N'Suzuki', N'Sidekick', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3144, N'Suzuki', N'SJ410', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3145, N'Suzuki', N'Swift', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3146, N'Suzuki', N'Swift+', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3147, N'Suzuki', N'SX4', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3148, N'Suzuki', N'Verona', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3149, N'Suzuki', N'Vitara', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3150, N'Suzuki', N'Wagon', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3151, N'Suzuki', N'X-90', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3152, N'Suzuki', N'XL7', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3153, N'Toyota', NULL, 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3154, N'Toyota', N'4Runner', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3155, N'Toyota', N'Amazon', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3156, N'Toyota', N'Avalon', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3157, N'Toyota', N'Avensis', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3158, N'Toyota', N'Avensis Verso', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3159, N'Toyota', N'AYGO', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3160, N'Toyota', N'Camionnette', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3161, N'Toyota', N'Camry', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3162, N'Toyota', N'Camry Solara', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3163, N'Toyota', N'Carina', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3164, N'Toyota', N'Celica', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3165, N'Toyota', N'Celica Supra', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3166, N'Toyota', N'Colorado', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3167, N'Toyota', N'Corolla', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3168, N'Toyota', N'Corolla Verso', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3169, N'Toyota', N'Corona', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3170, N'Toyota', N'Cressida', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3171, N'Toyota', N'Echo', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3172, N'Toyota', N'FJ Cruiser', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3173, N'Toyota', N'Fourgonnette', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3174, N'Toyota', N'HiAce', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3175, N'Toyota', N'Highlander', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3176, N'Toyota', N'HiLux', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3177, N'Toyota', N'HiLux Tundra', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3178, N'Toyota', N'Kluger', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3179, N'Toyota', N'Land Cruiser', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3180, N'Toyota', N'Land Cruiser 100', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3181, N'Toyota', N'Land Cruiser 70', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3182, N'Toyota', N'Land Cruiser 90', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3183, N'Toyota', N'Land Cruiser Amazon', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3184, N'Toyota', N'Land Cruiser Hg 80', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3185, N'Toyota', N'Lite Ace', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3186, N'Toyota', N'Mark II', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3187, N'Toyota', N'Matrix', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3188, N'Toyota', N'MR2', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3189, N'Toyota', N'MR2 Spyder', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3190, N'Toyota', N'Paseo', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3191, N'Toyota', N'Pickup', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3192, N'Toyota', N'Picnic', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3193, N'Toyota', N'Prado', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3194, N'Toyota', N'Prerunner', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3195, N'Toyota', N'Previa', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3196, N'Toyota', N'Prius', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3197, N'Toyota', N'RAV4', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3198, N'Toyota', N'Scion', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3199, N'Toyota', N'Sequoia', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3200, N'Toyota', N'Sienna', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3201, N'Toyota', N'Solara', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3202, N'Toyota', N'SportsVan', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3203, N'Toyota', N'Starlet', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3204, N'Toyota', N'Station Wagon', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3205, N'Toyota', N'Supra', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3206, N'Toyota', N'T100', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3207, N'Toyota', N'T1000', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3208, N'Toyota', N'Tacoma', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3209, N'Toyota', N'Tarago', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3210, N'Toyota', N'Tercel', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3211, N'Toyota', N'Town Ace', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3212, N'Toyota', N'Traveler', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3213, N'Toyota', N'Tundra', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3214, N'Toyota', N'Van', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3215, N'Toyota', N'Yaris', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3216, N'Toyota', N'Yaris Verso', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3217, N'Volkswagen', NULL, 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3218, N'Volkswagen', N'Beetle', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3219, N'Volkswagen', N'Bora', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3220, N'Volkswagen', N'Bus', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3221, N'Volkswagen', N'Cabriolet', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3222, N'Volkswagen', N'Caddy', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3223, N'Volkswagen', N'Caddy Pickup', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3224, N'Volkswagen', N'Caddy Van', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3225, N'Volkswagen', N'Caravelle', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3226, N'Volkswagen', N'Carrier', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3227, N'Volkswagen', N'City Golf', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3228, N'Volkswagen', N'City Jetta', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3229, N'Volkswagen', N'Colorado', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3230, N'Volkswagen', N'Combi', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3231, N'Volkswagen', N'Corrado', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
GO
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3232, N'Volkswagen', N'Dasher', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3233, N'Volkswagen', N'Eos', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3234, N'Volkswagen', N'Eurovan', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3235, N'Volkswagen', N'Fox', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3236, N'Volkswagen', N'Golf', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3237, N'Volkswagen', N'Golf Cabriolet', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3238, N'Volkswagen', N'Golf I', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3239, N'Volkswagen', N'Golf II', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3240, N'Volkswagen', N'Golf III', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3241, N'Volkswagen', N'Golf III Cabrio', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3242, N'Volkswagen', N'Golf III Variant', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3243, N'Volkswagen', N'Golf IV', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3244, N'Volkswagen', N'Golf IV Variant', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3245, N'Volkswagen', N'Golf Plus', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3246, N'Volkswagen', N'Golf V', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3247, N'Volkswagen', N'GTI', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3248, N'Volkswagen', N'Jetta', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3249, N'Volkswagen', N'Jetta Wagon', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3250, N'Volkswagen', N'Karmann Ghia', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3251, N'Volkswagen', N'LT28/LT31/LT35/LT46', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3252, N'Volkswagen', N'Lupo', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3253, N'Volkswagen', N'Multivan', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3254, N'Volkswagen', N'New Beetle', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3255, N'Volkswagen', N'New Beetle Cabrio', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3256, N'Volkswagen', N'Passat', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3257, N'Volkswagen', N'Phaeton', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3258, N'Volkswagen', N'Polo', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3259, N'Volkswagen', N'Polo Classic', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3260, N'Volkswagen', N'Polo Sedan', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3261, N'Volkswagen', N'Polo Variant', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3262, N'Volkswagen', N'Quantum', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3263, N'Volkswagen', N'Rabbit', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3264, N'Volkswagen', N'Scirocco', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3265, N'Volkswagen', N'Sharan', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3266, N'Volkswagen', N'Shuttle', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3267, N'Volkswagen', N'T4 Pickup', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3268, N'Volkswagen', N'T4 Transporter', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3269, N'Volkswagen', N'T5 Pickup', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3270, N'Volkswagen', N'T5 Transporter', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3271, N'Volkswagen', N'Taro', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3272, N'Volkswagen', N'Thing', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3273, N'Volkswagen', N'Touareg', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3274, N'Volkswagen', N'Touran', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3275, N'Volkswagen', N'Transporter', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3276, N'Volkswagen', N'Truck Bus', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3277, N'Volkswagen', N'Type II', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3278, N'Volkswagen', N'Type III', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3279, N'Volkswagen', N'Vanagon', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3280, N'Volkswagen', N'Vento', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3281, N'Subaru', NULL, 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3282, N'Subaru', N'4x4 Turbo', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3283, N'Subaru', N'B9 Tribeca', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3284, N'Subaru', N'Baja', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3285, N'Subaru', N'Brat', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3286, N'Subaru', N'DL', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3287, N'Subaru', N'Forester', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3288, N'Subaru', N'G3X Justy', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3289, N'Subaru', N'GL', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3290, N'Subaru', N'Impreza', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3291, N'Subaru', N'Justy', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3292, N'Subaru', N'Legacy', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3293, N'Subaru', N'Leone', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3294, N'Subaru', N'Liberty', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3295, N'Subaru', N'Loyale', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3296, N'Subaru', N'Outback', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3297, N'Subaru', N'Outback Sport', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3298, N'Subaru', N'Pic-Up', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3299, N'Subaru', N'STi', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3300, N'Subaru', N'Super Station', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3301, N'Subaru', N'SVX', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3302, N'Subaru', N'WRX', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3303, N'Subaru', N'WRX STi', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3304, N'Subaru', N'XT', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3305, N'Ssang Yong', NULL, 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3306, N'Ssang Yong', N'Korando', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3307, N'Ssang Yong', N'Musso', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3308, N'Ssang Yong', N'Rexton', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3309, N'MG', NULL, 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3310, N'MG', N'F', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3311, N'MG', N'Metro', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3312, N'MG', N'MGA', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3313, N'MG', N'MGB', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3314, N'MG', N'MGC', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3315, N'MG', N'MGF', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3316, N'MG', N'MGT', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3317, N'MG', N'MGTD', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3318, N'MG', N'Midget', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3319, N'MG', N'RV8', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3320, N'MG', N'TA', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3321, N'MG', N'TB', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3322, N'MG', N'TC', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3323, N'MG', N'TD', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3324, N'MG', N'TF', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3325, N'MG', N'X80', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3326, N'MG', N'ZF', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3327, N'MG', N'ZR', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3328, N'MG', N'ZS', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3329, N'MG', N'ZT', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3330, N'Renault', NULL, 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3331, N'Renault', N'12', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
GO
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3332, N'Renault', N'17', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3333, N'Renault', N'18', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3334, N'Renault', N'19', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3335, N'Renault', N'21', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3336, N'Renault', N'25', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3337, N'Renault', N'30', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3338, N'Renault', N'5', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3339, N'Renault', N'Alliance', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3340, N'Renault', N'Avantime', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3341, N'Renault', N'Clio', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3342, N'Renault', N'Encore', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3343, N'Renault', N'Espace', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3344, N'Renault', N'Express', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3345, N'Renault', N'Fuego', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3346, N'Renault', N'Kangoo', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3347, N'Renault', N'Laguna', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3348, N'Renault', N'LeCar', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3349, N'Renault', N'Magnum', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3350, N'Renault', N'Mascot', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3351, N'Renault', N'Master', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3352, N'Renault', N'Medaillon', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3353, N'Renault', N'Mégane', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3354, N'Renault', N'Midlum', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3355, N'Renault', N'Modus', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3356, N'Renault', N'Premium', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3357, N'Renault', N'RX4', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3358, N'Renault', N'Safrane', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3359, N'Renault', N'Scénic', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3360, N'Renault', N'Sportwagon', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3361, N'Renault', N'Traffic', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3362, N'Renault', N'Twingo', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3363, N'Renault', N'VelSatis', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3364, N'Volvo', NULL, 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3365, N'Volvo', N'160', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3366, N'Volvo', N'240', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3367, N'Volvo', N'242', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3368, N'Volvo', N'244', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3369, N'Volvo', N'245', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3370, N'Volvo', N'260', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3371, N'Volvo', N'340', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3372, N'Volvo', N'360', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3373, N'Volvo', N'440', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3374, N'Volvo', N'460', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3375, N'Volvo', N'480', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3376, N'Volvo', N'740', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3377, N'Volvo', N'760', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3378, N'Volvo', N'780', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3379, N'Volvo', N'850', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3380, N'Volvo', N'940', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3381, N'Volvo', N'960', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3382, N'Volvo', N'Bertone', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3383, N'Volvo', N'C30', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3384, N'Volvo', N'C70', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3385, N'Volvo', N'Cross Country', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3386, N'Volvo', N'F10', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3387, N'Volvo', N'F12', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3388, N'Volvo', N'FH', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3389, N'Volvo', N'FL10', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3390, N'Volvo', N'FL6', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3391, N'Volvo', N'FL7', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3392, N'Volvo', N'FM', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3393, N'Volvo', N'FS7', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3394, N'Volvo', N'NH', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3395, N'Volvo', N'P1800', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3396, N'Volvo', N'R', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3397, N'Volvo', N'S40', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3398, N'Volvo', N'S60', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3399, N'Volvo', N'S60 R', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3400, N'Volvo', N'S70', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3401, N'Volvo', N'S80', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3402, N'Volvo', N'S90', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3403, N'Volvo', N'V40', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3404, N'Volvo', N'V50', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3405, N'Volvo', N'V70', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3406, N'Volvo', N'V70 R', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3407, N'Volvo', N'V90', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3408, N'Volvo', N'VHD', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3409, N'Volvo', N'VN420', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3410, N'Volvo', N'VN610', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3411, N'Volvo', N'VN630', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3412, N'Volvo', N'VN660', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3413, N'Volvo', N'VN670', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3414, N'Volvo', N'VN770', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3415, N'Volvo', N'VN780', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3416, N'Volvo', N'VNL', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3417, N'Volvo', N'VNM Day Cab', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3418, N'Volvo', N'XC70', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3419, N'Volvo', N'XC90', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3420, N'Rolls Royce', NULL, 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3421, N'Rolls Royce', N'Corniche', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3422, N'Rolls Royce', N'Flying Spur', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3423, N'Rolls Royce', N'Park Ward', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3424, N'Rolls Royce', N'Phantom', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3425, N'Rolls Royce', N'Silver Cloud', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3426, N'Rolls Royce', N'Silver Dawn', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3427, N'Rolls Royce', N'Silver Seraph', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3428, N'Rolls Royce', N'Silver Shadow', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3429, N'Rolls Royce', N'Silver Spirit', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3430, N'Rolls Royce', N'Silver Spur', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3431, N'Rolls Royce', N'Silver Wraith', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
GO
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3432, N'Rolls Royce', N'Spirit', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3433, N'Rolls Royce', N'Spur', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3434, N'Rolls Royce', N'Wraith', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3435, N'Opel', NULL, 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3436, N'Opel', N'Agila', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3437, N'Opel', N'Ascona', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3438, N'Opel', N'Astra', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3439, N'Opel', N'Calibra', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3440, N'Opel', N'Campo', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3441, N'Opel', N'Combo', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3442, N'Opel', N'Commodore', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3443, N'Opel', N'Corsa', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3444, N'Opel', N'Frontera', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3445, N'Opel', N'Kadett', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3446, N'Opel', N'Meriva', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3447, N'Opel', N'Monterey', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3448, N'Opel', N'Movano', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3449, N'Opel', N'Omega', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3450, N'Opel', N'Rekord', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3451, N'Opel', N'Senator', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3452, N'Opel', N'Signum', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3453, N'Opel', N'Sintra', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3454, N'Opel', N'Tigra', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3455, N'Opel', N'Vectra', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3456, N'Opel', N'Vectra B', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3457, N'Opel', N'Vectra C', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3458, N'Opel', N'Vivaro', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3459, N'Opel', N'Zafira', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3460, N'Oldsmobile', NULL, 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3461, N'Oldsmobile', N'442', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3462, N'Oldsmobile', N'Achieva', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3463, N'Oldsmobile', N'Alero', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3464, N'Oldsmobile', N'Aurora', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3465, N'Oldsmobile', N'Bravada', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3466, N'Oldsmobile', N'Cabrio', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3467, N'Oldsmobile', N'Calais', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3468, N'Oldsmobile', N'Custom Cruiser', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3469, N'Oldsmobile', N'Cutlass', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3470, N'Oldsmobile', N'Cutlass Calais', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3471, N'Oldsmobile', N'Cutlass Ciera', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3472, N'Oldsmobile', N'Cutlass Cruiser', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3473, N'Oldsmobile', N'Cutlass Sedan', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3474, N'Oldsmobile', N'Cutlass Supreme', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3475, N'Oldsmobile', N'Delta 88', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3476, N'Oldsmobile', N'Eighty-eight (88)', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3477, N'Oldsmobile', N'Firenza', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3478, N'Oldsmobile', N'Intrigue', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3479, N'Oldsmobile', N'Ninety-eight (98)', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3480, N'Oldsmobile', N'Omega', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3481, N'Oldsmobile', N'Regency 98', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3482, N'Oldsmobile', N'Silhouette', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3483, N'Oldsmobile', N'Silhuette', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3484, N'Oldsmobile', N'Starfire', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3485, N'Oldsmobile', N'Toronado', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3486, N'Porsche', NULL, 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3487, N'Porsche', N'356', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3488, N'Porsche', N'550', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3489, N'Porsche', N'901', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3490, N'Porsche', N'911', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3491, N'Porsche', N'911 Turbo', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3492, N'Porsche', N'912', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3493, N'Porsche', N'914', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3494, N'Porsche', N'924', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3495, N'Porsche', N'928', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3496, N'Porsche', N'944', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3497, N'Porsche', N'959', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3498, N'Porsche', N'968', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3499, N'Porsche', N'996', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3500, N'Porsche', N'Boxster', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3501, N'Porsche', N'Carrera', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3502, N'Porsche', N'Carrera 911S', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3503, N'Porsche', N'Carrera GT', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3504, N'Porsche', N'Cayenne', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3505, N'Porsche', N'Cayman', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3506, N'Porsche', N'Speedster', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3507, N'Porsche', N'Spyder', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3508, N'Pontiac', NULL, 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3509, N'Pontiac', N'1000', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3510, N'Pontiac', N'6000', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3511, N'Pontiac', N'Acadian', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3512, N'Pontiac', N'Astre', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3513, N'Pontiac', N'Aztek', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3514, N'Pontiac', N'Bonneville', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3515, N'Pontiac', N'Catalina', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3516, N'Pontiac', N'Fiero', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3517, N'Pontiac', N'Firebird', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3518, N'Pontiac', N'Firefly', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3519, N'Pontiac', N'Fleetleader', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3520, N'Pontiac', N'Formula', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3521, N'Pontiac', N'G5', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3522, N'Pontiac', N'G6', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3523, N'Pontiac', N'Grand Am', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3524, N'Pontiac', N'Grand LeMans', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3525, N'Pontiac', N'Grand Prix', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3526, N'Pontiac', N'GTO', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3527, N'Pontiac', N'J2000', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3528, N'Pontiac', N'LeMans', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3529, N'Pontiac', N'Montana', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3530, N'Pontiac', N'Montana SV6', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3531, N'Pontiac', N'Parisienne', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
GO
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3532, N'Pontiac', N'Phoenix', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3533, N'Pontiac', N'Pursuit', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3534, N'Pontiac', N'Safari', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3535, N'Pontiac', N'Scooter', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3536, N'Pontiac', N'Solstice', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3537, N'Pontiac', N'Sunbird', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3538, N'Pontiac', N'Sunburst', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3539, N'Pontiac', N'Sunfire', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3540, N'Pontiac', N'Sunrunner', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3541, N'Pontiac', N'Tempest', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3542, N'Pontiac', N'Torrent', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3543, N'Pontiac', N'Trans Am', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3544, N'Pontiac', N'Trans Sport', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3545, N'Pontiac', N'Ventura', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3546, N'Pontiac', N'Vibe', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3547, N'Pontiac', N'Wave', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3548, N'Mercury', NULL, 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3549, N'Mercury', N'Bobcat', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3550, N'Mercury', N'Capri', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3551, N'Mercury', N'Colony Park', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3552, N'Mercury', N'Comet', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3553, N'Mercury', N'Cougar', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3554, N'Mercury', N'Cyclone', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3555, N'Mercury', N'Grand Marquis', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3556, N'Mercury', N'LN7', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3557, N'Mercury', N'Lynx', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3558, N'Mercury', N'Marauder', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3559, N'Mercury', N'Mariner', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3560, N'Mercury', N'Marquis', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3561, N'Mercury', N'Meteor', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3562, N'Mercury', N'Milan', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3563, N'Mercury', N'Monarch', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3564, N'Mercury', N'Monclair', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3565, N'Mercury', N'Montego', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3566, N'Mercury', N'Monterey', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3567, N'Mercury', N'Mountaineer', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3568, N'Mercury', N'Mystique', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3569, N'Mercury', N'Park Lane', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3570, N'Mercury', N'Sable', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3571, N'Mercury', N'Topaz', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3572, N'Mercury', N'Tracer', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3573, N'Mercury', N'Turnpike', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3574, N'Mercury', N'Villager', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3575, N'Mercury', N'Zephyr', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3576, N'Plymouth', NULL, 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3577, N'Plymouth', N'Acclaim', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3578, N'Plymouth', N'Arrow', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3579, N'Plymouth', N'Barracuda', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3580, N'Plymouth', N'Breeze', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3581, N'Plymouth', N'Caravelle', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3582, N'Plymouth', N'Colt', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3583, N'Plymouth', N'Coupe', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3584, N'Plymouth', N'Duster', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3585, N'Plymouth', N'Expo', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3586, N'Plymouth', N'Fury', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3587, N'Plymouth', N'Grand Fury', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3588, N'Plymouth', N'Grand Voyager', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3589, N'Plymouth', N'Horizon', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3590, N'Plymouth', N'Laser', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3591, N'Plymouth', N'Le Baron', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3592, N'Plymouth', N'Neon', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3593, N'Plymouth', N'Prowler', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3594, N'Plymouth', N'Reliant', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3595, N'Plymouth', N'RelSapporoiant', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3596, N'Plymouth', N'Saratoga', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3597, N'Plymouth', N'Satellite', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3598, N'Plymouth', N'Stratus', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3599, N'Plymouth', N'Sundance', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3600, N'Plymouth', N'TC3', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3601, N'Plymouth', N'Turismo', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3602, N'Plymouth', N'Volare', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3603, N'Plymouth', N'Voyager', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3604, N'Maserati', NULL, 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3605, N'Maserati', N'420', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3606, N'Maserati', N'430', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3607, N'Maserati', N'3200 GT', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3608, N'Maserati', N'3500 GT', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3609, N'Maserati', N'5000 GT', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3610, N'Maserati', N'Biturbo', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3611, N'Maserati', N'Bora', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3612, N'Maserati', N'Coupé', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3613, N'Maserati', N'Ghibli', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3614, N'Maserati', N'GranSport', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3615, N'Maserati', N'GranSport Spyder', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3616, N'Maserati', N'Khamsin', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3617, N'Maserati', N'MC12', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3618, N'Maserati', N'Merak', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3619, N'Maserati', N'Quattroporte', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3620, N'Maserati', N'Shamal', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3621, N'Maserati', N'Spyder', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3622, N'Saturn', NULL, 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3623, N'Saturn', N'Aura', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3624, N'Saturn', N'Ion', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3625, N'Saturn', N'Ion Quad Coupe', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3626, N'Saturn', N'L', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3627, N'Saturn', N'LS', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3628, N'Saturn', N'LW', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3629, N'Saturn', N'Outlook', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3630, N'Saturn', N'Relay', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3631, N'Saturn', N'S', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
GO
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3632, N'Saturn', N'SC', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3633, N'Saturn', N'Sky', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3634, N'Saturn', N'SL', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3635, N'Saturn', N'SW', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3636, N'Saturn', N'Vue', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3637, N'Lotus', NULL, 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3638, N'Lotus', N'Elan', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3639, N'Lotus', N'Elise', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3640, N'Lotus', N'Elite', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3641, N'Lotus', N'Esprit', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3642, N'Lotus', N'Europa', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3643, N'Lotus', N'Exige', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3644, N'Lotus', N'Super 7', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3645, N'Rover', NULL, 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3646, N'Rover', N'200/25', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3647, N'Rover', N'400/45', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3648, N'Rover', N'600', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3649, N'Rover', N'75', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3650, N'Rover', N'800', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3651, N'Rover', N'City Rover', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3652, N'Rover', N'SD', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3653, N'Rover', N'Street Wise', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3654, N'Saab', NULL, 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3655, N'Saab', N'900', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3656, N'Saab', N'9000', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3657, N'Saab', N'9-2x', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3658, N'Saab', N'9-3', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3659, N'Saab', N'9-5', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3660, N'Saab', N'9-6x', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3661, N'Saab', N'9-7x', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3662, N'Fiat', N'Fiorino', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3663, N'Suzuki', N'APV', 1, 2014, CAST(N'2014-04-25 22:47:35.383' AS DateTime), CAST(N'2014-04-25 22:47:35.383' AS DateTime), 1, 1, NULL)
INSERT [Vehicle].[MakeAndModel] ([MakeAndModelID], [Make], [Model], [VehicleTypeID], [Year], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active], [Description]) VALUES (3665, N'Mazda', N'CX-5', 1, 2015, CAST(N'2014-09-12 07:41:02.177' AS DateTime), CAST(N'2014-09-12 07:41:02.177' AS DateTime), 1, 1, NULL)
SET IDENTITY_INSERT [Vehicle].[MakeAndModel] OFF
SET IDENTITY_INSERT [Vehicle].[Motor] ON 

INSERT [Vehicle].[Motor] ([MotorId], [Name], [Value], [Description], [CreatedDate], [ModifiedDate], [ModifiedBy]) VALUES (1, N'1.8L', CAST(1.80 AS Decimal(5, 2)), NULL, CAST(N'2014-07-15 12:04:01.460' AS DateTime), CAST(N'2014-07-15 12:04:01.460' AS DateTime), 1)
INSERT [Vehicle].[Motor] ([MotorId], [Name], [Value], [Description], [CreatedDate], [ModifiedDate], [ModifiedBy]) VALUES (2, N'2.0L', CAST(2.00 AS Decimal(5, 2)), NULL, CAST(N'2014-08-29 13:49:20.493' AS DateTime), CAST(N'2014-08-29 13:49:20.493' AS DateTime), NULL)
INSERT [Vehicle].[Motor] ([MotorId], [Name], [Value], [Description], [CreatedDate], [ModifiedDate], [ModifiedBy]) VALUES (3, N'1.7L', CAST(1.70 AS Decimal(5, 2)), NULL, CAST(N'2014-08-29 13:49:31.540' AS DateTime), CAST(N'2014-08-29 13:49:31.540' AS DateTime), NULL)
INSERT [Vehicle].[Motor] ([MotorId], [Name], [Value], [Description], [CreatedDate], [ModifiedDate], [ModifiedBy]) VALUES (4, N'1.5L', CAST(1.50 AS Decimal(5, 2)), NULL, CAST(N'2014-08-29 13:49:38.043' AS DateTime), CAST(N'2014-08-29 13:49:38.043' AS DateTime), NULL)
INSERT [Vehicle].[Motor] ([MotorId], [Name], [Value], [Description], [CreatedDate], [ModifiedDate], [ModifiedBy]) VALUES (5, N'2.4L', CAST(2.40 AS Decimal(5, 2)), NULL, CAST(N'2014-08-29 13:49:43.333' AS DateTime), CAST(N'2014-08-29 13:49:43.333' AS DateTime), NULL)
INSERT [Vehicle].[Motor] ([MotorId], [Name], [Value], [Description], [CreatedDate], [ModifiedDate], [ModifiedBy]) VALUES (6, N'3.0L', CAST(3.00 AS Decimal(5, 2)), NULL, CAST(N'2014-08-29 13:50:04.710' AS DateTime), CAST(N'2014-08-29 13:50:04.710' AS DateTime), NULL)
INSERT [Vehicle].[Motor] ([MotorId], [Name], [Value], [Description], [CreatedDate], [ModifiedDate], [ModifiedBy]) VALUES (7, N'3.5L', CAST(3.50 AS Decimal(5, 2)), NULL, CAST(N'2014-08-29 13:50:17.707' AS DateTime), CAST(N'2014-08-29 13:50:17.707' AS DateTime), NULL)
INSERT [Vehicle].[Motor] ([MotorId], [Name], [Value], [Description], [CreatedDate], [ModifiedDate], [ModifiedBy]) VALUES (8, N'2.5L', CAST(2.50 AS Decimal(5, 2)), NULL, CAST(N'2014-08-29 13:51:45.810' AS DateTime), CAST(N'2014-08-29 13:51:45.810' AS DateTime), NULL)
INSERT [Vehicle].[Motor] ([MotorId], [Name], [Value], [Description], [CreatedDate], [ModifiedDate], [ModifiedBy]) VALUES (9, N'1.0L', CAST(1.00 AS Decimal(5, 2)), NULL, CAST(N'2014-09-11 10:33:53.173' AS DateTime), CAST(N'2014-09-11 10:33:53.173' AS DateTime), NULL)
INSERT [Vehicle].[Motor] ([MotorId], [Name], [Value], [Description], [CreatedDate], [ModifiedDate], [ModifiedBy]) VALUES (10, N'1.2L', CAST(1.20 AS Decimal(5, 2)), NULL, CAST(N'2014-09-11 10:34:01.823' AS DateTime), CAST(N'2014-09-11 10:34:01.823' AS DateTime), NULL)
INSERT [Vehicle].[Motor] ([MotorId], [Name], [Value], [Description], [CreatedDate], [ModifiedDate], [ModifiedBy]) VALUES (11, N'1.3L', CAST(1.30 AS Decimal(5, 2)), NULL, CAST(N'2014-09-11 10:34:09.280' AS DateTime), CAST(N'2014-09-11 10:34:09.280' AS DateTime), NULL)
INSERT [Vehicle].[Motor] ([MotorId], [Name], [Value], [Description], [CreatedDate], [ModifiedDate], [ModifiedBy]) VALUES (12, N'1.4L', CAST(1.40 AS Decimal(5, 2)), NULL, CAST(N'2014-09-11 10:34:18.400' AS DateTime), CAST(N'2014-09-11 10:34:18.400' AS DateTime), NULL)
INSERT [Vehicle].[Motor] ([MotorId], [Name], [Value], [Description], [CreatedDate], [ModifiedDate], [ModifiedBy]) VALUES (13, N'1.9L', CAST(1.90 AS Decimal(5, 2)), NULL, CAST(N'2014-09-11 10:34:38.847' AS DateTime), CAST(N'2014-09-11 10:34:38.847' AS DateTime), NULL)
INSERT [Vehicle].[Motor] ([MotorId], [Name], [Value], [Description], [CreatedDate], [ModifiedDate], [ModifiedBy]) VALUES (16, N'2.1L', CAST(2.10 AS Decimal(5, 2)), NULL, CAST(N'2014-09-11 10:34:54.177' AS DateTime), CAST(N'2014-09-11 10:34:54.177' AS DateTime), NULL)
INSERT [Vehicle].[Motor] ([MotorId], [Name], [Value], [Description], [CreatedDate], [ModifiedDate], [ModifiedBy]) VALUES (17, N'2.2L', CAST(2.20 AS Decimal(5, 2)), NULL, CAST(N'2014-09-11 10:35:38.310' AS DateTime), CAST(N'2014-09-11 10:35:38.310' AS DateTime), NULL)
INSERT [Vehicle].[Motor] ([MotorId], [Name], [Value], [Description], [CreatedDate], [ModifiedDate], [ModifiedBy]) VALUES (18, N'2.3L', CAST(2.30 AS Decimal(5, 2)), NULL, CAST(N'2014-09-11 10:35:48.307' AS DateTime), CAST(N'2014-09-11 10:35:48.307' AS DateTime), NULL)
INSERT [Vehicle].[Motor] ([MotorId], [Name], [Value], [Description], [CreatedDate], [ModifiedDate], [ModifiedBy]) VALUES (19, N'2.6L', CAST(2.60 AS Decimal(5, 2)), NULL, CAST(N'2014-09-11 10:35:58.593' AS DateTime), CAST(N'2014-09-11 10:35:58.593' AS DateTime), NULL)
INSERT [Vehicle].[Motor] ([MotorId], [Name], [Value], [Description], [CreatedDate], [ModifiedDate], [ModifiedBy]) VALUES (20, N'2.7L', CAST(2.70 AS Decimal(5, 2)), NULL, CAST(N'2014-09-11 10:36:06.460' AS DateTime), CAST(N'2014-09-11 10:36:06.460' AS DateTime), NULL)
INSERT [Vehicle].[Motor] ([MotorId], [Name], [Value], [Description], [CreatedDate], [ModifiedDate], [ModifiedBy]) VALUES (21, N'2.8L', CAST(2.80 AS Decimal(5, 2)), NULL, CAST(N'2014-09-11 10:36:14.117' AS DateTime), CAST(N'2014-09-11 10:36:14.117' AS DateTime), NULL)
INSERT [Vehicle].[Motor] ([MotorId], [Name], [Value], [Description], [CreatedDate], [ModifiedDate], [ModifiedBy]) VALUES (22, N'2.9L', CAST(2.90 AS Decimal(5, 2)), NULL, CAST(N'2014-09-11 10:36:19.303' AS DateTime), CAST(N'2014-09-11 10:36:19.303' AS DateTime), NULL)
INSERT [Vehicle].[Motor] ([MotorId], [Name], [Value], [Description], [CreatedDate], [ModifiedDate], [ModifiedBy]) VALUES (23, N'3.1L', CAST(3.10 AS Decimal(5, 2)), NULL, CAST(N'2014-09-11 10:36:49.590' AS DateTime), CAST(N'2014-09-11 10:36:49.590' AS DateTime), NULL)
INSERT [Vehicle].[Motor] ([MotorId], [Name], [Value], [Description], [CreatedDate], [ModifiedDate], [ModifiedBy]) VALUES (24, N'3.2L', CAST(3.20 AS Decimal(5, 2)), NULL, CAST(N'2014-09-11 10:37:02.930' AS DateTime), CAST(N'2014-09-11 10:37:02.930' AS DateTime), NULL)
INSERT [Vehicle].[Motor] ([MotorId], [Name], [Value], [Description], [CreatedDate], [ModifiedDate], [ModifiedBy]) VALUES (25, N'3.3L', CAST(3.30 AS Decimal(5, 2)), NULL, CAST(N'2014-09-11 10:37:08.343' AS DateTime), CAST(N'2014-09-11 10:37:08.343' AS DateTime), NULL)
INSERT [Vehicle].[Motor] ([MotorId], [Name], [Value], [Description], [CreatedDate], [ModifiedDate], [ModifiedBy]) VALUES (26, N'3.4L', CAST(3.40 AS Decimal(5, 2)), NULL, CAST(N'2014-09-11 10:37:16.417' AS DateTime), CAST(N'2014-09-11 10:37:16.417' AS DateTime), NULL)
INSERT [Vehicle].[Motor] ([MotorId], [Name], [Value], [Description], [CreatedDate], [ModifiedDate], [ModifiedBy]) VALUES (27, N'3.6L', CAST(3.60 AS Decimal(5, 2)), NULL, CAST(N'2014-09-11 10:37:21.647' AS DateTime), CAST(N'2014-09-11 10:37:21.647' AS DateTime), NULL)
INSERT [Vehicle].[Motor] ([MotorId], [Name], [Value], [Description], [CreatedDate], [ModifiedDate], [ModifiedBy]) VALUES (28, N'3.7L', CAST(3.70 AS Decimal(5, 2)), NULL, CAST(N'2014-09-11 10:37:29.527' AS DateTime), CAST(N'2014-09-11 10:37:29.527' AS DateTime), NULL)
INSERT [Vehicle].[Motor] ([MotorId], [Name], [Value], [Description], [CreatedDate], [ModifiedDate], [ModifiedBy]) VALUES (29, N'3.8L', CAST(3.80 AS Decimal(5, 2)), NULL, CAST(N'2014-09-11 10:37:39.980' AS DateTime), CAST(N'2014-09-11 10:37:39.980' AS DateTime), NULL)
INSERT [Vehicle].[Motor] ([MotorId], [Name], [Value], [Description], [CreatedDate], [ModifiedDate], [ModifiedBy]) VALUES (30, N'3.9L', CAST(3.90 AS Decimal(5, 2)), NULL, CAST(N'2014-09-11 10:37:49.310' AS DateTime), CAST(N'2014-09-11 10:37:49.310' AS DateTime), NULL)
INSERT [Vehicle].[Motor] ([MotorId], [Name], [Value], [Description], [CreatedDate], [ModifiedDate], [ModifiedBy]) VALUES (31, N'4.0L', CAST(4.00 AS Decimal(5, 2)), NULL, CAST(N'2014-09-11 10:38:10.600' AS DateTime), CAST(N'2014-09-11 10:38:10.600' AS DateTime), NULL)
INSERT [Vehicle].[Motor] ([MotorId], [Name], [Value], [Description], [CreatedDate], [ModifiedDate], [ModifiedBy]) VALUES (32, N'4.1L', CAST(4.10 AS Decimal(5, 2)), NULL, CAST(N'2014-09-11 10:38:17.193' AS DateTime), CAST(N'2014-09-11 10:38:17.193' AS DateTime), NULL)
INSERT [Vehicle].[Motor] ([MotorId], [Name], [Value], [Description], [CreatedDate], [ModifiedDate], [ModifiedBy]) VALUES (33, N'4.2L', CAST(4.20 AS Decimal(5, 2)), NULL, CAST(N'2014-09-11 10:38:24.247' AS DateTime), CAST(N'2014-09-11 10:38:24.247' AS DateTime), NULL)
INSERT [Vehicle].[Motor] ([MotorId], [Name], [Value], [Description], [CreatedDate], [ModifiedDate], [ModifiedBy]) VALUES (34, N'4.3L', CAST(4.30 AS Decimal(5, 2)), NULL, CAST(N'2014-09-11 10:38:31.820' AS DateTime), CAST(N'2014-09-11 10:38:31.820' AS DateTime), NULL)
INSERT [Vehicle].[Motor] ([MotorId], [Name], [Value], [Description], [CreatedDate], [ModifiedDate], [ModifiedBy]) VALUES (35, N'4.4L', CAST(4.40 AS Decimal(5, 2)), NULL, CAST(N'2014-09-11 10:38:36.583' AS DateTime), CAST(N'2014-09-11 10:38:36.583' AS DateTime), NULL)
INSERT [Vehicle].[Motor] ([MotorId], [Name], [Value], [Description], [CreatedDate], [ModifiedDate], [ModifiedBy]) VALUES (36, N'4.5L', CAST(4.40 AS Decimal(5, 2)), NULL, CAST(N'2014-09-11 10:38:41.200' AS DateTime), CAST(N'2014-09-11 10:38:41.200' AS DateTime), NULL)
INSERT [Vehicle].[Motor] ([MotorId], [Name], [Value], [Description], [CreatedDate], [ModifiedDate], [ModifiedBy]) VALUES (37, N'4.6L', CAST(4.40 AS Decimal(5, 2)), NULL, CAST(N'2014-09-11 10:38:47.480' AS DateTime), CAST(N'2014-09-11 10:38:47.480' AS DateTime), NULL)
INSERT [Vehicle].[Motor] ([MotorId], [Name], [Value], [Description], [CreatedDate], [ModifiedDate], [ModifiedBy]) VALUES (38, N'4.7L', CAST(4.70 AS Decimal(5, 2)), NULL, CAST(N'2014-09-11 10:38:53.403' AS DateTime), CAST(N'2014-09-11 10:38:53.403' AS DateTime), NULL)
INSERT [Vehicle].[Motor] ([MotorId], [Name], [Value], [Description], [CreatedDate], [ModifiedDate], [ModifiedBy]) VALUES (39, N'4.8L', CAST(4.80 AS Decimal(5, 2)), NULL, CAST(N'2014-09-11 10:40:07.853' AS DateTime), CAST(N'2014-09-11 10:40:07.853' AS DateTime), NULL)
INSERT [Vehicle].[Motor] ([MotorId], [Name], [Value], [Description], [CreatedDate], [ModifiedDate], [ModifiedBy]) VALUES (40, N'4.9L', CAST(4.90 AS Decimal(5, 2)), NULL, CAST(N'2014-09-11 10:40:20.993' AS DateTime), CAST(N'2014-09-11 10:40:20.993' AS DateTime), NULL)
INSERT [Vehicle].[Motor] ([MotorId], [Name], [Value], [Description], [CreatedDate], [ModifiedDate], [ModifiedBy]) VALUES (41, N'5.0L', CAST(5.00 AS Decimal(5, 2)), NULL, CAST(N'2014-09-11 10:40:28.703' AS DateTime), CAST(N'2014-09-11 10:40:28.703' AS DateTime), NULL)
INSERT [Vehicle].[Motor] ([MotorId], [Name], [Value], [Description], [CreatedDate], [ModifiedDate], [ModifiedBy]) VALUES (42, N'5.1L', CAST(5.10 AS Decimal(5, 2)), NULL, CAST(N'2014-09-11 10:40:45.163' AS DateTime), CAST(N'2014-09-11 10:40:45.163' AS DateTime), NULL)
INSERT [Vehicle].[Motor] ([MotorId], [Name], [Value], [Description], [CreatedDate], [ModifiedDate], [ModifiedBy]) VALUES (43, N'5.2L', CAST(5.20 AS Decimal(5, 2)), NULL, CAST(N'2014-09-11 10:40:51.117' AS DateTime), CAST(N'2014-09-11 10:40:51.117' AS DateTime), NULL)
INSERT [Vehicle].[Motor] ([MotorId], [Name], [Value], [Description], [CreatedDate], [ModifiedDate], [ModifiedBy]) VALUES (44, N'5.3L', CAST(5.30 AS Decimal(5, 2)), NULL, CAST(N'2014-09-11 10:40:56.180' AS DateTime), CAST(N'2014-09-11 10:40:56.180' AS DateTime), NULL)
INSERT [Vehicle].[Motor] ([MotorId], [Name], [Value], [Description], [CreatedDate], [ModifiedDate], [ModifiedBy]) VALUES (45, N'5.4L', CAST(5.40 AS Decimal(5, 2)), NULL, CAST(N'2014-09-11 10:43:04.190' AS DateTime), CAST(N'2014-09-11 10:43:04.190' AS DateTime), NULL)
INSERT [Vehicle].[Motor] ([MotorId], [Name], [Value], [Description], [CreatedDate], [ModifiedDate], [ModifiedBy]) VALUES (46, N'5.5L', CAST(5.50 AS Decimal(5, 2)), NULL, CAST(N'2014-09-11 10:43:13.313' AS DateTime), CAST(N'2014-09-11 10:43:13.313' AS DateTime), NULL)
INSERT [Vehicle].[Motor] ([MotorId], [Name], [Value], [Description], [CreatedDate], [ModifiedDate], [ModifiedBy]) VALUES (47, N'5.6L', CAST(5.60 AS Decimal(5, 2)), NULL, CAST(N'2014-09-11 10:43:19.277' AS DateTime), CAST(N'2014-09-11 10:43:19.277' AS DateTime), NULL)
INSERT [Vehicle].[Motor] ([MotorId], [Name], [Value], [Description], [CreatedDate], [ModifiedDate], [ModifiedBy]) VALUES (48, N'5.7L', CAST(5.70 AS Decimal(5, 2)), NULL, CAST(N'2014-09-11 10:43:23.890' AS DateTime), CAST(N'2014-09-11 10:43:23.890' AS DateTime), NULL)
INSERT [Vehicle].[Motor] ([MotorId], [Name], [Value], [Description], [CreatedDate], [ModifiedDate], [ModifiedBy]) VALUES (49, N'5.8L', CAST(5.80 AS Decimal(5, 2)), NULL, CAST(N'2014-09-11 10:43:51.847' AS DateTime), CAST(N'2014-09-11 10:43:51.847' AS DateTime), NULL)
INSERT [Vehicle].[Motor] ([MotorId], [Name], [Value], [Description], [CreatedDate], [ModifiedDate], [ModifiedBy]) VALUES (50, N'5.9L', CAST(5.90 AS Decimal(5, 2)), NULL, CAST(N'2014-09-11 10:44:06.680' AS DateTime), CAST(N'2014-09-11 10:44:06.680' AS DateTime), NULL)
INSERT [Vehicle].[Motor] ([MotorId], [Name], [Value], [Description], [CreatedDate], [ModifiedDate], [ModifiedBy]) VALUES (51, N'6.0L', CAST(6.00 AS Decimal(5, 2)), NULL, CAST(N'2014-09-11 10:44:15.390' AS DateTime), CAST(N'2014-09-11 10:44:15.390' AS DateTime), NULL)
INSERT [Vehicle].[Motor] ([MotorId], [Name], [Value], [Description], [CreatedDate], [ModifiedDate], [ModifiedBy]) VALUES (52, N'1.6L', CAST(1.60 AS Decimal(5, 2)), NULL, CAST(N'2014-09-21 14:36:42.097' AS DateTime), CAST(N'2014-09-21 14:36:42.097' AS DateTime), NULL)
SET IDENTITY_INSERT [Vehicle].[Motor] OFF
SET IDENTITY_INSERT [Vehicle].[NotificatioModel] ON 

INSERT [Vehicle].[NotificatioModel] ([NotificatioModelID], [Name], [Value], [Description], [Created], [Modified], [ModifiedBy]) VALUES (1, N'Email', CAST(1.00 AS Decimal(5, 2)), N'Envia un Email al correo asignado', CAST(N'2014-07-15 09:25:06.323' AS DateTime), CAST(N'2014-07-15 09:25:06.323' AS DateTime), NULL)
INSERT [Vehicle].[NotificatioModel] ([NotificatioModelID], [Name], [Value], [Description], [Created], [Modified], [ModifiedBy]) VALUES (2, N'Email/SMS', CAST(2.00 AS Decimal(5, 2)), N'Envia un Email y SMS al correo asignado', CAST(N'2014-07-15 09:25:40.333' AS DateTime), CAST(N'2014-07-15 09:25:40.333' AS DateTime), NULL)
SET IDENTITY_INSERT [Vehicle].[NotificatioModel] OFF
SET IDENTITY_INSERT [Vehicle].[Photo] ON 

INSERT [Vehicle].[Photo] ([PhotoID], [VehicleID], [PhotoThumbnail], [ActualPhoto], [CreatedDate], [ModifiedDate], [MainPhoto]) VALUES (2, 2, N'\Uploads\photos\2\2009_Toyota_Corolla_XRS_DC.JPG', N'C:\Users\fv0419\Documents\Visual Studio 2012\Projects\eCars\eCars.WebUI\Uploads\photos\2\2009_Toyota_Corolla_XRS_DC.JPG', CAST(N'2014-08-27 16:02:48.010' AS DateTime), CAST(N'2014-08-27 16:02:48.010' AS DateTime), 0)
INSERT [Vehicle].[Photo] ([PhotoID], [VehicleID], [PhotoThumbnail], [ActualPhoto], [CreatedDate], [ModifiedDate], [MainPhoto]) VALUES (3, 2, N'\Uploads\photos\2\2009_toyota_corolla_s-pic-7544428267893186992.jpeg', N'C:\Users\fv0419\Documents\Visual Studio 2012\Projects\eCars\eCars.WebUI\Uploads\photos\2\2009_toyota_corolla_s-pic-7544428267893186992.jpeg', CAST(N'2014-08-28 08:25:29.323' AS DateTime), CAST(N'2014-08-28 08:25:29.323' AS DateTime), 0)
INSERT [Vehicle].[Photo] ([PhotoID], [VehicleID], [PhotoThumbnail], [ActualPhoto], [CreatedDate], [ModifiedDate], [MainPhoto]) VALUES (4, 2, N'Uploads\photos\2\51415177.jpg', N'C:\Users\fv0419\Documents\Visual Studio 2012\Projects\eCars\eCars.WebUI\Uploads\photos\2\51415177.jpg', CAST(N'2014-08-28 08:40:46.600' AS DateTime), CAST(N'2014-08-28 08:40:46.600' AS DateTime), 0)
INSERT [Vehicle].[Photo] ([PhotoID], [VehicleID], [PhotoThumbnail], [ActualPhoto], [CreatedDate], [ModifiedDate], [MainPhoto]) VALUES (5, 2, N'C:\Users\fv0419\Documents\Visual Studio 2012\Projects\eCars\eCars.WebUI\Uploads\photos\2\ag_09corollaxle_leftrear.jpg', N'C:\Users\fv0419\Documents\Visual Studio 2012\Projects\eCars\eCars.WebUI\Uploads\photos\2\ag_09corollaxle_leftrear.jpg', CAST(N'2014-08-28 08:41:02.037' AS DateTime), CAST(N'2014-08-28 08:41:02.037' AS DateTime), 0)
INSERT [Vehicle].[Photo] ([PhotoID], [VehicleID], [PhotoThumbnail], [ActualPhoto], [CreatedDate], [ModifiedDate], [MainPhoto]) VALUES (6, 2, N'C:\Users\fv0419\Documents\Visual Studio 2012\Projects\eCars\eCars.WebUI\Uploads\photos\2\2009_Toyota_Corolla_XRS_DC.JPG', N'C:\Users\fv0419\Documents\Visual Studio 2012\Projects\eCars\eCars.WebUI\Uploads\photos\2\2009_Toyota_Corolla_XRS_DC.JPG', CAST(N'2014-08-28 09:38:56.297' AS DateTime), CAST(N'2014-08-28 09:38:56.297' AS DateTime), 0)
INSERT [Vehicle].[Photo] ([PhotoID], [VehicleID], [PhotoThumbnail], [ActualPhoto], [CreatedDate], [ModifiedDate], [MainPhoto]) VALUES (7, 2, N'C:\Users\fv0419\Documents\Visual Studio 2012\Projects\eCars\eCars.WebUI\Uploads\photos\2\2009_Toyota_Corolla_XRS_DC.JPG', N'C:\Users\fv0419\Documents\Visual Studio 2012\Projects\eCars\eCars.WebUI\Uploads\photos\2\2009_Toyota_Corolla_XRS_DC.JPG', CAST(N'2014-08-28 09:44:20.783' AS DateTime), CAST(N'2014-08-28 09:44:20.783' AS DateTime), 0)
INSERT [Vehicle].[Photo] ([PhotoID], [VehicleID], [PhotoThumbnail], [ActualPhoto], [CreatedDate], [ModifiedDate], [MainPhoto]) VALUES (8, 2, N'C:\Users\fv0419\Documents\Visual Studio 2012\Projects\eCars\eCars.WebUI\Uploads\photos\2\2009_Toyota_Corolla_XRS_DC.JPG', N'C:\Users\fv0419\Documents\Visual Studio 2012\Projects\eCars\eCars.WebUI\Uploads\photos\2\2009_Toyota_Corolla_XRS_DC.JPG', CAST(N'2014-08-28 09:50:09.583' AS DateTime), CAST(N'2014-08-28 09:50:09.583' AS DateTime), 0)
INSERT [Vehicle].[Photo] ([PhotoID], [VehicleID], [PhotoThumbnail], [ActualPhoto], [CreatedDate], [ModifiedDate], [MainPhoto]) VALUES (9, 2, N'C:\Users\fv0419\Documents\Visual Studio 2012\Projects\eCars\eCars.WebUI\Uploads\photos\2\2009_Toyota_Corolla_XRS_DC.JPG', N'C:\Users\fv0419\Documents\Visual Studio 2012\Projects\eCars\eCars.WebUI\Uploads\photos\2\2009_Toyota_Corolla_XRS_DC.JPG', CAST(N'2014-08-28 09:55:05.710' AS DateTime), CAST(N'2014-08-28 09:55:05.710' AS DateTime), 0)
INSERT [Vehicle].[Photo] ([PhotoID], [VehicleID], [PhotoThumbnail], [ActualPhoto], [CreatedDate], [ModifiedDate], [MainPhoto]) VALUES (10, 2, N'C:\Users\fv0419\Documents\Visual Studio 2012\Projects\eCars\eCars.WebUI\Uploads\photos\2\2009_Toyota_Corolla_XRS_DC.JPG', N'C:\Users\fv0419\Documents\Visual Studio 2012\Projects\eCars\eCars.WebUI\Uploads\photos\2\2009_Toyota_Corolla_XRS_DC.JPG', CAST(N'2014-08-28 10:04:04.113' AS DateTime), CAST(N'2014-08-28 10:04:04.113' AS DateTime), 0)
INSERT [Vehicle].[Photo] ([PhotoID], [VehicleID], [PhotoThumbnail], [ActualPhoto], [CreatedDate], [ModifiedDate], [MainPhoto]) VALUES (11, 2, N'C:\Users\fv0419\Documents\Visual Studio 2012\Projects\eCars\eCars.WebUI\Uploads\photos\2\2009_toyota_corolla_s-pic-7544428267893186992.jpeg', N'C:\Users\fv0419\Documents\Visual Studio 2012\Projects\eCars\eCars.WebUI\Uploads\photos\2\2009_toyota_corolla_s-pic-7544428267893186992.jpeg', CAST(N'2014-08-28 10:04:16.107' AS DateTime), CAST(N'2014-08-28 10:04:16.107' AS DateTime), 0)
INSERT [Vehicle].[Photo] ([PhotoID], [VehicleID], [PhotoThumbnail], [ActualPhoto], [CreatedDate], [ModifiedDate], [MainPhoto]) VALUES (12, 2, N'C:\Users\fv0419\Documents\Visual Studio 2012\Projects\eCars\eCars.WebUI\Uploads\photos\2\2007_Toyota_Corolla_Ascent_(Rear_view).jpg', N'C:\Users\fv0419\Documents\Visual Studio 2012\Projects\eCars\eCars.WebUI\Uploads\photos\2\2007_Toyota_Corolla_Ascent_(Rear_view).jpg', CAST(N'2014-08-28 10:04:30.010' AS DateTime), CAST(N'2014-08-28 10:04:30.010' AS DateTime), 0)
INSERT [Vehicle].[Photo] ([PhotoID], [VehicleID], [PhotoThumbnail], [ActualPhoto], [CreatedDate], [ModifiedDate], [MainPhoto]) VALUES (13, 2, N'C:\Users\fv0419\Documents\Visual Studio 2012\Projects\eCars\eCars.WebUI\Uploads\photos\2\51415177.jpg', N'C:\Users\fv0419\Documents\Visual Studio 2012\Projects\eCars\eCars.WebUI\Uploads\photos\2\51415177.jpg', CAST(N'2014-08-28 10:04:40.773' AS DateTime), CAST(N'2014-08-28 10:04:40.773' AS DateTime), 0)
INSERT [Vehicle].[Photo] ([PhotoID], [VehicleID], [PhotoThumbnail], [ActualPhoto], [CreatedDate], [ModifiedDate], [MainPhoto]) VALUES (14, 1, N'Uploads\photos\1\2009_Toyota_Corolla_XRS_DC.JPG', N'Uploads\photos\1\2009_Toyota_Corolla_XRS_DC.JPG', CAST(N'2014-08-29 11:49:52.630' AS DateTime), CAST(N'2014-08-29 11:49:52.630' AS DateTime), 0)
INSERT [Vehicle].[Photo] ([PhotoID], [VehicleID], [PhotoThumbnail], [ActualPhoto], [CreatedDate], [ModifiedDate], [MainPhoto]) VALUES (15, 1, N'Uploads\photos\1\2009_toyota_corolla_s-pic-7544428267893186992.jpeg', N'Uploads\photos\1\2009_toyota_corolla_s-pic-7544428267893186992.jpeg', CAST(N'2014-08-29 11:50:00.637' AS DateTime), CAST(N'2014-08-29 11:50:00.637' AS DateTime), 0)
INSERT [Vehicle].[Photo] ([PhotoID], [VehicleID], [PhotoThumbnail], [ActualPhoto], [CreatedDate], [ModifiedDate], [MainPhoto]) VALUES (16, 1, N'Uploads\photos\1\51415177.jpg', N'Uploads\photos\1\51415177.jpg', CAST(N'2014-08-29 11:50:08.347' AS DateTime), CAST(N'2014-08-29 11:50:08.347' AS DateTime), 0)
INSERT [Vehicle].[Photo] ([PhotoID], [VehicleID], [PhotoThumbnail], [ActualPhoto], [CreatedDate], [ModifiedDate], [MainPhoto]) VALUES (17, 1, N'Uploads\photos\1\2007_Toyota_Corolla_Ascent_(Rear_view).jpg', N'Uploads\photos\1\2007_Toyota_Corolla_Ascent_(Rear_view).jpg', CAST(N'2014-08-29 11:50:17.307' AS DateTime), CAST(N'2014-08-29 11:50:17.307' AS DateTime), 0)
INSERT [Vehicle].[Photo] ([PhotoID], [VehicleID], [PhotoThumbnail], [ActualPhoto], [CreatedDate], [ModifiedDate], [MainPhoto]) VALUES (18, 3, N'Uploads\photos\3\2013-Civic-Interior.jpg', N'Uploads\photos\3\2013-Civic-Interior.jpg', CAST(N'2014-08-29 14:00:19.480' AS DateTime), CAST(N'2014-08-29 14:00:19.480' AS DateTime), 0)
INSERT [Vehicle].[Photo] ([PhotoID], [VehicleID], [PhotoThumbnail], [ActualPhoto], [CreatedDate], [ModifiedDate], [MainPhoto]) VALUES (19, 3, N'Uploads\photos\3\2013-honda-civic-cockpit.jpg', N'Uploads\photos\3\2013-honda-civic-cockpit.jpg', CAST(N'2014-08-29 14:00:25.730' AS DateTime), CAST(N'2014-08-29 14:00:25.730' AS DateTime), 0)
INSERT [Vehicle].[Photo] ([PhotoID], [VehicleID], [PhotoThumbnail], [ActualPhoto], [CreatedDate], [ModifiedDate], [MainPhoto]) VALUES (20, 3, N'Uploads\photos\3\p3181022.jpg', N'Uploads\photos\3\p3181022.jpg', CAST(N'2014-08-29 14:00:32.860' AS DateTime), CAST(N'2014-08-29 14:00:32.860' AS DateTime), 0)
INSERT [Vehicle].[Photo] ([PhotoID], [VehicleID], [PhotoThumbnail], [ActualPhoto], [CreatedDate], [ModifiedDate], [MainPhoto]) VALUES (21, 3, N'Uploads\photos\3\p3181025.jpg', N'Uploads\photos\3\p3181025.jpg', CAST(N'2014-08-29 14:00:40.297' AS DateTime), CAST(N'2014-08-29 14:00:40.297' AS DateTime), 0)
INSERT [Vehicle].[Photo] ([PhotoID], [VehicleID], [PhotoThumbnail], [ActualPhoto], [CreatedDate], [ModifiedDate], [MainPhoto]) VALUES (22, 4, N'Uploads\photos\4\8th-gen-honda-accord-coupe-nighthawk-black-pearl.jpg', N'Uploads\photos\4\8th-gen-honda-accord-coupe-nighthawk-black-pearl.jpg', CAST(N'2014-08-29 14:01:04.647' AS DateTime), CAST(N'2014-08-29 14:01:04.647' AS DateTime), 0)
INSERT [Vehicle].[Photo] ([PhotoID], [VehicleID], [PhotoThumbnail], [ActualPhoto], [CreatedDate], [ModifiedDate], [MainPhoto]) VALUES (23, 4, N'Uploads\photos\4\2010-honda-accord-crosstour-ex-interior-photo-306225-s-1280x782.jpg', N'Uploads\photos\4\2010-honda-accord-crosstour-ex-interior-photo-306225-s-1280x782.jpg', CAST(N'2014-08-29 14:01:11.653' AS DateTime), CAST(N'2014-08-29 14:01:11.653' AS DateTime), 0)
INSERT [Vehicle].[Photo] ([PhotoID], [VehicleID], [PhotoThumbnail], [ActualPhoto], [CreatedDate], [ModifiedDate], [MainPhoto]) VALUES (24, 4, N'Uploads\photos\4\2012-honda-crosstour-ex-l-black-dash.jpg', N'Uploads\photos\4\2012-honda-crosstour-ex-l-black-dash.jpg', CAST(N'2014-08-29 14:01:18.243' AS DateTime), CAST(N'2014-08-29 14:01:18.243' AS DateTime), 0)
INSERT [Vehicle].[Photo] ([PhotoID], [VehicleID], [PhotoThumbnail], [ActualPhoto], [CreatedDate], [ModifiedDate], [MainPhoto]) VALUES (25, 4, N'Uploads\photos\4\hot-sale-led-auto-tail-lamps-for-honda-accord.jpg', N'Uploads\photos\4\hot-sale-led-auto-tail-lamps-for-honda-accord.jpg', CAST(N'2014-08-29 14:01:24.100' AS DateTime), CAST(N'2014-08-29 14:01:24.100' AS DateTime), 0)
INSERT [Vehicle].[Photo] ([PhotoID], [VehicleID], [PhotoThumbnail], [ActualPhoto], [CreatedDate], [ModifiedDate], [MainPhoto]) VALUES (26, 8, N'/Uploads/photos/8/2012_Toyota_Camry_LE_--_10-19-2011.jpg', N'/Uploads/photos/8/2012_Toyota_Camry_LE_--_10-19-2011.jpg', CAST(N'2014-09-12 07:58:55.293' AS DateTime), CAST(N'2014-09-12 07:58:55.293' AS DateTime), 0)
INSERT [Vehicle].[Photo] ([PhotoID], [VehicleID], [PhotoThumbnail], [ActualPhoto], [CreatedDate], [ModifiedDate], [MainPhoto]) VALUES (27, 8, N'/Uploads/photos/8/2012_Toyota_Camry_SE_--_10-19-2011.jpg', N'/Uploads/photos/8/2012_Toyota_Camry_SE_--_10-19-2011.jpg', CAST(N'2014-09-12 08:05:02.350' AS DateTime), CAST(N'2014-09-12 08:05:02.350' AS DateTime), 0)
INSERT [Vehicle].[Photo] ([PhotoID], [VehicleID], [PhotoThumbnail], [ActualPhoto], [CreatedDate], [ModifiedDate], [MainPhoto]) VALUES (28, 8, N'/Uploads/photos/8\2012-Toyota-Camry-Hybrid-Sedan-LE-4dr-Sedan-Interior-Driver-Side.jpeg', N'/Uploads/photos/8\2012-Toyota-Camry-Hybrid-Sedan-LE-4dr-Sedan-Interior-Driver-Side.jpeg', CAST(N'2014-09-12 08:07:31.710' AS DateTime), CAST(N'2014-09-12 08:07:31.710' AS DateTime), 0)
INSERT [Vehicle].[Photo] ([PhotoID], [VehicleID], [PhotoThumbnail], [ActualPhoto], [CreatedDate], [ModifiedDate], [MainPhoto]) VALUES (29, 8, N'/Uploads/photos/8\2012-toyota-camry-interior.jpg', N'/Uploads/photos/8\2012-toyota-camry-interior.jpg', CAST(N'2014-09-12 08:09:11.553' AS DateTime), CAST(N'2014-09-12 08:09:11.553' AS DateTime), 0)
INSERT [Vehicle].[Photo] ([PhotoID], [VehicleID], [PhotoThumbnail], [ActualPhoto], [CreatedDate], [ModifiedDate], [MainPhoto]) VALUES (30, 11, N'/Uploads/photos/11\Mercedes 1.jpg', N'/Uploads/photos/11\Mercedes 1.jpg', CAST(N'2014-09-21 12:16:23.480' AS DateTime), CAST(N'2014-09-21 12:16:23.480' AS DateTime), 0)
INSERT [Vehicle].[Photo] ([PhotoID], [VehicleID], [PhotoThumbnail], [ActualPhoto], [CreatedDate], [ModifiedDate], [MainPhoto]) VALUES (31, 11, N'/Uploads/photos/11\mercedes 2.jpg', N'/Uploads/photos/11\mercedes 2.jpg', CAST(N'2014-09-21 12:16:34.500' AS DateTime), CAST(N'2014-09-21 12:16:34.500' AS DateTime), 0)
INSERT [Vehicle].[Photo] ([PhotoID], [VehicleID], [PhotoThumbnail], [ActualPhoto], [CreatedDate], [ModifiedDate], [MainPhoto]) VALUES (32, 11, N'/Uploads/photos/11\mercedes 3.jpg', N'/Uploads/photos/11\mercedes 3.jpg', CAST(N'2014-09-21 12:16:41.470' AS DateTime), CAST(N'2014-09-21 12:16:41.470' AS DateTime), 0)
INSERT [Vehicle].[Photo] ([PhotoID], [VehicleID], [PhotoThumbnail], [ActualPhoto], [CreatedDate], [ModifiedDate], [MainPhoto]) VALUES (33, 11, N'/Uploads/photos/11\mercedes 4.jpg', N'/Uploads/photos/11\mercedes 4.jpg', CAST(N'2014-09-21 12:16:47.237' AS DateTime), CAST(N'2014-09-21 12:16:47.237' AS DateTime), 0)
INSERT [Vehicle].[Photo] ([PhotoID], [VehicleID], [PhotoThumbnail], [ActualPhoto], [CreatedDate], [ModifiedDate], [MainPhoto]) VALUES (34, 11, N'/Uploads/photos/11\mercedes 5.jpg', N'/Uploads/photos/11\mercedes 5.jpg', CAST(N'2014-09-21 12:16:56.873' AS DateTime), CAST(N'2014-09-21 12:16:56.873' AS DateTime), 0)
INSERT [Vehicle].[Photo] ([PhotoID], [VehicleID], [PhotoThumbnail], [ActualPhoto], [CreatedDate], [ModifiedDate], [MainPhoto]) VALUES (35, 11, N'/Uploads/photos/11\mercedes 6.jpg', N'/Uploads/photos/11\mercedes 6.jpg', CAST(N'2014-09-21 12:17:02.617' AS DateTime), CAST(N'2014-09-21 12:17:02.617' AS DateTime), 0)
INSERT [Vehicle].[Photo] ([PhotoID], [VehicleID], [PhotoThumbnail], [ActualPhoto], [CreatedDate], [ModifiedDate], [MainPhoto]) VALUES (36, 11, N'/Uploads/photos/11\mercedes 7.jpg', N'/Uploads/photos/11\mercedes 7.jpg', CAST(N'2014-09-21 12:17:08.160' AS DateTime), CAST(N'2014-09-21 12:17:08.160' AS DateTime), 0)
INSERT [Vehicle].[Photo] ([PhotoID], [VehicleID], [PhotoThumbnail], [ActualPhoto], [CreatedDate], [ModifiedDate], [MainPhoto]) VALUES (37, 12, N'/Uploads/photos/12\IMG-20140721-02474.jpg', N'/Uploads/photos/12\IMG-20140721-02474.jpg', CAST(N'2014-09-21 14:39:48.940' AS DateTime), CAST(N'2014-09-21 14:39:48.940' AS DateTime), 0)
INSERT [Vehicle].[Photo] ([PhotoID], [VehicleID], [PhotoThumbnail], [ActualPhoto], [CreatedDate], [ModifiedDate], [MainPhoto]) VALUES (38, 12, N'/Uploads/photos/12\IMG-20140721-02475.jpg', N'/Uploads/photos/12\IMG-20140721-02475.jpg', CAST(N'2014-09-21 14:39:55.333' AS DateTime), CAST(N'2014-09-21 14:39:55.333' AS DateTime), 0)
INSERT [Vehicle].[Photo] ([PhotoID], [VehicleID], [PhotoThumbnail], [ActualPhoto], [CreatedDate], [ModifiedDate], [MainPhoto]) VALUES (39, 12, N'/Uploads/photos/12\IMG-20140721-02476.jpg', N'/Uploads/photos/12\IMG-20140721-02476.jpg', CAST(N'2014-09-21 14:40:00.430' AS DateTime), CAST(N'2014-09-21 14:40:00.430' AS DateTime), 0)
INSERT [Vehicle].[Photo] ([PhotoID], [VehicleID], [PhotoThumbnail], [ActualPhoto], [CreatedDate], [ModifiedDate], [MainPhoto]) VALUES (40, 12, N'/Uploads/photos/12\IMG-20140721-02477.jpg', N'/Uploads/photos/12\IMG-20140721-02477.jpg', CAST(N'2014-09-21 14:40:06.173' AS DateTime), CAST(N'2014-09-21 14:40:06.173' AS DateTime), 0)
INSERT [Vehicle].[Photo] ([PhotoID], [VehicleID], [PhotoThumbnail], [ActualPhoto], [CreatedDate], [ModifiedDate], [MainPhoto]) VALUES (41, 12, N'/Uploads/photos/12\IMG-20140721-02478.jpg', N'/Uploads/photos/12\IMG-20140721-02478.jpg', CAST(N'2014-09-21 14:40:11.170' AS DateTime), CAST(N'2014-09-21 14:40:11.170' AS DateTime), 0)
INSERT [Vehicle].[Photo] ([PhotoID], [VehicleID], [PhotoThumbnail], [ActualPhoto], [CreatedDate], [ModifiedDate], [MainPhoto]) VALUES (42, 12, N'/Uploads/photos/12\IMG-20140721-02479.jpg', N'/Uploads/photos/12\IMG-20140721-02479.jpg', CAST(N'2014-09-21 14:40:16.347' AS DateTime), CAST(N'2014-09-21 14:40:16.347' AS DateTime), 0)
INSERT [Vehicle].[Photo] ([PhotoID], [VehicleID], [PhotoThumbnail], [ActualPhoto], [CreatedDate], [ModifiedDate], [MainPhoto]) VALUES (43, 13, N'/Uploads/photos/13\IMG-20140717-02434.jpg', N'/Uploads/photos/13\IMG-20140717-02434.jpg', CAST(N'2014-09-21 15:07:06.420' AS DateTime), CAST(N'2014-09-21 15:07:06.420' AS DateTime), 0)
INSERT [Vehicle].[Photo] ([PhotoID], [VehicleID], [PhotoThumbnail], [ActualPhoto], [CreatedDate], [ModifiedDate], [MainPhoto]) VALUES (44, 13, N'/Uploads/photos/13\IMG-20140717-02437.jpg', N'/Uploads/photos/13\IMG-20140717-02437.jpg', CAST(N'2014-09-21 15:07:17.330' AS DateTime), CAST(N'2014-09-21 15:07:17.330' AS DateTime), 0)
INSERT [Vehicle].[Photo] ([PhotoID], [VehicleID], [PhotoThumbnail], [ActualPhoto], [CreatedDate], [ModifiedDate], [MainPhoto]) VALUES (45, 13, N'/Uploads/photos/13\IMG-20140717-02435.jpg', N'/Uploads/photos/13\IMG-20140717-02435.jpg', CAST(N'2014-09-21 15:07:23.800' AS DateTime), CAST(N'2014-09-21 15:07:23.800' AS DateTime), 0)
INSERT [Vehicle].[Photo] ([PhotoID], [VehicleID], [PhotoThumbnail], [ActualPhoto], [CreatedDate], [ModifiedDate], [MainPhoto]) VALUES (46, 13, N'/Uploads/photos/13\IMG-20140717-02436.jpg', N'/Uploads/photos/13\IMG-20140717-02436.jpg', CAST(N'2014-09-21 15:07:28.703' AS DateTime), CAST(N'2014-09-21 15:07:28.703' AS DateTime), 0)
INSERT [Vehicle].[Photo] ([PhotoID], [VehicleID], [PhotoThumbnail], [ActualPhoto], [CreatedDate], [ModifiedDate], [MainPhoto]) VALUES (47, 14, N'/Uploads/photos/14\IMG_20140813_111113_720.jpg', N'/Uploads/photos/14\IMG_20140813_111113_720.jpg', CAST(N'2014-09-21 15:15:06.573' AS DateTime), CAST(N'2014-09-21 15:15:06.573' AS DateTime), 0)
INSERT [Vehicle].[Photo] ([PhotoID], [VehicleID], [PhotoThumbnail], [ActualPhoto], [CreatedDate], [ModifiedDate], [MainPhoto]) VALUES (48, 14, N'/Uploads/photos/14\IMG_20140813_111623_065.jpg', N'/Uploads/photos/14\IMG_20140813_111623_065.jpg', CAST(N'2014-09-21 15:15:13.553' AS DateTime), CAST(N'2014-09-21 15:15:13.553' AS DateTime), 0)
INSERT [Vehicle].[Photo] ([PhotoID], [VehicleID], [PhotoThumbnail], [ActualPhoto], [CreatedDate], [ModifiedDate], [MainPhoto]) VALUES (49, 14, N'/Uploads/photos/14\IMG_20140813_111142_887.jpg', N'/Uploads/photos/14\IMG_20140813_111142_887.jpg', CAST(N'2014-09-21 15:15:21.947' AS DateTime), CAST(N'2014-09-21 15:15:21.947' AS DateTime), 0)
INSERT [Vehicle].[Photo] ([PhotoID], [VehicleID], [PhotoThumbnail], [ActualPhoto], [CreatedDate], [ModifiedDate], [MainPhoto]) VALUES (50, 14, N'/Uploads/photos/14\IMG_20140813_111155_505.jpg', N'/Uploads/photos/14\IMG_20140813_111155_505.jpg', CAST(N'2014-09-21 15:15:28.123' AS DateTime), CAST(N'2014-09-21 15:15:28.123' AS DateTime), 0)
INSERT [Vehicle].[Photo] ([PhotoID], [VehicleID], [PhotoThumbnail], [ActualPhoto], [CreatedDate], [ModifiedDate], [MainPhoto]) VALUES (51, 14, N'/Uploads/photos/14\IMG_20140813_111251_643.jpg', N'/Uploads/photos/14\IMG_20140813_111251_643.jpg', CAST(N'2014-09-21 15:15:35.390' AS DateTime), CAST(N'2014-09-21 15:15:35.390' AS DateTime), 0)
INSERT [Vehicle].[Photo] ([PhotoID], [VehicleID], [PhotoThumbnail], [ActualPhoto], [CreatedDate], [ModifiedDate], [MainPhoto]) VALUES (52, 14, N'/Uploads/photos/14\IMG_20140813_111636_359.jpg', N'/Uploads/photos/14\IMG_20140813_111636_359.jpg', CAST(N'2014-09-21 15:15:40.867' AS DateTime), CAST(N'2014-09-21 15:15:40.867' AS DateTime), 0)
SET IDENTITY_INSERT [Vehicle].[Photo] OFF
SET IDENTITY_INSERT [Vehicle].[Plan] ON 

INSERT [Vehicle].[Plan] ([PlanID], [NotificationModelID], [Name], [Price], [NumberOfImages], [SearchRankingMinutes], [AdvertisingDurationHours], [NumberVehiclesAllowed], [SearchRankVehiclesAllowed], [Created], [Modified]) VALUES (1, 1, N'Básico', CAST(0.0000 AS Decimal(10, 4)), 4, 60, 72, 1, 0, CAST(N'2014-07-15 09:39:51.893' AS DateTime), CAST(N'2014-07-15 09:39:51.893' AS DateTime))
INSERT [Vehicle].[Plan] ([PlanID], [NotificationModelID], [Name], [Price], [NumberOfImages], [SearchRankingMinutes], [AdvertisingDurationHours], [NumberVehiclesAllowed], [SearchRankVehiclesAllowed], [Created], [Modified]) VALUES (2, 1, N'Standard', CAST(500.0000 AS Decimal(10, 4)), 8, 60, 336, 1, 0, CAST(N'2014-07-15 09:39:51.893' AS DateTime), CAST(N'2014-07-15 09:39:51.893' AS DateTime))
INSERT [Vehicle].[Plan] ([PlanID], [NotificationModelID], [Name], [Price], [NumberOfImages], [SearchRankingMinutes], [AdvertisingDurationHours], [NumberVehiclesAllowed], [SearchRankVehiclesAllowed], [Created], [Modified]) VALUES (4, 1, N'Profesional', CAST(1000.0000 AS Decimal(10, 4)), 12, 120, 720, 2, 0, CAST(N'2014-09-11 08:15:57.097' AS DateTime), CAST(N'2014-09-11 08:15:57.097' AS DateTime))
INSERT [Vehicle].[Plan] ([PlanID], [NotificationModelID], [Name], [Price], [NumberOfImages], [SearchRankingMinutes], [AdvertisingDurationHours], [NumberVehiclesAllowed], [SearchRankVehiclesAllowed], [Created], [Modified]) VALUES (6, 1, N'Enterprise', CAST(1500.0000 AS Decimal(10, 4)), 16, 300, 720, 5, 0, CAST(N'2014-09-11 08:17:10.430' AS DateTime), CAST(N'2014-09-11 08:17:10.430' AS DateTime))
SET IDENTITY_INSERT [Vehicle].[Plan] OFF
SET IDENTITY_INSERT [Vehicle].[Publication] ON 

INSERT [Vehicle].[Publication] ([PublicationID], [UserIdPerformance], [VehicleID], [Active], [Expired], [Created], [Modified]) VALUES (9, 2, 11, 1, 0, CAST(N'2014-09-21 12:15:59.833' AS DateTime), CAST(N'2014-09-21 12:17:19.760' AS DateTime))
INSERT [Vehicle].[Publication] ([PublicationID], [UserIdPerformance], [VehicleID], [Active], [Expired], [Created], [Modified]) VALUES (10, 2, 12, 1, 0, CAST(N'2014-09-21 14:39:27.050' AS DateTime), CAST(N'2014-09-21 14:40:22.993' AS DateTime))
INSERT [Vehicle].[Publication] ([PublicationID], [UserIdPerformance], [VehicleID], [Active], [Expired], [Created], [Modified]) VALUES (11, 2, 13, 1, 0, CAST(N'2014-09-21 15:06:51.353' AS DateTime), CAST(N'2014-09-21 15:07:36.483' AS DateTime))
INSERT [Vehicle].[Publication] ([PublicationID], [UserIdPerformance], [VehicleID], [Active], [Expired], [Created], [Modified]) VALUES (12, 2, 14, 1, 0, CAST(N'2014-09-21 15:14:50.610' AS DateTime), CAST(N'2014-09-21 15:15:44.680' AS DateTime))
SET IDENTITY_INSERT [Vehicle].[Publication] OFF
SET IDENTITY_INSERT [Vehicle].[State] ON 

INSERT [Vehicle].[State] ([StateId], [Name], [Value], [Active], [Created], [Modified]) VALUES (1, N'Distrito Nacional', 1, 1, CAST(N'2014-08-30 18:19:35.710' AS DateTime), CAST(N'2014-08-30 18:19:35.710' AS DateTime))
INSERT [Vehicle].[State] ([StateId], [Name], [Value], [Active], [Created], [Modified]) VALUES (2, N'Azua', 2, 1, CAST(N'2014-08-30 18:19:42.370' AS DateTime), CAST(N'2014-08-30 18:19:42.370' AS DateTime))
INSERT [Vehicle].[State] ([StateId], [Name], [Value], [Active], [Created], [Modified]) VALUES (3, N'Baoruco', 2, 1, CAST(N'2014-08-30 18:19:54.347' AS DateTime), CAST(N'2014-08-30 18:19:54.347' AS DateTime))
INSERT [Vehicle].[State] ([StateId], [Name], [Value], [Active], [Created], [Modified]) VALUES (5, N'Barahona', 3, 1, CAST(N'2014-08-30 18:20:07.307' AS DateTime), CAST(N'2014-08-30 18:20:07.307' AS DateTime))
INSERT [Vehicle].[State] ([StateId], [Name], [Value], [Active], [Created], [Modified]) VALUES (6, N'Duarte', 4, 1, CAST(N'2014-08-30 18:20:16.040' AS DateTime), CAST(N'2014-08-30 18:20:16.040' AS DateTime))
INSERT [Vehicle].[State] ([StateId], [Name], [Value], [Active], [Created], [Modified]) VALUES (7, N'El Seibo', 5, 1, CAST(N'2014-08-30 18:20:39.280' AS DateTime), CAST(N'2014-08-30 18:20:39.280' AS DateTime))
INSERT [Vehicle].[State] ([StateId], [Name], [Value], [Active], [Created], [Modified]) VALUES (8, N'Espaillat', 6, 1, CAST(N'2014-08-30 18:20:50.440' AS DateTime), CAST(N'2014-08-30 18:20:50.440' AS DateTime))
INSERT [Vehicle].[State] ([StateId], [Name], [Value], [Active], [Created], [Modified]) VALUES (10, N'Independencia', 7, 1, CAST(N'2014-08-30 18:21:03.983' AS DateTime), CAST(N'2014-08-30 18:21:03.983' AS DateTime))
INSERT [Vehicle].[State] ([StateId], [Name], [Value], [Active], [Created], [Modified]) VALUES (11, N'La Altragracia', 8, 1, CAST(N'2014-08-30 18:21:13.590' AS DateTime), CAST(N'2014-08-30 18:21:13.590' AS DateTime))
INSERT [Vehicle].[State] ([StateId], [Name], [Value], [Active], [Created], [Modified]) VALUES (12, N'La Romana', 9, 1, CAST(N'2014-08-30 18:21:21.237' AS DateTime), CAST(N'2014-08-30 18:21:21.237' AS DateTime))
INSERT [Vehicle].[State] ([StateId], [Name], [Value], [Active], [Created], [Modified]) VALUES (13, N'La Vega', 10, 1, CAST(N'2014-08-30 18:21:34.447' AS DateTime), CAST(N'2014-08-30 18:21:34.447' AS DateTime))
INSERT [Vehicle].[State] ([StateId], [Name], [Value], [Active], [Created], [Modified]) VALUES (14, N'Monte Cristi', 11, 1, CAST(N'2014-08-30 18:21:43.300' AS DateTime), CAST(N'2014-08-30 18:21:43.300' AS DateTime))
INSERT [Vehicle].[State] ([StateId], [Name], [Value], [Active], [Created], [Modified]) VALUES (15, N'Pedernales', 12, 1, CAST(N'2014-08-30 18:21:50.477' AS DateTime), CAST(N'2014-08-30 18:21:50.477' AS DateTime))
INSERT [Vehicle].[State] ([StateId], [Name], [Value], [Active], [Created], [Modified]) VALUES (16, N'Peravia', 13, 1, CAST(N'2014-08-30 18:22:00.637' AS DateTime), CAST(N'2014-08-30 18:22:00.637' AS DateTime))
INSERT [Vehicle].[State] ([StateId], [Name], [Value], [Active], [Created], [Modified]) VALUES (17, N'Puerto Plata', 14, 1, CAST(N'2014-08-30 18:22:08.817' AS DateTime), CAST(N'2014-08-30 18:22:08.817' AS DateTime))
INSERT [Vehicle].[State] ([StateId], [Name], [Value], [Active], [Created], [Modified]) VALUES (18, N'Hermanas Mirabal', 15, 1, CAST(N'2014-08-30 18:22:21.110' AS DateTime), CAST(N'2014-08-30 18:22:21.110' AS DateTime))
INSERT [Vehicle].[State] ([StateId], [Name], [Value], [Active], [Created], [Modified]) VALUES (19, N'San Juan', 16, 1, CAST(N'2014-08-30 18:22:59.457' AS DateTime), CAST(N'2014-08-30 18:22:59.457' AS DateTime))
INSERT [Vehicle].[State] ([StateId], [Name], [Value], [Active], [Created], [Modified]) VALUES (20, N'Santiago', 17, 1, CAST(N'2014-08-30 18:23:06.680' AS DateTime), CAST(N'2014-08-30 18:23:06.680' AS DateTime))
INSERT [Vehicle].[State] ([StateId], [Name], [Value], [Active], [Created], [Modified]) VALUES (21, N'Valverde', 18, 1, CAST(N'2014-08-30 18:23:14.213' AS DateTime), CAST(N'2014-08-30 18:23:14.213' AS DateTime))
INSERT [Vehicle].[State] ([StateId], [Name], [Value], [Active], [Created], [Modified]) VALUES (22, N'Monte Plata ', 19, 1, CAST(N'2014-08-30 18:23:23.563' AS DateTime), CAST(N'2014-08-30 18:23:23.563' AS DateTime))
INSERT [Vehicle].[State] ([StateId], [Name], [Value], [Active], [Created], [Modified]) VALUES (23, N'Hato Mayor', 20, 1, CAST(N'2014-08-30 18:23:36.393' AS DateTime), CAST(N'2014-08-30 18:23:36.393' AS DateTime))
INSERT [Vehicle].[State] ([StateId], [Name], [Value], [Active], [Created], [Modified]) VALUES (24, N'Santo Domingo', 21, 1, CAST(N'2014-08-30 18:23:51.420' AS DateTime), CAST(N'2014-08-30 18:23:51.420' AS DateTime))
INSERT [Vehicle].[State] ([StateId], [Name], [Value], [Active], [Created], [Modified]) VALUES (25, N'Santo Domingo Este', 22, 1, CAST(N'2014-08-30 18:24:04.977' AS DateTime), CAST(N'2014-08-30 18:24:04.977' AS DateTime))
INSERT [Vehicle].[State] ([StateId], [Name], [Value], [Active], [Created], [Modified]) VALUES (26, N'Boston', 23, 1, CAST(N'2014-08-30 18:24:25.687' AS DateTime), CAST(N'2014-08-30 18:24:25.687' AS DateTime))
INSERT [Vehicle].[State] ([StateId], [Name], [Value], [Active], [Created], [Modified]) VALUES (27, N'Miami', 24, 1, CAST(N'2014-08-30 18:24:35.183' AS DateTime), CAST(N'2014-08-30 18:24:35.183' AS DateTime))
INSERT [Vehicle].[State] ([StateId], [Name], [Value], [Active], [Created], [Modified]) VALUES (28, N'New York', 25, 1, CAST(N'2014-08-30 18:24:44.287' AS DateTime), CAST(N'2014-08-30 18:24:44.287' AS DateTime))
INSERT [Vehicle].[State] ([StateId], [Name], [Value], [Active], [Created], [Modified]) VALUES (30, N'New Jersey', 26, 1, CAST(N'2014-08-30 18:25:18.327' AS DateTime), CAST(N'2014-08-30 18:25:18.327' AS DateTime))
SET IDENTITY_INSERT [Vehicle].[State] OFF
SET IDENTITY_INSERT [Vehicle].[Transmission] ON 

INSERT [Vehicle].[Transmission] ([TransmissionID], [Name], [Value], [Description], [Created], [Modified], [Active]) VALUES (1, N'Automática 5 velocidades', CAST(5.00 AS Decimal(5, 2)), NULL, CAST(N'2014-07-15 12:11:26.613' AS DateTime), CAST(N'2014-07-15 12:11:26.613' AS DateTime), 1)
INSERT [Vehicle].[Transmission] ([TransmissionID], [Name], [Value], [Description], [Created], [Modified], [Active]) VALUES (2, N'Manual 6 velocidades', CAST(6.00 AS Decimal(5, 2)), NULL, CAST(N'2014-07-15 12:11:48.187' AS DateTime), CAST(N'2014-07-15 12:11:48.187' AS DateTime), 1)
INSERT [Vehicle].[Transmission] ([TransmissionID], [Name], [Value], [Description], [Created], [Modified], [Active]) VALUES (4, N'Transmisión variable Continua', CAST(0.00 AS Decimal(5, 2)), NULL, CAST(N'2014-07-15 12:13:46.620' AS DateTime), CAST(N'2014-07-15 12:13:46.620' AS DateTime), 1)
INSERT [Vehicle].[Transmission] ([TransmissionID], [Name], [Value], [Description], [Created], [Modified], [Active]) VALUES (5, N'Automática 6 velocidades', CAST(6.00 AS Decimal(5, 2)), NULL, CAST(N'2014-09-11 16:23:33.880' AS DateTime), CAST(N'2014-09-11 16:23:33.880' AS DateTime), 1)
INSERT [Vehicle].[Transmission] ([TransmissionID], [Name], [Value], [Description], [Created], [Modified], [Active]) VALUES (6, N'Automática 7 velocidades', CAST(7.00 AS Decimal(5, 2)), NULL, CAST(N'2014-09-11 16:23:42.953' AS DateTime), CAST(N'2014-09-11 16:23:42.953' AS DateTime), 1)
INSERT [Vehicle].[Transmission] ([TransmissionID], [Name], [Value], [Description], [Created], [Modified], [Active]) VALUES (7, N'Automática 8 velocidades', CAST(8.00 AS Decimal(5, 2)), NULL, CAST(N'2014-09-11 16:23:51.723' AS DateTime), CAST(N'2014-09-11 16:23:51.723' AS DateTime), 1)
INSERT [Vehicle].[Transmission] ([TransmissionID], [Name], [Value], [Description], [Created], [Modified], [Active]) VALUES (9, N'Automática 4 velocidades', CAST(4.00 AS Decimal(5, 2)), NULL, CAST(N'2014-09-11 16:24:11.307' AS DateTime), CAST(N'2014-09-11 16:24:11.307' AS DateTime), 1)
INSERT [Vehicle].[Transmission] ([TransmissionID], [Name], [Value], [Description], [Created], [Modified], [Active]) VALUES (10, N'Manual 5 velocidades', CAST(5.00 AS Decimal(5, 2)), NULL, CAST(N'2014-09-11 16:24:24.800' AS DateTime), CAST(N'2014-09-11 16:24:24.800' AS DateTime), 1)
INSERT [Vehicle].[Transmission] ([TransmissionID], [Name], [Value], [Description], [Created], [Modified], [Active]) VALUES (11, N'Manual 7 velocidades', CAST(7.00 AS Decimal(5, 2)), NULL, CAST(N'2014-09-11 16:24:35.800' AS DateTime), CAST(N'2014-09-11 16:24:35.800' AS DateTime), 1)
INSERT [Vehicle].[Transmission] ([TransmissionID], [Name], [Value], [Description], [Created], [Modified], [Active]) VALUES (12, N'Manual 8 velocidades', CAST(8.00 AS Decimal(5, 2)), NULL, CAST(N'2014-09-11 16:24:48.910' AS DateTime), CAST(N'2014-09-11 16:24:48.910' AS DateTime), 1)
INSERT [Vehicle].[Transmission] ([TransmissionID], [Name], [Value], [Description], [Created], [Modified], [Active]) VALUES (14, N'Manual 4 velocidades', CAST(4.00 AS Decimal(5, 2)), NULL, CAST(N'2014-09-11 16:24:59.303' AS DateTime), CAST(N'2014-09-11 16:24:59.303' AS DateTime), 1)
INSERT [Vehicle].[Transmission] ([TransmissionID], [Name], [Value], [Description], [Created], [Modified], [Active]) VALUES (15, N'Sincronizada', CAST(5.00 AS Decimal(5, 2)), NULL, CAST(N'2014-09-21 11:55:04.837' AS DateTime), CAST(N'2014-09-21 11:55:04.837' AS DateTime), 1)
SET IDENTITY_INSERT [Vehicle].[Transmission] OFF
SET IDENTITY_INSERT [Vehicle].[Type] ON 

INSERT [Vehicle].[Type] ([VehicleTypeID], [Name], [Description], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active]) VALUES (1, N'Sedan', NULL, CAST(N'2014-07-15 12:00:54.303' AS DateTime), CAST(N'2014-07-15 12:00:54.303' AS DateTime), 1, 1)
INSERT [Vehicle].[Type] ([VehicleTypeID], [Name], [Description], [CreatedDate], [ModifiedDate], [ModifiedBy], [Active]) VALUES (2, N'Sedan', NULL, CAST(N'2014-07-15 12:00:54.303' AS DateTime), CAST(N'2014-07-15 12:00:54.303' AS DateTime), 1, 1)
SET IDENTITY_INSERT [Vehicle].[Type] OFF
SET IDENTITY_INSERT [Vehicle].[Vehicle] ON 

INSERT [Vehicle].[Vehicle] ([vehicleID], [MakeAndModelID], [MotorID], [TransmissionID], [FuelID], [DriveTrainID], [VehicleConditionID], [Cilinders], [CurrencyId], [Price], [TotalSeating], [ColorInteriorId], [ColorExteriorId], [Year], [Created], [Modified], [Tags], [Description], [StateId]) VALUES (11, 2909, 7, 15, 1, 3, 2, 6, 2, CAST(29900.00 AS Decimal(14, 2)), 5, 2, 4, 2010, CAST(N'2014-09-21 12:15:56.383' AS DateTime), CAST(N'2014-09-21 12:15:56.383' AS DateTime), N'Asientos en piel, Faros halógenos, Aire acondicionado digital, CD player,Pantalla de navegación, Seguros eléctricos, Bolsa de aire (pasajero), Retrovisores eléctricos, Bolsa de aire (chofer), Luces encendido diurno, Sonido profesional, Bolsa de aire (laterales), Asientos eléctricos, Radio AM/FM, Frenos ABS, Aros de magnesio, Pintura de fábrica, Guía Multifunción, Bluetooth, Llave inteligente, Techo Panorámico.', N'Me llego esta hermosura, para los amantes de los carros confortables y sport le tengo este C-350 en US $ 29,900.00', 1)
INSERT [Vehicle].[Vehicle] ([vehicleID], [MakeAndModelID], [MotorID], [TransmissionID], [FuelID], [DriveTrainID], [VehicleConditionID], [Cilinders], [CurrencyId], [Price], [TotalSeating], [ColorInteriorId], [ColorExteriorId], [Year], [Created], [Modified], [Tags], [Description], [StateId]) VALUES (12, 2718, 52, 1, 1, 2, 1, 4, 2, CAST(26750.00 AS Decimal(14, 2)), 5, 2, 4, 2014, CAST(N'2014-09-21 14:39:27.033' AS DateTime), CAST(N'2014-09-21 14:39:27.033' AS DateTime), N'Barra de protección lateral ultra resistente, Doble bolsa de aire frontal, Radio AM/FM/CD Mp3/Aux, Vidrios, Seguros y Espejos Eléctricos, Luces antiniebla LED, Luces direccionales LED en espejos retrovisores, Controles del radio en el guía, Bluetooth con capacidad de manejo de hasta 5 teléfonos y Media Streaming, Aros de aleación de 17 pulgadas, Ipod interface, Guía Hidráulico, Asientos tapizados en tela.', N'Kia Cerato (2014) por tan solo US $ 26,750.00 con una taza de
 10.95 % aprovecha esta Super Oferta!! ', 1)
INSERT [Vehicle].[Vehicle] ([vehicleID], [MakeAndModelID], [MotorID], [TransmissionID], [FuelID], [DriveTrainID], [VehicleConditionID], [Cilinders], [CurrencyId], [Price], [TotalSeating], [ColorInteriorId], [ColorExteriorId], [Year], [Created], [Modified], [Tags], [Description], [StateId]) VALUES (13, 3665, 2, 5, 1, 2, 1, 4, 2, CAST(34900.00 AS Decimal(14, 2)), 5, 2, 1, 2015, CAST(N'2014-09-21 15:06:51.337' AS DateTime), CAST(N'2014-09-21 15:06:51.337' AS DateTime), N'Faros halógenos, Aire acondicionado digital, Vidrios eléctricos, CD player, Seguros eléctricos, Spoiler trasero, Bolsa de aire (pasajero), Retrovisores eléctricos, Bolsa de aire (chófer), Full power, Guía hidráulico, Asientos en tela, Cruise control, Sonido profesional, Bolsa de aire (laterales), Radio AM/FM/CD/AUX, Frenos ABS, Alarma, Aros de magnesio, Cámara de Reversa, Quia Multinacional, Bluetooth.', N'Ya tenemos la nueva Mazda CX-5 (2015) ven y llévate la tuya por tan solo US $ 34,900.00.', 1)
INSERT [Vehicle].[Vehicle] ([vehicleID], [MakeAndModelID], [MotorID], [TransmissionID], [FuelID], [DriveTrainID], [VehicleConditionID], [Cilinders], [CurrencyId], [Price], [TotalSeating], [ColorInteriorId], [ColorExteriorId], [Year], [Created], [Modified], [Tags], [Description], [StateId]) VALUES (14, 2904, 8, 2, 1, 3, 2, 6, 1, CAST(595000.00 AS Decimal(14, 2)), 5, 2, 1, 2007, CAST(N'2014-09-21 15:14:50.597' AS DateTime), CAST(N'2014-09-21 15:14:50.597' AS DateTime), N'Asientos en piel, Faros halógenos, Vidrios eléctricos, CD player, Seguros eléctricos, Aire acondicionado normal, Bolsa de aire (pasajero), Bolsa de aire (chofer), Asientos eléctricos, Radio AM/FM, Frenos ABS, Aros de magnesio, Llave inteligente', N'Para los que les gustan los carros de lujo y confortable les tengo este Mercedes Benz C-230 por tan solo RD $ 595,000.00', 1)
SET IDENTITY_INSERT [Vehicle].[Vehicle] OFF
ALTER TABLE [Config].[Role] ADD  CONSTRAINT [def_createdDateConfigRoles]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [Config].[Role] ADD  CONSTRAINT [def_modifiedDateConfigRoles]  DEFAULT (getdate()) FOR [ModifiedDate]
GO
ALTER TABLE [User].[Photo] ADD  CONSTRAINT [DF_UserPhoto_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [User].[Photo] ADD  CONSTRAINT [DF_UserPhoto_ModifiedDate]  DEFAULT (getdate()) FOR [ModifiedDate]
GO
ALTER TABLE [User].[Preference] ADD  CONSTRAINT [DF_UserPreferences_Created]  DEFAULT (getdate()) FOR [Createddate]
GO
ALTER TABLE [User].[Preference] ADD  CONSTRAINT [DF_UserPreferences_ModifiedDate]  DEFAULT (getdate()) FOR [ModifiedDate]
GO
ALTER TABLE [Vehicle].[Notification] ADD  CONSTRAINT [DF_VehicleNotification_Sent]  DEFAULT ((0)) FOR [Sent]
GO
ALTER TABLE [Vehicle].[Notification] ADD  CONSTRAINT [DF_VehicleNotification_Created]  DEFAULT (getdate()) FOR [Created]
GO
ALTER TABLE [Vehicle].[Notification] ADD  CONSTRAINT [DF_VehicleNotification_Modified]  DEFAULT (getdate()) FOR [Modified]
GO
ALTER TABLE [Vehicle].[MakeAndModel]  WITH CHECK ADD  CONSTRAINT [FK_MakeAndModel_Type] FOREIGN KEY([VehicleTypeID])
REFERENCES [Vehicle].[Type] ([VehicleTypeID])
GO
ALTER TABLE [Vehicle].[MakeAndModel] CHECK CONSTRAINT [FK_MakeAndModel_Type]
GO
/****** Object:  StoredProcedure [dbo].[vehicle_uspGetDealers]    Script Date: 9/21/2014 3:24:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Fagro Vizcaino Salcedo
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[vehicle_uspGetDealers] 
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT c.user_id_performance, UserId, CompanyName, Name, Lastname, Identification, 
		  Birthday, Phone, Mobile, Email, isDealer, [Address], CreatedDate, ModifiedDate, 
		  ModifiedBy, Username, PasswordHash, SecurityStamp, ConfirmationToken, 
		  IsConfirmed
	From app.Customer c
	Where c.isDealer = 1
	Order By CreatedDate
END






GO
/****** Object:  StoredProcedure [Vehicle].[usp_ActivatePublication]    Script Date: 9/21/2014 3:24:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Fagro Vizcaino Salcedo
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [Vehicle].[usp_ActivatePublication] 
	-- Add the parameters for the stored procedure here
	@id int, 
	@userId varchar(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	Declare @userIdPerformance int = (Select user_id_performance From app.Customer c Where c.UserId = @userId)
    -- Insert statements for procedure here
	Update Vehicle.Publication
	Set
	    --PublicationID - this column value is auto-generated
	    Active = 1, -- bit
	   	Modified = GETDATE()
	Where UserIdPerformance = @userIdPerformance And
		  PublicationID = @id
END






GO
/****** Object:  StoredProcedure [Vehicle].[usp_getAllVehiclePhotos]    Script Date: 9/21/2014 3:24:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Fagro Vizcaino Salcedo
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [Vehicle].[usp_getAllVehiclePhotos] 
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Select Top 20 rs.VehicleID, rs.PhotoThumbnail, rs.ActualPhoto, rs.Created
	From(  
			Select  p.VehicleID, p.PhotoThumbnail, p.ActualPhoto, p2.Created, RANK() Over( Partition By p.VehicleID  Order By p.CreatedDate) As Ranck
			From Vehicle.Photo p
				Join Vehicle.Publication p2
					On p2.VehicleID = p.VehicleID And p2.Expired = 0 And p2.Active = 1
			
		) rs
		where Ranck <= 4
	Order By Created desc
	
END






GO
/****** Object:  StoredProcedure [Vehicle].[usp_getAllVehicles]    Script Date: 9/21/2014 3:24:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Fagro Vizcaino Salcedo
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [Vehicle].[usp_getAllVehicles] 
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Select Top 20 mam.Make, mam.Model, v.Price, v.CurrencyId, v.[Year],
		   v.vehicleID, v.[Description], StateId, v.FuelID, v.VehicleConditionID,
		   v.CurrencyId
	From Vehicle.Vehicle v
		Join Vehicle.Publication p
			On p.VehicleID = v.vehicleID And p.Expired = 0 And p.Active = 1
		Join Vehicle.MakeAndModel mam
			On mam.MakeAndModelID = v.MakeAndModelID

	Order By PublicationID desc
END









GO
/****** Object:  StoredProcedure [Vehicle].[usp_getColors]    Script Date: 9/21/2014 3:24:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Fagro Vizcaino
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [Vehicle].[usp_getColors] 
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Select c.ColorID, Name, [Value], [Description],
		   Active
	From Vehicle.Color c
	Where c.Active = 1 
END


GO
/****** Object:  StoredProcedure [Vehicle].[usp_getCondition]    Script Date: 9/21/2014 3:24:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Fagro Vizcaino Salcedo
-- Create date: 2014-07-26
-- Description:	
-- =============================================
CREATE PROCEDURE [Vehicle].[usp_getCondition]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	Select c.ConditionID, c.Name
	From Vehicle.Condition c

END










GO
/****** Object:  StoredProcedure [Vehicle].[usp_getCurrency]    Script Date: 9/21/2014 3:24:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Fagro Vizcaino
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [Vehicle].[usp_getCurrency] 
	-- Add the parameters for the stored procedure here
	@id smallint = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	If	@id = 0
		Begin
			-- Insert statements for procedure here
			Select c.CurrencyId, c.Name, c.[Value], c.[Description], c.Created, c.Modified
			From Vehicle.Currency c
		End
	Else
			Select c.CurrencyId, c.Name, c.[Value], c.[Description], c.Created, c.Modified
			From Vehicle.Currency c
			Where c.CurrencyId = @id
END









GO
/****** Object:  StoredProcedure [Vehicle].[usp_getCustomerByUserID]    Script Date: 9/21/2014 3:24:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Fagro Vizcaino
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [Vehicle].[usp_getCustomerByUserID] 
	-- Add the parameters for the stored procedure here
	@userId uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT c.User_Id_performance, UserId, CompanyName, Name, 
		   Lastname, Identification, Birthday, Phone, Mobile, 
		   Email, isDealer, Address, CreatedDate, ModifiedDate, 
		   ModifiedBy, Username, PasswordHash, IsConfirmed, SecurityStamp
	From app.Customer c
	Where c.UserId = @userId
END






GO
/****** Object:  StoredProcedure [Vehicle].[usp_getCustomerByVehicleId]    Script Date: 9/21/2014 3:24:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Fagro Vizcaino Salcedo
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [Vehicle].[usp_getCustomerByVehicleId] 
	-- Add the parameters for the stored procedure here
	@vehicleId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	--Test
	--Declare @vehicleId int = 2
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Select c.User_Id_performance, UserId, CompanyName, Name, Lastname, 
		   Identification, Birthday, Phone, Mobile, Email, isDealer, 
		   [Address], CreatedDate, ModifiedDate, ModifiedBy, Username, 
		   PasswordHash, IsConfirmed, SecurityStamp
	
	From Vehicle.Publication p
		Join app.Customer c
			On c.User_Id_performance = p.UserIdPerformance

	Where p.VehicleID = @vehicleId
END






GO
/****** Object:  StoredProcedure [Vehicle].[usp_getCustomPlanByUserId]    Script Date: 9/21/2014 3:24:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Fagro Vizcaino Salcedo
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [Vehicle].[usp_getCustomPlanByUserId] 
	-- Add the parameters for the stored procedure here
	@userId int
As
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

			-- Insert statements for procedure here
			Select cp.CustomPlanId, cp.PlanId, cp.NotificationModelID, cp.Name,
			       cp.Price, cp.NumberOfImages, cp.SearchRankingMinutes, 
				   cp.AdvertisingDurationHours, cp.NumberVehiclesAllowed, 
				   cp.SearchRankVehiclesAllowed, cp.Created, cp.Modified
			From Vehicle.[Plan] p
				Join app.Customer c
					On p.PlanID = c.PlanId	
				Left Join Vehicle.CustomPlan cp
					On cp.PlanId = c.PlanId		
			Where c.user_id_performance = @userId
	
END





GO
/****** Object:  StoredProcedure [Vehicle].[usp_getDriveTrain]    Script Date: 9/21/2014 3:24:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Fagro Vizcaino Salcedo
-- Create date: 2014-07-17
-- Description:	
-- =============================================
CREATE PROCEDURE [Vehicle].[usp_getDriveTrain]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	Select dri.DriveTrainID, dri.Name, dri.Value, dri.[Description]
	From Vehicle.DriveTrain dri

END










GO
/****** Object:  StoredProcedure [Vehicle].[usp_getFuel]    Script Date: 9/21/2014 3:24:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Fagro Vizcaino Salcedo
-- Create date: 2014-07-17
-- Description:	
-- =============================================
CREATE PROCEDURE [Vehicle].[usp_getFuel]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	Select f.FuelID, f.Name, f.Value, f.[Description]
	From Vehicle.Fuel f

END










GO
/****** Object:  StoredProcedure [Vehicle].[usp_getMakeAndModel]    Script Date: 9/21/2014 3:24:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Fagro Vizcaino Salcedo
-- Create date: 2014-07-16
-- Description:	
-- =============================================
CREATE PROCEDURE [Vehicle].[usp_getMakeAndModel]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	Select mam.MakeAndModelID, mam.Make, Coalesce(mam.Model, '')As Model
	From Vehicle.MakeAndModel mam
	
END









GO
/****** Object:  StoredProcedure [Vehicle].[usp_getMakes]    Script Date: 9/21/2014 3:24:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Fagro Vizcaino Salcedo
-- Create date: 2014-07-17
-- Description:	
-- =============================================
CREATE PROCEDURE [Vehicle].[usp_getMakes]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT MakeAndModelID, Make, Model
	From Vehicle.MakeAndModel
	where model Is Null
	Order by Make;

END











GO
/****** Object:  StoredProcedure [Vehicle].[usp_getModelByMake]    Script Date: 9/21/2014 3:24:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Fagro Vizcaino Salcedo
-- Create date: 2014-07-17
-- Description:	
-- =============================================
CREATE PROCEDURE [Vehicle].[usp_getModelByMake]
	@make varchar(max)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT MakeAndModelID, Make, Model
	From Vehicle.MakeAndModel
	where make = @make and model Is Not Null
	Order by Model;


END
RETURN 0










GO
/****** Object:  StoredProcedure [Vehicle].[usp_getMotors]    Script Date: 9/21/2014 3:24:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Fagro Vizcaino Salcedo
-- Create date: 2014-07-17
-- Description:	
-- =============================================
CREATE PROCEDURE [Vehicle].[usp_getMotors]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	Select mo.MotorId, mo.Name, mo.Value, mo.CreatedDate,
		   mo.ModifiedDate, mo.ModifiedBy, mo.[Description]
	From Vehicle.Motor mo
	Order By mo.[Value]

END











GO
/****** Object:  StoredProcedure [Vehicle].[usp_getPlanByUserId]    Script Date: 9/21/2014 3:24:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Fagro Vizcaino Salcedo
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [Vehicle].[usp_getPlanByUserId] 
	-- Add the parameters for the stored procedure here
	@userId int
As
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;



			-- Insert statements for procedure here
			Select p.PlanID, p.NotificationModelID, p.Name, p.Price, 
				   p.NumberOfImages, p.SearchRankingMinutes, p.AdvertisingDurationHours,
				   p.NumberVehiclesAllowed, p.SearchRankVehiclesAllowed, p.Created, p.Modified
			From Vehicle.[Plan] p
				Join app.Customer c
					On p.PlanID = c.PlanId			
			Where c.user_id_performance = @userId
	
END





GO
/****** Object:  StoredProcedure [Vehicle].[usp_GetPublicationByCusID]    Script Date: 9/21/2014 3:24:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Fagro Vizcaino Salcedo
-- Create date: 2014-07-17
-- Description:	
-- =============================================
CREATE PROCEDURE [Vehicle].[usp_GetPublicationByCusID]
	@userId nvarchar(200)
As
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	Select pub.PublicationID, pub.UserIdPerformance, pub.VehicleID, 
		    pub.Active, pub.Created, pub.Modified,
		   mam.Make, mam.Model,veh.[Year], Price, CurrencyId --, pht.PhotoThumbnail 
	From Vehicle.Publication pub
		Join app.Customer cus
			On cus.User_Id_performance = pub.UserIdPerformance
		Join Vehicle.Vehicle veh
			On veh.vehicleID = pub.VehicleID	
		Join Vehicle.MakeAndModel mam
			On mam.MakeAndModelID = veh.MakeAndModelID
	    
	Where cus.UserId = @userId
			And
		pub.expired = 0
			and
		pub.active = 0;
		
END







GO
/****** Object:  StoredProcedure [Vehicle].[usp_getSimilarVehicles]    Script Date: 9/21/2014 3:24:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Fagro Vizcaino Salcedo
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [Vehicle].[usp_getSimilarVehicles] 
	-- Add the parameters for the stored procedure here
	@vehicleId int = 0 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	Declare @similarMakeAndModel int = (Select MakeAndModelID From Vehicle.Vehicle v Where v.vehicleID = @vehicleId)

    -- Insert statements for procedure here
	Select Top 3 v.vehicleID, Make, Model, v.[Year], Price, CurrencyId
	From Vehicle.Vehicle v
		Join Vehicle.MakeAndModel mam
			On mam.MakeAndModelID = v.MakeAndModelID
		Join Vehicle.Publication p 
			On p.vehicleId = v.vehicleID And p.Active = 1
	Where v.MakeAndModelID = @similarMakeAndModel
			And v.vehicleID <> @vehicleId

END






GO
/****** Object:  StoredProcedure [Vehicle].[usp_getTransmission]    Script Date: 9/21/2014 3:24:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Fagro Vizcaino Salcedo
-- Create date: 2014-07-17
-- Description:	
-- =============================================
CREATE PROCEDURE [Vehicle].[usp_getTransmission]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	Select trn.TransmissionID, trn.Name, trn.Value, trn.[Description]
	From Vehicle.Transmission trn

END










GO
/****** Object:  StoredProcedure [Vehicle].[usp_getVehicleByCusID]    Script Date: 9/21/2014 3:24:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Fagro Vizcaino Salcedo
-- Create date: 2014-08-03
-- Description:	
-- =============================================
CREATE PROCEDURE [Vehicle].[usp_getVehicleByCusID] 
	-- Add the parameters for the stored procedure here
	@UserId nvarchar(100)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	--TEST
	--DECLARE @customerID int = 1
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT v.vehicleID, mam.Make, mam.Model, v.[Year]
	FROM Vehicle.Vehicle v
		Join App.Customer c
			On c.UserId = @UserId
		JOIN Vehicle.Publication p
			ON p.VehicleID = v.vehicleID And c.User_Id_performance = p.UserIdPerformance
		JOIN Vehicle.MakeAndModel mam
			ON mam.MakeAndModelID = v.MakeAndModelID
	
END










GO
/****** Object:  StoredProcedure [Vehicle].[usp_getVehicleByID]    Script Date: 9/21/2014 3:24:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Fagro Vizcaino Salcedo
-- Create date: 2014-07-17
-- Description:	
-- =============================================
CREATE PROCEDURE [Vehicle].[usp_getVehicleByID]
	@VehicleID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.

	--Declare @VehicleID int = 4
	SET NOCOUNT ON;

	Select Top 1 veh.vehicleID,mam.Make, mam.Model, veh.[Year], veh.Price,  c.[Value] as ColorExteriorValue,
				 (Select c2.[Value] From Vehicle.Color c2 Where c2.ColorID = veh.ColorInteriorId) As ColorInteriorValue,
				 veh.TransmissionID As TransmissionId, trans.Name as TransmissionName, 
				 mot.MotorId As MotorId, mot.Name as MotorName, veh.Cilinders, veh.TotalSeating, 
				 cond.ConditionID As VehicleConditionId, cond.Name As VehicleConditionName, veh.[Description],
				 ful.FuelID as FuelId, ful.Name as FuelName, dt.DriveTrainID As DriveTrainId,
				 dt.Name as DriveTrainName, p.ActualPhoto, s.StateId, s.Name As StateName , veh.Tags, pub.Created
		  
		   
	From Vehicle.Vehicle veh
		Join Vehicle.MakeAndModel mam
			on veh.MakeAndModelID = mam.MakeAndModelID
		Join Vehicle.Photo p
			On p.VehicleID = @VehicleID
		Join Vehicle.Transmission trans
			on trans.TransmissionID = veh.TransmissionID
		Join Vehicle.Motor mot
			on mot.MotorId = veh.MotorID
		Join Vehicle.Fuel ful
			on ful.FuelID = veh.FuelID
		Join Vehicle.DriveTrain dt
			on dt.DriveTrainID = veh.DriveTrainID
		Join Vehicle.Condition cond
			on cond.ConditionID = veh.VehicleConditionID
		Join Vehicle.Publication pub
			on pub.VehicleID = veh.vehicleID
			and pub.Active = 1
		Join vehicle.State s
			On s.StateId = veh.StateId
		Join vehicle.Color c
			On c.ColorID = veh.ColorExteriorId

	Where veh.vehicleID = @VehicleID
END

	

GO
/****** Object:  StoredProcedure [Vehicle].[usp_GetVehicleByMakeAndModel]    Script Date: 9/21/2014 3:24:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Fagro Vizcaino Salcedo
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [Vehicle].[usp_GetVehicleByMakeAndModel] 
	-- Add the parameters for the stored procedure here
	@searchCriteria varchar(65)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

		--Declare @searchCriteria varchar(65) = 'Honda';	
	
		Select  mam.Make, mam.Model, v.Price, v.CurrencyId, v.[Year], v.vehicleID, v.[Description],CurrencyId
		From Vehicle.Vehicle v
			Join Vehicle.Publication p
				On p.VehicleID = v.vehicleID And p.Expired = 0 And p.Active = 1
			Join Vehicle.MakeAndModel mam
				On mam.MakeAndModelID = v.MakeAndModelID
		Where mam.Make +' '+ mam.Model+' '+cast(v.[Year] As varchar(4)) Like '%'+@searchCriteria+'%'
		Order By [Year] Desc 
		
	
End










GO
/****** Object:  StoredProcedure [Vehicle].[usp_getVehicleByMakeAndModelID]    Script Date: 9/21/2014 3:24:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Fagro Vizcaino Salcedo
-- Create date: 2014-07-16
-- Description:	
-- =============================================
CREATE PROCEDURE [Vehicle].[usp_getVehicleByMakeAndModelID]
	@makeAndModelID int
AS
BEGIN 
	SET NOCOUNT ON;

	SELECT veh.vehicleID,mam.Make, mam.Model, mam.[Year], veh.Price, 
			c.[Value] As ColorExteriorValue, 
		   (Select c2.[Value] From Vehicle.Color c2 Where c2.ColorID = veh.ColorInteriorId) As ColorInteriorValue, 
		   trans.Name as TransmissionName, mot.Name as MotorName,
		   ful.Name as FuelName, dt.Name as DriveTrainName, cond.Name as VehicleConditionName
	From Vehicle.Vehicle veh
		Join Vehicle.MakeAndModel mam
			on veh.MakeAndModelID = @makeAndModelID
		Join Vehicle.Transmission trans
			on trans.TransmissionID = veh.TransmissionID
		Join Vehicle.Motor mot
			on mot.MotorId = veh.MotorID
		Join Vehicle.Fuel ful
			on ful.FuelID = veh.FuelID
		Join Vehicle.DriveTrain dt
			on dt.DriveTrainID = veh.DriveTrainID
		Join Vehicle.Condition cond
			on cond.ConditionID = veh.VehicleConditionID
		Join Vehicle.Publication pub
			on pub.VehicleID = veh.vehicleID
			and pub.Active = 1
		Join vehicle.Color c
			On c.ColorID = veh.ColorExteriorId

END;










GO
/****** Object:  StoredProcedure [Vehicle].[usp_getVehicleHighlights]    Script Date: 9/21/2014 3:24:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Fagro Vizcaino Salcedo
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [Vehicle].[usp_getVehicleHighlights] 
	-- Add the parameters for the stored procedure here 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT v.vehicleID, Make, Model, v.[Year], Price, CurrencyId
	From Vehicle.Vehicle v
		Join Vehicle.MakeAndModel mam
			On mam.MakeAndModelID = v.MakeAndModelID
		Join Vehicle.Highlights h
			On h.vehicleId = v.vehicleID
	Where h.Active = 1

END






GO
/****** Object:  StoredProcedure [Vehicle].[usp_getVehiclePhotoByVehicleID]    Script Date: 9/21/2014 3:24:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Fagro Vizcaino Salcedo
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [Vehicle].[usp_getVehiclePhotoByVehicleID] 
	-- Add the parameters for the stored procedure here
	@vehicleIds VehicleIdTableType ReadOnly
									 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

   Select  p.PhotoID,p.VehicleID,p.PhotoThumbnail, p.ActualPhoto
		From Vehicle.Photo p
			Join (Select Id From @vehicleIds) v
				On v.Id = p.VehicleID
			Join Vehicle.Publication p2
				On p2.VehicleID = p.VehicleID And p2.Expired = 0 And p2.Active = 1

		Order By p2.Created Desc

END








GO
/****** Object:  StoredProcedure [Vehicle].[usp_getVehiclePlan]    Script Date: 9/21/2014 3:24:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Fagro Vizcaino Salcedo
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [Vehicle].[usp_getVehiclePlan]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Select p.PlanID, p.NotificationModelID, p.Name, p.Price, p.NumberOfImages, 
	       p.SearchRankingMinutes / 60 As SearchRankingMinutes , p.AdvertisingDurationHours / 24 As AdvertisingDurationHours, p.NumberVehiclesAllowed, 
		   p.SearchRankVehiclesAllowed, p.Created, p.Modified 
	From Vehicle.[Plan] p
END





GO
/****** Object:  StoredProcedure [Vehicle].[usp_getVehicleStates]    Script Date: 9/21/2014 3:24:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Fagro Vizcaino Salcedo
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [Vehicle].[usp_getVehicleStates] 
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Select s.StateId, Name, [Value], Active, Created, Modified 
	From Vehicle.[State] s
END








GO
/****** Object:  StoredProcedure [Vehicle].[usp_insertUpdatePublication]    Script Date: 9/21/2014 3:24:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Fagro Vizcaino Salcedo
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [Vehicle].[usp_insertUpdatePublication]
	-- Add the parameters for the stored procedure here
	@publicationID int = 0, 
	@customerID varchar(Max),
	@vehicleID int,
	@active bit = 1,
	@expired bit = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;



    -- Insert statements for procedure here
	Merge Vehicle.Publication As Target
		Using (Select @publicationID, @customerID, @vehicleID, @active, @expired) As 
		Source ( PublicationID, UserIdPerformance, VehicleID, Active, Expired)
			On Target.PublicationID = Source.PublicationID
		When Matched Then
			Update Set UserIdPerformance = Source.UserIdPerformance,
			           VehicleID = Source.VehicleID,
					   Active = Source.Active,
					   Expired = Source.Expired
		When Not Matched Then
			Insert (UserIdPerformance, VehicleID, Active, Expired)

			values(Source.UserIdPerformance, Source.VehicleID, Source.Active, Source.Expired)

		Output $ACTION;
END





GO
/****** Object:  StoredProcedure [Vehicle].[usp_InsertUpdateVehicle]    Script Date: 9/21/2014 3:24:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Fagro Vizcaino Salcedo
-- Create date: 2014-07-15
-- Description:	
-- =============================================
CREATE PROCEDURE [Vehicle].[usp_InsertUpdateVehicle] 
	-- Add the parameters for the stored procedure here
	@vehicleID int,
	@makeAndModelID int, 
	@motorID smallint,
	@TransmissionID smallint,
	@FuelID smallint,
	@DriveTrainID smallint,
	@VehicleConditionID smallint,
	@Cilinders smallint,
	@Price decimal(14,2),
	@TotalSeating smallint,
	@ColorExteriorId smallint,
	@ColorInteriorId smallint,
	@Year smallint,
	@CurrencyId smallint,
	@Tags nvarchar(Max) = '',
	@Description nvarchar(max) = '',
	@StateId smallint
	

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	

    -- Insert statements for procedure here
	Merge vehicle.Vehicle as Target
		Using (select @vehicleID, @makeAndModelID, @motorID, @TransmissionID,
					  @FuelID, @DriveTrainID, @VehicleConditionID,
					  @Cilinders, @Price, @TotalSeating,
					  @ColorExteriorId, @ColorInteriorId, @Year,@CurrencyId, @Tags, @Description, @StateId)
		 As Source (vehicleID, makeAndModelID, motorID, TransmissionID,
				    FuelID, DriveTrainID, VehicleConditionID, Cilinders,
					Price, TotalSeating, ColorExteriorId, ColorInteriorId, [Year], CurrencyId, Tags, [Description], StateId)
		On(Target.VehicleID = Source.VehicleID)

			When Matched Then
				Update Set makeandModelID = Source.MakeAndModelID,
						  MotorID = Source.MotorID,
						  TransmissionID = Source.TransmissionID,
						  FuelID = Source.FuelID,
						  DriveTrainID = Source.DriveTrainID,
						  VehicleConditionID = Source.VehicleConditionID,
						  Cilinders = Source.Cilinders,
						  Price = Source.Price,
						  TotalSeating = Source.TotalSeating,
						  ColorExteriorId = Source.ColorExteriorId,
						  ColorInteriorId = Source.ColorInteriorId,
						  [Year] = Source.[Year],
						  CurrencyId = Source.CurrencyId,
						  Tags  = Source.Tags,
						  [description] = source.[description],
						  StateId = Source.StateId
			When Not Matched Then
				Insert (MakeAndModelID, MotorId, TransmissionID, FuelID,
						DriveTrainID, VehicleConditionID, Cilinders,
						Price, TotalSeating, ColorExteriorId,ColorInteriorId, [Year], CurrencyId, Tags, [description], stateId)
				
				Values (Source.MakeAndModelID, Source.MotorID, Source.TransmissionID, 
						Source.FuelID,Source.DriveTrainID, Source.VehicleConditionID, Source.Cilinders,
						Source.Price, Source.TotalSeating, Source.ColorExteriorId, Source.ColorInteriorId, Source.[year], 
						Source.CurrencyId , Source.Tags, Source.[Description], Source.StateId)

						OUTPUT $action, INSERTED.vehicleID;
		END;










GO
/****** Object:  StoredProcedure [Vehicle].[usp_insertUpdateVehiclePhoto]    Script Date: 9/21/2014 3:24:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Fagro Vizcaino Salcedo
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [Vehicle].[usp_insertUpdateVehiclePhoto] 
	-- Add the parameters for the stored procedure here
	@photoId int, 
	@VehicleID int,
	@PhotoThumbnail varchar(160),
	@ActualPhoto varchar(160)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Merge Vehicle.Photo As Target
		Using (Select @photoId, @VehicleID, @photoThumbnail,@ActualPhoto) As 
			Source(PhotoID, VehicleID, PhotoThumbnail, ActualPhoto)
				On (Target.PhotoID = Source.PhotoID)
		When Matched Then
			Update Set VehicleID = Source.VehicleID,
					   PhotoThumbnail = Source.PhotoThumbnail,
					   ActualPhoto = Source.ActualPhoto
		When Not Matched Then
			Insert 
			(
				--PhotoID - this column value is auto-generated
				VehicleID,
				PhotoThumbnail,
				ActualPhoto,
				MainPhoto
			)
			Values
			(
				-- PhotoID - int
				Source.VehicleID, -- VehicleID - int
				Source.PhotoThumbnail, -- PhotoThumbnail - varchar
				Source.ActualPhoto, -- ActualPhoto - varchar
				0 -- MainPhoto - bit
			)

	Output $ACTION;

END









GO
/****** Object:  StoredProcedure [Vehicle].[usp_UpsertUser]    Script Date: 9/21/2014 3:24:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		vehicle.usp_RegisterUser
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [Vehicle].[usp_UpsertUser] 
	-- Add the parameters for the stored procedure here
	@UserId UNIQUEIDENTIFIER = '',
	@username varchar(max), 
	@password varchar(max),
	@confirmPassword bit = 0,
	@company varchar(50),
	@name varchar(60),
	@lastname varchar(60),
	@birthday varchar(10),
	@Phone varchar(10),
	@mobile varchar(10),
	@email varchar(100),
	@isDealer bit = false,
	@Address varchar(400),
	@SecurityStamp nvarchar(max),
	@ConfirmationToken varchar(Max) = '',
	@IsConfirmed bit = 0
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Merge App.Customer As Target
		Using (Select @UserId, @name, @lastname, @birthday,@email, @username, @password, @company, @phone, @mobile, @isDealer, @Address, @SecurityStamp, @ConfirmationToken, @IsConfirmed) As 
		Source (UserId, Name,Lastname,Birthday,Email,Username,PasswordHash,CompanyName, Phone,Mobile, isDealer, [Address], SecurityStamp, ConfirmationToken, Isconfirmed) 
			On Target.UserId = Source.UserId

		When Matched Then
			Update Set Name = Source.Name,
					  Lastname = Source.Lastname,
					  BirthDay = Source.BirthDay,
					  Email = Source.Email,
					  username = Source.username,
					  PasswordHash = Source.PasswordHash,
					  CompanyName = Source.CompanyName,
					  phone = Source.Phone,
					  Mobile = Source.Mobile,
					  IsDealer = Source.isDealer,
					  [Address]= Source.[Address],
					  SecurityStamp = Source.SecurityStamp,
					  ConfirmationToken = Source.ConfirmationToken,
					  IsConfirmed = Source.IsConfirmed
		When Not Matched Then
			Insert(
					UserId, CompanyName, Name, Lastname, 
					Birthday, Phone, Mobile, Email, isDealer, [Address], 
					Username, PasswordHash, SecurityStamp, ConfirmationToken, IsConfirmed
				  )
			Values
			(
				Source.UserId, Source.CompanyName, Source.Name, Source.Lastname, 
				Source.Birthday, Source.Phone, Source.Mobile, Source.Email, Source.isDealer, Source.[Address], 
				Source.Username, Source.PasswordHash, Source.SecurityStamp, Source.ConfirmationToken, Source.IsConfirmed
			)

		Output $ACTION;

END








GO
/****** Object:  StoredProcedure [Vehicle].[uspUpdatePlan]    Script Date: 9/21/2014 3:24:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Fagro Vizcaino Salcedo
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [Vehicle].[uspUpdatePlan] 
	-- Add the parameters for the stored procedure here
	@userId varchar(max), 
	@planId int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.

	Declare @userIdPerformance int = (Select c.user_id_performance From app.Customer c Where c.userId = @userId )
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Update App.Customer
	Set
	    PlanId = @planId -- smallint
	Where user_id_performance = user_id_performance
END





GO
USE [master]
GO
ALTER DATABASE [eCars] SET  READ_WRITE 
GO
