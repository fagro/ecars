﻿using System.Collections.Generic;
using eCars.Domain.Entities;

namespace eCars.Domain.Interfaces
{
    public interface IVehiclePhotoRepository
    {
        void Insert(VehiclePhoto vehiclePhoto);
        void Delete(VehiclePhoto entity);
        IEnumerable<VehiclePhoto> SearchFor(IEnumerable<Vehicle>  vehicles);
        IEnumerable<VehiclePhoto> GetAll();
        VehiclePhoto GetById(int id);
    }
}
