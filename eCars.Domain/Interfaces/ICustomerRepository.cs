﻿using System.Collections.Generic;
using eCars.Domain.Entities;

namespace eCars.Domain.Interfaces
{
    public interface ICustomerRepository
    {
        void Insert(Customer entity);
        void Delete(Customer entity);
        IEnumerable<Customer> GetCondition();
        IEnumerable<Customer> GetConditionById(int conditionId);
        /// <summary>
        /// Get a intance of customer.
        /// </summary>
        /// <param name="userId">Use the user guid identifier to search</param>
        /// <returns>IEnumerable of vehicles</returns>
        Customer GetById(string id);
        Customer GetCustomerByVehicleId(int id);
        IEnumerable<Customer> GetDealers();
        Customer GetDealerById();

    }
}
