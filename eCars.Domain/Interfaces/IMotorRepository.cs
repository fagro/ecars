﻿using System.Collections.Generic;
using eCars.Domain.Entities;

namespace eCars.Domain.Interfaces
{
    public interface IMotorRepository
    {
        void Insert(Motor entity);
        void Delete(Motor entity);
        IEnumerable<Motor> GetMotors();
        IEnumerable<Motor> GetMotorsByMotorId(int motorId);
        Motor GetById(int id);
    }
}
