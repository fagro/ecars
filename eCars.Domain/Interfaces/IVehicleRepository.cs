﻿using System.Collections.Generic;
using eCars.Domain.Entities;

namespace eCars.Domain.Interfaces
{
    public interface IVehicleRepository
    {
        int Insert(Vehicle entity);
        void Delete(Vehicle entity);
        IEnumerable<Vehicle> SearchFor(string searchTerm);
        IEnumerable<Vehicle> GetAll();
        IEnumerable<Vehicle> GetVehiclesByCustomerId(string userId); 
        Vehicle GetById(int id);
        IEnumerable<Vehicle> GetVehicleHighlights();
        IEnumerable<Vehicle> GetSimilarVehicles(int id);
    }
}
