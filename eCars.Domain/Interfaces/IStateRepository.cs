﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using eCars.Domain.Entities;

namespace eCars.Domain.Interfaces
{
    public interface IStateRepository
    {
        void Insert(States entity);
        void Delete(States entity);
        IEnumerable<States> GetStates();
        IEnumerable<States> GetStateById(int stateId);
        States GetById(int id);
    }
}
