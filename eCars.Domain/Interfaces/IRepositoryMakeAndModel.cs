﻿using System.Collections.Generic;
using eCars.Domain.Entities;

namespace eCars.Domain.Interfaces
{
    public interface IMakeAndModelRepository
    {
        void Insert(MakeAndModel entity);
        void Delete(MakeAndModel entity);
        IEnumerable<MakeAndModel> GetMakes();
        IEnumerable<MakeAndModel> GetModelByMake(string make);
        IEnumerable<MakeAndModel> GetMakesAndModels(); 
        MakeAndModel GetById(int id);
    }
}
