﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using eCars.Domain.Entities;

namespace eCars.Domain.Interfaces
{
    public interface IVehiclePlanRepository
    {
        void Insert(Plan entity);
        void Delete(Plan entity);
        IEnumerable<Plan> GetPlans();
        IEnumerable<Plan> GetCurrencyById(int planId);
        Plan GetById(int id);
        int Update(int planId, string userId);
        Plan GetPlanByUser(int userId);
        CustomPlan GetCustomPlanByUser(int userId);
    }
}
