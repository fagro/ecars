﻿using System.Collections.Generic;
using eCars.Domain.Entities;

namespace eCars.Domain.Interfaces
{
    public interface IConditionRepository
    {
        void Insert(Condition entity);
        void Delete(Condition entity);
        IEnumerable<Condition> GetCondition();
        IEnumerable<Condition> GetConditionById(int conditionId);
        Condition GetById(int id);
    }
}
