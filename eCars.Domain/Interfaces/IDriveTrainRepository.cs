﻿using System.Collections.Generic;
using eCars.Domain.Entities;

namespace eCars.Domain.Interfaces
{
    public interface IDriveTrainRepository
    {
        void Insert(DriveTrain entity);
        void Delete(DriveTrain entity);
        IEnumerable<DriveTrain> GetDriveTrain();
        IEnumerable<DriveTrain> GetDriveTrainBy(int driveTrainId);
        DriveTrain GetById(int id);
    }
}
