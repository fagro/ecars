﻿using System.Collections.Generic;
using eCars.Domain.Entities;

namespace eCars.Domain.Interfaces
{
    public interface IFuelRepository
    {
        void Insert(Fuel entity);
        void Delete(Fuel entity);
        IEnumerable<Fuel> GetFuels();
        IEnumerable<Fuel> GetFuelById(int fuelId);
        Fuel GetById(int id);
    }
}
