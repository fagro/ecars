﻿using System.Collections.Generic;
using eCars.Domain.Entities;

namespace eCars.Domain.Interfaces
{
    public interface IColorRepository
    {
        void Insert(Color entity);
        void Delete(Color entity);
        IEnumerable<Color> GetColors();
        IEnumerable<Color> GetColorsById(int colorId);
        Color GetById(int id);
    }
}
