﻿using System.Collections.Generic;
using eCars.Domain.Entities;

namespace eCars.Domain.Interfaces
{
    public interface ITransmissionRepository
    {
        void Insert(Transmission entity);
        void Delete(Transmission entity);
        IEnumerable<Transmission> GetTransmission();
        IEnumerable<Transmission> GetTransmissionById(int transmissionId);
        Transmission GetById(int id);
    }
}
