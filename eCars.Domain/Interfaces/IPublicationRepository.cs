﻿using System.Collections.Generic;
using eCars.Domain.Entities;

namespace eCars.Domain.Interfaces
{
    public interface IPublicationRepository
    {
        void Insert(Publication entity);
        void Delete(Publication entity);
        IEnumerable<Publication> SearchFor(string searchTerm);
        IEnumerable<Publication> GetAll();
        IEnumerable<Publication> GetByClientId(int id);
        int Publish(int publicationId, string userId);
        Publication GetById(int id);
    }
}
