﻿using eCars.Domain.Entities;
using System.Collections.Generic;

namespace eCars.Domain.Interfaces
{
    public interface ICurrencyRepository
    {
        void Insert(Currency entity);
        void Delete(Currency entity);
        IEnumerable<Currency> GetCurrency();
        IEnumerable<Currency> GetCurrencyById(int currencyId);
        Currency GetById(int id);
    }
}
