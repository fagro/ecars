﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCars.Domain.Entities
{
    public class CustomPlan
    {
        public int CustomPlanId { get; set; }
        public int PlanId { get; set; }
        public short NotificationModelId { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public short NumberOfImages { get; set; }
        public short SearchRankingMinutes { get; set; }
        public short AdvertisingDurationHours { get; set; }
        public short NumberVehiclesAllowed { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
    }
}
