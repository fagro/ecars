﻿using System;

namespace eCars.Domain.Entities
{
    public class Publication
    {
        public int PublicationId { get; set; }
        public int CustomerId { get; set; }
        public int VehicleId { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public short Year { get; set; }
        public decimal Price { get; set; }
        public short CurrencyId { get; set; }
        public int PlanId { get; set; }
        public bool Active { get; set; }
        public string PhotoThumbnail { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public bool Expired { get; set; }
    }
}
