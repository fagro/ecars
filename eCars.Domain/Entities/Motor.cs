﻿using System;

namespace eCars.Domain.Entities
{
    public class Motor
    {
        public int MotorId { get; set; }
        public string Name { get; set; }
        public decimal Value { get; set; }
        public string Description { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public short ModifiedBy { get; set; }

    }
}
