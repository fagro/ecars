﻿using System;

namespace eCars.Domain.Entities
{
    public class Vehicle
    {
        public int VehicleId { get; set; }
        public int MakeAndModelId { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public short MotorId { get; set; }
        public string MotorName { get; set; }
        public short TransmissionId { get; set; }
        public string TransmissionName { get; set; }
        public short FuelId { get; set; }
        public string FuelName { get; set; }
        public short DriveTrainId { get; set; }
        public string DriveTrainName { get; set; }
        public short CurrencyId { get; set; }
        public short VehicleConditionId { get; set; }
        public short StateId { get; set; }
        public string StateName { get; set; }
        public string VehicleConditionName { get; set; }
        public short Cilinders { get; set; }
        public decimal Price { get; set; }
        public short TotalSeating { get; set; }
        public short ColorExteriorId { get; set; }
        public short ColorInteriorId { get; set; }
        public string ColorInteriorValue { get; set; }
        public string ColorExteriorValue { get; set; }
        public short Year { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public string Tags { get; set; }
        public string Description { get; set; }   
    }
}
