﻿using System;
using System.Collections.Generic;

namespace eCars.Domain.Entities
{
   public class VehiclePhoto
    {
        public int PhotoId { get; set; }
        public int VehicleId { get; set; }
        public IEnumerable<string> PhotoThumbnail { get; set; }
        public IEnumerable<string> ActualPhoto { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
    }
}
