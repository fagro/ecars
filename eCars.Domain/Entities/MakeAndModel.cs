﻿using System;

namespace eCars.Domain.Entities
{
    public class MakeAndModel
    {
        public int MakeAndModelId { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public short VehicleTypeId { get; set; }
        public short Year { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public short ModifiedBy { get; set; }
        public bool Active { get; set; }
        public string Description { get; set; }

    }
}
