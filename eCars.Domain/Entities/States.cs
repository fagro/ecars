﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCars.Domain.Entities
{
    public class States
    {
        public int StateId { get; set; }
        public string Name { get; set; }
        public short Value { get; set; }
        public bool Active { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }

    }
}
