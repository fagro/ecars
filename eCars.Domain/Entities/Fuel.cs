﻿using System;

namespace eCars.Domain.Entities
{
    public class Fuel
    {
        public int FuelId { get; set; }
        public string Name { get; set; }
        public decimal Value { get; set; }
        public string Description { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public bool Active { get; set; }
        public short ModifiedBy { get; set; }

    }
}
