﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace eCars.Domain.Entities
{
    public class Customer
    {
        public int User_Id_performance { get; set; }
        public Guid UserId { get; set; }
        public string CompanyName { get; set; }
        public string Name { get; set; }
        public string Lastname { get; set; }
        public string Identification { get; set; }
        public DateTime Birthday { get; set; }
        public long Phone { get; set; }
        public long Mobile { get; set; }
        public string Email { get; set; }
        public bool IsDealer { get; set; }
        public string Address { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public short ModifiedBy { get; set; }
        public string UserName { get; set; }
        public string PasswordHash { get; set; }
        public bool isConfirmed { get; set; }
        public string SecurityStamp { get; set; }
    }
}
