﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using eCars.Domain.Entities;
using eCars.Domain.Interfaces;
using System.Data.SqlClient;
using Dapper;
using System.Data;

namespace eCars.Domain.Repository
{
    public class VehiclePlanRepository : RepositoryBase, IVehiclePlanRepository
    {
         private readonly IDbConnection _conn;

         public VehiclePlanRepository()
        {
            _conn = new SqlConnection
            {
                ConnectionString = ConnectionString
            };
        }

        public void Insert(Plan entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(Plan entity)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Plan> GetPlans()
        {
          return _conn.Query<Plan>("vehicle.usp_getVehiclePlan", commandType: CommandType.StoredProcedure);
        }

        public IEnumerable<Plan> GetCurrencyById(int planId)
        {
            throw new NotImplementedException();
        }

        public Plan GetById(int id)
        {
            throw new NotImplementedException();
        }

        public int Update(int planId, string userId)
        {
            return _conn.Execute("vehicle.uspUpdatePlan", new { userId = userId, planId = planId }, commandType: CommandType.StoredProcedure);
        }


        public Plan GetPlanByUser(int userId)
        {
            return _conn.Query<Plan>("vehicle.usp_getPlanByUserId", new { userId = userId}, commandType: CommandType.StoredProcedure).FirstOrDefault();
        }


        public CustomPlan GetCustomPlanByUser(int userId)
        {
            return _conn.Query<CustomPlan>("vehicle.usp_getCustomPlanByUserId", new { userId = userId }, commandType: CommandType.StoredProcedure).FirstOrDefault();
        }
    }
}
