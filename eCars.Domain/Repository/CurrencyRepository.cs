﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dapper;
using System.Threading.Tasks;
using eCars.Domain.Entities;
using eCars.Domain.Interfaces;
using System.Data;
using System.Data.SqlClient;

namespace eCars.Domain.Repository
{
    public class CurrencyRepository : RepositoryBase, ICurrencyRepository
    {
        private readonly IDbConnection _conn;

        public CurrencyRepository()
        {
            _conn = new SqlConnection
            {
                ConnectionString = ConnectionString
            };
        }
        public void Insert(Currency entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(Currency entity)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Currency> GetCurrency()
        {
            try
            {
                return _conn.Query<Currency>("vehicle.usp_getCurrency", commandType:CommandType.StoredProcedure);
            }
            finally
            {
                _conn.Close();
            }
        }

        public IEnumerable<Currency> GetCurrencyById(int currencyId)
        {
            throw new NotImplementedException();
        }

        public Currency GetById(int id)
        {
            throw new NotImplementedException();
        }
    }
}
