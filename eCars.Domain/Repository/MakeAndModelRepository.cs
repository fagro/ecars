﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using eCars.Domain.Entities;
using eCars.Domain.Interfaces;

namespace eCars.Domain.Repository
{
    public class MakeAndModelRepository : RepositoryBase, IMakeAndModelRepository
    {
        private readonly IDbConnection conn;

        public MakeAndModelRepository()
        {
            conn = new SqlConnection { ConnectionString = ConnectionString };
        }

        public void Insert(MakeAndModel entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(MakeAndModel entity)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<MakeAndModel> GetMakes()
        {
            try
            {
                return conn.Query<MakeAndModel>("vehicle.usp_getMakes", commandType: CommandType.StoredProcedure);
            }
            finally
            {
                conn.Close();
            }

        }

        public IEnumerable<MakeAndModel> GetModelByMake(string make)
        {
            try
            {
                return conn.Query<MakeAndModel>("vehicle.usp_getModelByMake", new { Make = make}, commandType: CommandType.StoredProcedure);;
            }
            finally
            {
                conn.Close();
            }
        }

        public MakeAndModel GetById(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<MakeAndModel> GetMakesAndModels()
        {
            try
            {
                return conn.Query<MakeAndModel>("vehicle.usp_getMakeAndModel", commandType: CommandType.StoredProcedure);
            }
            finally
            {
                conn.Close();
            }
        }
    }
}
