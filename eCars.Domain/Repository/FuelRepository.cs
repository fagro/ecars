﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Dapper;
using eCars.Domain.Entities;
using eCars.Domain.Interfaces;

namespace eCars.Domain.Repository
{
    public class FuelRepository : RepositoryBase, IFuelRepository
    {
        private readonly IDbConnection conn;
        public FuelRepository()
        {
            conn = new SqlConnection {ConnectionString = ConnectionString};
        }

        public void Insert(Fuel entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(Fuel entity)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Fuel> GetFuels()
        {
            try
            {
                return conn.Query<Fuel>("vehicle.usp_getFuel", commandType: CommandType.StoredProcedure);
            }
            finally
            {
                conn.Close();
            }
        }

        public IEnumerable<Fuel> GetFuelById(int fuelId)
        {
            throw new NotImplementedException();
        }

        public Fuel GetById(int id)
        {
            throw new NotImplementedException();
        }
    }
}
