﻿using System.Configuration;

namespace eCars.Domain.Repository
{
     public class RepositoryBase
    {
        private readonly string _connectionString;
        public RepositoryBase()
        {
            _connectionString = ConfigurationManager.ConnectionStrings["eCarsDB"].ConnectionString;
        }

        public string ConnectionString { get { return _connectionString; } }
        
    }
}
