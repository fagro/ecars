﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using eCars.Domain.Entities;
using eCars.Domain.Interfaces;

namespace eCars.Domain.Repository
{
    public class VehicleRepository : RepositoryBase, IVehicleRepository
    {
        private readonly IDbConnection _conn;

        public VehicleRepository()
        {
            _conn = new SqlConnection { ConnectionString = ConnectionString };
            _conn.Open();
        }

        public int Insert(Vehicle vehicle)
        {
            try
            {
                var parameters = new DynamicParameters();
                parameters.Add("@VehicleID", vehicle.VehicleId);
                parameters.Add("@makeAndModelID", vehicle.MakeAndModelId);
                parameters.Add("@motorID", vehicle.MotorId);
                parameters.Add("@TransmissionID", vehicle.TransmissionId);
                parameters.Add("@FuelID", vehicle.FuelId);
                parameters.Add("@DriveTrainID", vehicle.DriveTrainId);
                parameters.Add("@VehicleConditionID", vehicle.VehicleConditionId);
                parameters.Add("@Cilinders", vehicle.Cilinders);
                parameters.Add("@Price", vehicle.Price);
                parameters.Add("@TotalSeating", vehicle.TotalSeating);
                parameters.Add("@ColorExteriorId", vehicle.ColorExteriorId);
                parameters.Add("@ColorInteriorId", vehicle.ColorInteriorId);
                parameters.Add("@CurrencyId", vehicle.CurrencyId);
                parameters.Add("@Year", vehicle.Year);
                parameters.Add("@Tags", vehicle.Tags);
                parameters.Add("@Description", vehicle.Description);
                parameters.Add("@StateId", vehicle.StateId);
                var data = _conn.ExecuteReader("Vehicle.usp_InsertUpdateVehicle", parameters, commandType: CommandType.StoredProcedure);
                var vehicleId = data.Read() ? data.GetInt32(1) : 0;
                return vehicleId;
            }
            finally
            {
                _conn.Close();

            }
        }

        public IEnumerable<Vehicle> SearchFor(string makeAndModel)
        {
            try
            {
                return _conn.Query<Vehicle>("Vehicle.usp_GetVehicleByMakeAndModel",
                                            new { searchCriteria = makeAndModel},
                                             commandType: CommandType.StoredProcedure);
            }
            finally
            {
                _conn.Close();
            }

        }

        public IEnumerable<Vehicle> GetAll()
        {
            try
            {
                return _conn.Query<Vehicle>("vehicle.usp_getAllVehicles", commandType: CommandType.StoredProcedure);

            }
            finally
            {
                _conn.Close();
            }
        }

        public Vehicle GetById(int id)
        {
            try
            {
                var result = _conn.Query<Vehicle>("vehicle.usp_getVehicleByID",
                    new { VehicleID = id },
                    commandType: CommandType.StoredProcedure).First();
                return result;
            }
            finally
            {
                _conn.Close();
            }

        }
       
        public IEnumerable<Vehicle> GetVehiclesByCustomerId(string userId)
        {
            try
            {
                var result = _conn.Query<Vehicle>("vehicle.usp_getVehicleByCusID", new { UserId = userId},
                    commandType: CommandType.StoredProcedure);
                return result;
            }
            finally
            {
                _conn.Close();
            }
        }

        public void Delete(Vehicle entity)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Vehicle> GetVehicleHighlights()
        {
            try
            {
                var result = _conn.Query<Vehicle>("vehicle.usp_getVehicleHighlights",
                    commandType: CommandType.StoredProcedure);
                return result;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                _conn.Close();
            }
        }

        public IEnumerable<Vehicle> GetSimilarVehicles(int id)
        {
            try
            {
                var result = _conn.Query<Vehicle>("vehicle.usp_getSimilarVehicles", new {vehicleId = id},
                    commandType: CommandType.StoredProcedure);
                return result;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                _conn.Close();
            }
        }
    }
}
