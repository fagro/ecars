﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq.Expressions;
using Dapper;
using Dapper.Tvp;
using System.Linq;
using eCars.Domain.Entities;
using eCars.Domain.Interfaces;
using Microsoft.SqlServer.Server;

namespace eCars.Domain.Repository
{
    public class VehiclePhotoRepository : RepositoryBase, IVehiclePhotoRepository
    {
        private readonly IDbConnection _conn;
        public VehiclePhotoRepository()
        {
            _conn = new SqlConnection { ConnectionString = ConnectionString };
        }

        public void Insert(VehiclePhoto entity)
        {
            var thumb = "";
            var actual = "";
            foreach (var item in entity.PhotoThumbnail)
            {
                thumb = item;
            }

            foreach (var item in entity.ActualPhoto)
            {
                actual = item;
            }
            try
            {
                var parameters = new DynamicParameters();
                parameters.Add("@PhotoID", entity.PhotoId);
                parameters.Add("@VehicleID", entity.VehicleId);
                parameters.Add("@PhotoThumbnail", thumb);
                parameters.Add("@ActualPhoto", actual);
                _conn.Execute("vehicle.usp_insertUpdateVehiclePhoto", parameters, commandType: CommandType.StoredProcedure);
            }
            finally
            {
                _conn.Close();
            }
        }

        public IEnumerable<VehiclePhoto> SearchFor(IEnumerable<Vehicle> vehicles)
        {
            var updatedOrderIdsParameter = new List<SqlDataRecord>();
            var myMetaData = new[] { new SqlMetaData("Id", SqlDbType.Int) }; 
           
            foreach (var item in vehicles)
            {
                // Create a new record, i.e. row.
                var record = new SqlDataRecord(myMetaData);
                // Set the 1st colunm, i.e., position 0 with the correcponding value:
                record.SetInt32(0, item.VehicleId);

                // Add the new row to the table rows array:
                updatedOrderIdsParameter.Add(record);
            }

            var queryResult = _conn.Query("vehicle.usp_getVehiclePhotoByVehicleID", new TableValueParameter("@vehicleIds", "vehicle.VehicleIdTableType", updatedOrderIdsParameter), commandType: CommandType.StoredProcedure);

            return VehilcePhotoList(queryResult);
        }

        public IEnumerable<VehiclePhoto> GetAll()
        {
            try
            {
                var queryResult = _conn.Query("vehicle.usp_getAllVehiclePhotos", commandType: CommandType.StoredProcedure);
                return VehilcePhotoList(queryResult);
            }
            finally
            {
                _conn.Close();
            }
        }

        public VehiclePhoto GetById(int id)
        {
            throw new NotImplementedException();
        }

        public void Delete(VehiclePhoto entity)
        {
            throw new NotImplementedException();
        }

        private static IEnumerable<VehiclePhoto> VehilcePhotoList(IEnumerable<dynamic> queryResult)
        {
            var vehicleIdList = new List<int>();
            var vehilcePhotoList = new List<VehiclePhoto>();

            foreach (var item in queryResult)
            {
                vehicleIdList.Add(item.VehicleID);
            }
            vehicleIdList = vehicleIdList.Distinct().ToList();
            foreach (var item in vehicleIdList)
            {
                var vehiclePhoto = new VehiclePhoto { VehicleId = item };
                var filepath = new List<string>();
                foreach (var data in queryResult)
                {
                    if (data.VehicleID == item)
                    {
                        filepath.Add(data.ActualPhoto);
                    }
                }
                vehiclePhoto.PhotoThumbnail = filepath;
                vehiclePhoto.ActualPhoto = filepath;
                vehilcePhotoList.Add(vehiclePhoto);
            }
            return vehilcePhotoList;
        }

       
    }
}
