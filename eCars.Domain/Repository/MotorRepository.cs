﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Dapper;
using eCars.Domain.Entities;
using eCars.Domain.Interfaces;

namespace eCars.Domain.Repository
{
    public class MotorRepository : RepositoryBase, IMotorRepository
    {
        private readonly IDbConnection _conn;
        public MotorRepository()
        {
            _conn = new SqlConnection {ConnectionString = ConnectionString};
        }

        public void Insert(Motor entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(Motor entity)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Motor> GetMotors()
        {
            try
            {
                return _conn.Query<Motor>("vehicle.usp_getMotors", commandType: CommandType.StoredProcedure);
            }
            finally
            {
                _conn.Close();
            }
        }

        public IEnumerable<Motor> GetMotorsByMotorId(int motorId)
        {
            throw new NotImplementedException();
        }

        public Motor GetById(int id)
        {
            throw new NotImplementedException();
        }
    }
}
