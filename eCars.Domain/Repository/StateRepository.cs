﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using eCars.Domain.Interfaces;
using eCars.Domain.Entities;

namespace eCars.Domain.Repository
{
    public class StateRepository : RepositoryBase, IStateRepository
    {
         private readonly IDbConnection _conn;
        public StateRepository()
        {
            _conn = new SqlConnection {ConnectionString = ConnectionString};
        }

        public void Insert(States entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(States entity)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<States> GetStates()
        {
            try
            {
                return _conn.Query<States>("vehicle.usp_getVehicleStates", commandType: CommandType.StoredProcedure);
            }
            finally
            {
                _conn.Close();
            }
        }

        public IEnumerable<States> GetStateById(int stateId)
        {
            throw new NotImplementedException();
        }

        public States GetById(int id)
        {
            throw new NotImplementedException();
        }
    }
}
