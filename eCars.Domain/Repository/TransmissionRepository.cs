﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Dapper;
using eCars.Domain.Entities;
using eCars.Domain.Interfaces;

namespace eCars.Domain.Repository
{
    public class TransmissionRepository : RepositoryBase, ITransmissionRepository
    {
        private readonly IDbConnection _conn;
        public TransmissionRepository()
        {
            _conn = new SqlConnection {ConnectionString = ConnectionString};
        }

        public void Insert(Transmission entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(Transmission entity)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Transmission> GetTransmission()
        {
            try
            {
                return _conn.Query<Transmission>("vehicle.usp_getTransmission", commandType: CommandType.StoredProcedure);
            }
            finally
            {
                _conn.Close();  
            }
        }

        public IEnumerable<Transmission> GetTransmissionById(int transmissionId)
        {
            throw new NotImplementedException();
        }

        public Transmission GetById(int id)
        {
            throw new NotImplementedException();
        }
    }
}
