﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using eCars.Domain.Entities;
using eCars.Domain.Interfaces;

namespace eCars.Domain.Repository
{
    public class CustomerRepository : RepositoryBase, ICustomerRepository
    {

         private readonly IDbConnection _conn;
         public CustomerRepository()
        {
            _conn = new SqlConnection { ConnectionString = ConnectionString };
        }

        public void Insert(Customer entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(Customer entity)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Customer> GetCondition()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Customer> GetConditionById(int conditionId)
        {
            throw new NotImplementedException();
        }

        public Customer GetById(string id)
        {
            return _conn.Query<Customer>("vehicle.usp_getCustomerByUserID", new {UserId = id},
                commandType: CommandType.StoredProcedure).FirstOrDefault();
        }


        public Customer GetCustomerByVehicleId(int id)
        {
            return _conn.Query<Customer>("vehicle.usp_getCustomerByVehicleId", new {vehicleId = id},
                commandType: CommandType.StoredProcedure).FirstOrDefault();
        }


        public IEnumerable<Customer> GetDealers()
        {
            return _conn.Query<Customer>("vehicle_uspGetDealers", commandType: CommandType.StoredProcedure);
          
        }

        public Customer GetDealerById()
        {
            throw new NotImplementedException();
        }
    }
}
