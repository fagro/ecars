﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Dapper;
using eCars.Domain.Entities;
using eCars.Domain.Interfaces;

namespace eCars.Domain.Repository
{
    public class DriveTrainRepository: RepositoryBase, IDriveTrainRepository
    {
        private readonly IDbConnection _conn;
        public DriveTrainRepository()
        {
            _conn = new SqlConnection {ConnectionString = ConnectionString};
        }
        public void Insert(DriveTrain entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(DriveTrain entity)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<DriveTrain> GetDriveTrain()
        {
            try
            {
                return _conn.Query<DriveTrain>("vehicle.usp_getDriveTrain", commandType: CommandType.StoredProcedure);
            }
            finally
            {
                _conn.Close();
            }
        }

        public IEnumerable<DriveTrain> GetDriveTrainBy(int driveTrainId)
        {
            throw new NotImplementedException();
        }

        public DriveTrain GetById(int id)
        {
            throw new NotImplementedException();
        }
    }
}
