﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Dapper;
using eCars.Domain.Entities;
using eCars.Domain.Interfaces;

namespace eCars.Domain.Repository
{
    public class ConditionRepository : RepositoryBase, IConditionRepository
    {
        private readonly IDbConnection _conn;
        public ConditionRepository()
        {
            _conn = new SqlConnection {ConnectionString = ConnectionString};
        }
        public void Insert(Condition entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(Condition entity)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Condition> GetCondition()
        {
            try
            {
                return _conn.Query<Condition>("vehicle.usp_getCondition", commandType: CommandType.StoredProcedure);
            }
            finally
            {
                _conn.Close();
            }
        }

        public IEnumerable<Condition> GetConditionById(int conditionId)
        {
            throw new NotImplementedException();
        }

        public Condition GetById(int id)
        {
            throw new NotImplementedException();
        }
    }
}
