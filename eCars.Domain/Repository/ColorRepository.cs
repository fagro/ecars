﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using eCars.Domain.Entities;
using eCars.Domain.Interfaces;
using Dapper;

namespace eCars.Domain.Repository
{
    public class ColorRepository : RepositoryBase, IColorRepository
    {

         private readonly IDbConnection _conn;
         public ColorRepository()
        {
            _conn = new SqlConnection {ConnectionString = ConnectionString};
        }
        public void Insert(Color entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(Color entity)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Color> GetColors()
        {
            return _conn.Query<Color>("vehicle.usp_getColors");
        }

        public IEnumerable<Color> GetColorsById(int colorId)
        {
            throw new NotImplementedException();
        }

        public Color GetById(int id)
        {
            throw new NotImplementedException();
        }
    }
}
