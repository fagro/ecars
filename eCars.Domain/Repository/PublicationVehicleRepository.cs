﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Dapper;
using eCars.Domain.Entities;
using eCars.Domain.Interfaces;

namespace eCars.Domain.Repository
{
    public class PublicationVehicleRepository : RepositoryBase, IPublicationRepository
    {
        private readonly IDbConnection _conn;
        public PublicationVehicleRepository()
        {
            _conn = new SqlConnection {ConnectionString = ConnectionString};
        }

        public void Insert(Publication entity)
        {
            try
            {
                var parameters = new DynamicParameters();
                parameters.Add("@publicationID", entity.PublicationId);
                parameters.Add("@customerID", entity.CustomerId);
                parameters.Add("@vehicleID", entity.VehicleId);
                parameters.Add("@active", entity.Active);
                parameters.Add("@expired", entity.Expired);

                _conn.Execute("vehicle.usp_insertUpdatePublication", parameters, commandType: CommandType.StoredProcedure);
            }
            finally
            {
                _conn.Close();
            }
            
        }

        public void Delete(Publication entity)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Publication> SearchFor(string userId)
        {
            try
            {
                return _conn.Query<Publication>("vehicle.usp_GetPublicationByCusID", new { UserId = userId }, commandType: CommandType.StoredProcedure);
            }
            finally
            {
                _conn.Close();
            }
            
        }

        public IEnumerable<Publication> GetAll()
        {
            throw new NotImplementedException();
        }

        

        public Publication GetById(int id)
        {
            throw new NotImplementedException();
        }


        public IEnumerable<Publication> GetByClientId(int id)
        {
            throw new NotImplementedException();
        }


        public int Publish(int publicationId, string userId)
        {
            //vehicle.usp_ActivatePublication
            var result = 0;
            try
            {
                var parameters = new DynamicParameters();
                parameters.Add("@id", publicationId);
                parameters.Add("@userId", userId);

               result =  _conn.Execute("vehicle.usp_ActivatePublication", parameters, commandType: CommandType.StoredProcedure);
            }
            finally
            {
                _conn.Close();
            }
            return result;
        }
    }
}
