using System.Web.Optimization;

[assembly: WebActivatorEx.PostApplicationStartMethod(typeof(eCars.WebUI.App_Start.BootstrapBundleConfig), "RegisterBundles")]

namespace eCars.WebUI.App_Start
{
	public class BootstrapBundleConfig
	{
		public static void RegisterBundles()
		{
			// Add @Styles.Render("~/Content/bootstrap") in the <head/> of your _Layout.cshtml view
			// For Bootstrap theme add @Styles.Render("~/Content/bootstrap-theme") in the <head/> of your _Layout.cshtml view
			// Add @Scripts.Render("~/bundles/bootstrap") after jQuery in your _Layout.cshtml view
			// When <compilation debug="true" />, MVC4 will render the full readable version. When set to <compilation debug="false" />, the minified version will be rendered automatically
			BundleTable.Bundles.Add(new ScriptBundle("~/bundles/bootstrapJs").Include("~/Scripts/bootstrap.js",
                                                                                      "~/Scripts/bootstrap-tagsinput.min.js"));
            BundleTable.Bundles.Add(new StyleBundle("~/Content/bootstrapCss").Include("~/Content/bootstrap/css/bootstrap.css",
                                                                                      "~/Content/bootsrap/css/bootstrap-tagsinput.css"));
			BundleTable.Bundles.Add(new StyleBundle("~/Content/bootstrap-themeCss").Include("~/Content/bootstrap-theme.css"));
		}
	}
}
