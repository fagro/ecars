﻿$(document).ready(function () {
    var makesAndModels = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        limit: 10,
        prefetch: {
            url: 'GetMakesAndModels/',
            filter: function (list) {
                return $.map(list, function (makeAndModels) { return { name: makeAndModels }; });
            }
        }
    });


    makesAndModels.initialize();
    $('#prefetch .typeahead').typeahead(null,
    {
        name: 'makesAndModels',
        displayKey: 'name',
        source: makesAndModels.ttAdapter()
    });

    $('#searchMainTxt').keypress(function (e) {
        if (e.which == 13) {
            searchByMakeAndModel();
        }
    });

    $('#vehicelSearchBtn').click(function () {
        searchByMakeAndModel();
    });


    $('#yearTo').change(function () {
        $.ajax({
            Type: 'GET',
            url: 'FilterByYear',
            data: {
                     yearFrom: parseInt($('#yearFrom').val()),
                     yearTo: parseInt($('#yearTo').val()),
                     makeAndModel: $('#searchMainTxt').val()
            },
            success: function (data) {

                $('#carProductGrid').html(data);
                $("a[data-gal^='prettyPhoto']").prettyPhoto({ hook: 'data-gal' });
            },
            error: function (ex) {
                alert(ex.message);
            }
        });
    });

    $('#StateId').change(function () {
        $.ajax({
            Type: 'GET',
            url: 'FilterByState',
            data: { stateId: parseInt($('#StateId').val()), makeAndModel: $('#searchMainTxt').val() },
            success: function (data) {

                $('#carProductGrid').html(data);
                $("a[data-gal^='prettyPhoto']").prettyPhoto({ hook: 'data-gal' });
            },
            error: function (ex) {
                alert(ex.message);
            }
        });
    });

    $('#PriceTo').keypress(function (e) {

        if (e.which == 13) {
            $.ajax({
                Type: 'GET',
                url: 'FilterByPrice',
                data: { priceFrom: parseFloat($('#PriceFrom').val()), priceTo: parseFloat($('#PriceTo').val()) },
                success: function (data) {

                    $('#carProductGrid').html(data);
                    $("a[data-gal^='prettyPhoto']").prettyPhoto({ hook: 'data-gal' });
                },
                error: function (ex) {
                    alert(ex.message);
                }
            });
        }
    });

    $('#FuelId').change(function () {
        $.ajax({
            Type: 'GET',
            url: 'FilterByFuelId',
            data: { fuelId: parseInt($('#FuelId').val()), makeAndModel: $('#searchMainTxt').val() },
            success: function (data) {

                $('#carProductGrid').html(data);
                $("a[data-gal^='prettyPhoto']").prettyPhoto({ hook: 'data-gal' });
            },
            error: function (ex) {
                alert(ex.message);
            }
        });
    });

    $('#VehicleConditionId').change(function () {
        $.ajax({
            Type: 'GET',
            url: 'FilterByConditionId',
            data: { conditionId: parseInt($('#VehicleConditionId').val()), makeAndModel: $('#searchMainTxt').val() },
            success: function (data) {

                $('#carProductGrid').html(data);
                $("a[data-gal^='prettyPhoto']").prettyPhoto({ hook: 'data-gal' });
            },
            error: function (ex) {
                alert(ex.message);
            }
        });
    });

    $('#searchSortBy').change(function () {

        if ($('#searchSortBy').val() !== "") {
            $.ajax({
                Type: 'GET',
                url: 'OrderBy',
                data: { orderById: $('#searchSortBy').val() },
                success: function (data) {

                    $('#carProductGrid').html(data);
                    $("a[data-gal^='prettyPhoto']").prettyPhoto({ hook: 'data-gal' });
                },
                error: function (ex) {
                    alert(ex.message);
                }
            });
        }
    });
});


function searchByMakeAndModel() {
    $.ajax({
        type: 'GET',
        url: 'SearchFor',
        data: { makeAndModel: $('#searchMainTxt').val() },
        success: function (data) {
            $('#carProductGrid').html(data);
            $("a[data-gal^='prettyPhoto']").prettyPhoto({ hook: 'data-gal' });

        },
        error: function (ex) {
            alert(ex.message);
        }
    });
}