﻿$(document).ready(function() {

    $('#homeEmailbtn').click(function(e) {
        e.preventDefault();

        if (validate()) {
            
            $.ajax({
                type: 'Post',
                url: 'Home/SendEmail',
                
                data: {
                    name: $('#nametxt').val(),
                    email: $('#emailtxt').val(),
                    phone: $('#phonetxt').val(),
                    subject: $('#subjecttxt').val(),
                    message: $('#messagetxt').val()
                },

                success: function(data) {
                    alert('Mensaje enviado con exito');
                },
                error: function(e) {
                    alert(e.message);
                }
            });
        } else {
            alert('Complete todos los campos');
        }
    });

    function validate() {

        if ($('#nametxt').val() == "" | $('#nametxt').val() == null) return false;
        if ($('#emailtxt').val() == "" | $('#emailtxt').val() == null) return false;
        if ($('#phonetxt').val() == "" | $('#phonetxt').val() == null) return false;
        if ($('#subjecttxt').val() == "" | $('#subjecttxt').val() == null) return false;
        if ($('#messagetxt').val() == "" | $('#messagetxt').val() == null) return false;

        return true;
    }

});
