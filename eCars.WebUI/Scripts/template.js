
	$(document).ready (function () {
  
		/* BOXED LAYOUT DEMO SUPPORT
    ==================================================
      - Only useful in the demo
      - No need for this if you set boxed layout in sass
		================================================== */
    adaptHeader();
    
    $(window).resize(function() {
      adaptHeader();
		});
    
	    /* Send email */
    //$("#emailForm").ajaxSubmit({ url: 'SendEmail', type: 'post' });


		/* PRETTY PHOTO
		================================================== */
		$("a[data-gal^='prettyPhoto']").prettyPhoto({hook:'data-gal'});

		/* FOOTER INSTAGRAM FEED
		================================================== */

		jQuery.fn.spectragram.accessData = {
		    accessToken: '1462187957.1fbe532.5f8f2e3a13404c119cfb33b3030b29bf',
		    clientID: '1fbe532bfc7348f08f2091c0f0bb38ac'
		};
		$('#footer-flickr-stream').spectragram('getRecentTagged', {
		    query: 'enautos',
		    clientID: '1fbe532bfc7348f08f2091c0f0bb38ac',
		    max: 9,
		    wrapEachWith: '<li></li>'
		});


	    
    
	/* FOOTER TWITTER FEED
	================================================== */

    var config5 = {
        "id": '508340237584453632',
        "domId": '',
        "maxTweets": 1,
        "enableLinks": true,
        "showUser": true,
        "showTime": false,
        "dateFunction": '',
        "showRetweet": false,
        "customCallback": handleTweets,
        "showInteraction": false
    };

    function handleTweets(tweets) {
        var x = tweets.length;
        var n = 0;
        var element = document.getElementById('footer-tweets');
        var html = '<ul class="tweetList">';
        while (n < x) {
            html += '<li>' + tweets[n] + '</li>';
            n++;
        }
        html += '<i class="fa fa-twitter"></i></ul>';
        element.innerHTML = html;
    }
    twitterFetcher.fetch(config5);

		
		/* CUSTOM TOGGLE
		================================================== */
    $("*[data-custom-toggle]").each(function() {
      $(this).click(function() {
      
        var target = $(this).attr('data-target');
        var tclass = $(this).attr('data-class');
        var tmode = $(this).attr('data-custom-toggle');

        // Toggle classes if supplied
        if(tclass) {
          $(this).toggleClass(tclass);
          $(target).toggleClass(tclass);
        }
        
        switch(tmode) {
        
          case "slide":
             $(target).slideToggle('slow');
          break;
          
          case "visibility":
             $(target).toggle('slow');
          break;
          
          // Extend more options here...
        }

      });
    });
    
		/* SIDE-LINKS TOGGLE
		================================================== */
		$( ".side-links .toggle-children" ).click(function() {
			$(this).toggleClass('open').next().toggle( "slow");

		});
		
		
	});
  
  $(window).load(function(){
       $("*[data-remove-class]").each(function() {
      var str = $(this).attr("data-remove-class");
      $(this).removeClass(str);
    })
})
    /* CLASS REMOVER
		================================================== */

	
	/* FUNCTIONS
	================================================== */
	
	function changeView(view) {

		switch (view) {
			case "grid":
			$('.product-grid').removeClass('listview');
			$('.product-grid > li').removeClass('col-xs-12 col-sm-12 col-md-12').addClass('col-xs-6 col-sm-4 col-md-4');
			break;
			case "list":
			$('.product-grid').addClass('listview');
			$('.product-grid > li').removeClass('col-xs-6 col-sm-4 col-md-4').addClass('col-xs-12 col-sm-12 col-md-12');
			break;
		}

	}
	
    function adaptHeader() {
      var wrapperW = $('#page-wrapper').width();
      $('#header').width(wrapperW);
    }
    

