﻿$(document).ready(function () {

    $(".calculator-loan").accrue({

        // set the output element
        response_output_div: ".results",

        // set the response format
        response_basic:
          '<p><strong>Pago Mensual:</strong><br>RD$%payment_amount%</p>' +
          '<p><strong>Número de pagos:</strong><br>%num_payments%</p>' +
          '<p><strong>Pago Total:</strong><br>RD$%total_payments%</p>' +
          '<p><strong>Intereses Totales:</strong><br>RD$%total_interest%</p>',

        // set error text for when one of the fields is empty or invalid.
        error_text: "Por favor llene todos los campos."

    });

    $('#tagsDetail').tagsinput({
        itemText: 'label'
    });

});
