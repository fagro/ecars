﻿

$(document).ready(function () {

 

    $('#Make').change(function () {
        $('#MakeAndModelId').empty();
        $.ajax({
            type: 'GET',
            url: 'GetModels',
            contentType: "application/json; charset=utf-8",
            data: { makes: $('#Make').val() },

            success: function (models) {

                $.each(models, function (i, model) {
                    $('#MakeAndModelId').append('<option value="' + model.Value + '">' + model.Text + '</option>');
                });
            },

            error: function (ex) {
                alert('Failed to retrieve model' + ex);
            }
        });
        return false;
    });

    $('#carphototab').click(function () {
        $('#selectVehicle').empty();
        $('#selectVehicle').append('<option value ="0">Seleccione un auto</option>');
        $.ajax({
            type: 'POST',
            url: 'GetClientVehiclesByCustomer',
            contentType: "application/json; charset=utf-8",

            success: function (autos) {
                $.each(autos, function (i, auto) {
                    $('#selectVehicle').append('<option value= "' + auto.Value + '">' + auto.Text + '</option>');
                });
            },
            error: function (ex) {
                alert('Failed to retrive data' + ex);
            }
        });
    });

    $('#selectVehicle').change(function () {
        $('#objectContext').val($('#selectVehicle').val());
        console.log($('#objectContext').val());
    });

    $('#publishBtn').click(function (ex) {
        ex.preventDefault();
        console.log($('#idPublication').val());
        $.ajax({
            type: 'GET',
            url: '/Publish/PublishVehicle',
            contentType: "application/json; charset=utf-8",
            data: { publishId : $('#idPublication').val() },
            success: function (data) {
                alert(JSON.stringify(data));
            },
            error: function (ex) {
                alert('Oops no se pudo publicar :(' + ex);
            }
        });
    });

    var jqXHRData;
    //initSimpleFileUpload();
    initAutoFileUpload();

    initFileUploadWithCheckingSize();

    $("#hl-start-upload").on('click', function () {
        if (jqXHRData) {
            jqXHRData.submit();
        }
        return false;
    });

    $("#hl-start-upload-with-size").on('click', function () {
        console.log('click ok');
        if (jqXHRData) {
            var isStartUpload = true;
            var uploadFile = jqXHRData.files[0];

            if (!(/\.(gif|jpg|jpeg|tiff|png)$/i).test(uploadFile.name)) {
                alert('Debe Seleccionar una imagen');
                isStartUpload = false;
            } else if (uploadFile.size > 5000000) { // 5mb
                alert('Por favor suba una imagen de menor tamaño, el tamaño m&aacute;ximo es 5MB');
                isStartUpload = false;
            }
            if (isStartUpload) {
                jqXHRData.submit();
            }
        }
        return false;
    });


    function initSimpleFileUpload() {
        'use strict';

        $('#fu-my-simple-upload').fileupload({
            url: '/File/UploadFile',
            dataType: 'json',
            add: function (e, data) {
                jqXHRData = data
            },
            done: function (event, data) {
                if (data.result.isUploaded) {

                }
                else {

                }
                alert(data.result.message);
            },
            fail: function (event, data) {
                if (data.files[0].error) {
                    alert(data.files[0].error);
                }
            }
        });
    }

    function initAutoFileUpload() {
        'use strict';

        $('#fu-my-auto-upload').fileupload({
            autoUpload: true,
            url: 'File/UploadFile',
            dataType: 'json',
            add: function (e, data) {
                var jqXHR = data.submit()
                    .success(function (data, textStatus, jqXHR) {
                        if (data.isUploaded) {

                        }
                        else {

                        }
                        alert(data.message);
                    })
                    .error(function (data, textStatus, errorThrown) {
                        if (typeof (data) != 'undefined' || typeof (textStatus) != 'undefined' || typeof (errorThrown) != 'undefined') {
                            alert(textStatus + errorThrown + data);
                        }
                    });
            },
            fail: function (event, data) {
                if (data.files[0].error) {
                    alert(data.files[0].error);
                }
            }
        });
    }

    function initFileUploadWithCheckingSize() {
        'use strict';

        $('#fu-my-simple-upload-with-size').fileupload({

            url: '/File/UploadFile',
            dataType: 'json',
            add: function (e, data) {
                if (parseInt($('#objectContext').val()) == null) {
                    return false;
                }
                jqXHRData = data.formData = {
                    vehicleID: $('#objectContext').val()
                }
                jqXHRData = data;

            },
            done: function (event, data) {
                if (data.result.isUploaded) {

                }
                else {

                }
                alert(data.result.message);
            },
            fail: function (event, data) {
                if (data.files[0].error) {
                    alert(data.files[0].error);
                }
            }
        });
    }

});