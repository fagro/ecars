﻿using System.Runtime.InteropServices;
using eCars.WebUI.Models;
using Dapper;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Data;

namespace eCars.WebUI.Identity
{

    public class UserStore : IUserStore<User>, IUserLoginStore<User>, IUserPasswordStore<User>, IUserSecurityStampStore<User>
    {
        private readonly string connectionString;

        public UserStore(string connectionString)
        {
            if (string.IsNullOrWhiteSpace(connectionString))
                throw new ArgumentNullException("connectionString");

            this.connectionString = connectionString;
        }

        public UserStore()
        {
            this.connectionString = ConfigurationManager.ConnectionStrings["eCarsDB"].ConnectionString;

        }

        public void Dispose()
        {

        }

        #region IUserStore

        public virtual Task CreateAsync(User user)
        {
            if (user == null)
                throw new ArgumentNullException("user");

            return Task.Factory.StartNew(() =>
            {
                user.UserId = Guid.NewGuid();
                using (SqlConnection connection = new SqlConnection(connectionString)) 
                {
                    var parameters = new DynamicParameters();
                    parameters.Add("@userId", user.UserId);
                    parameters.Add("@username", user.UserName);
                    parameters.Add("@password", user.PasswordHash);
                    parameters.Add("@company", user.Company);
                    parameters.Add("@name", user.Name);
                    parameters.Add("@lastname", user.Lastname);
                    parameters.Add("@birthday", user.Birthday.ToString("yyyy/MM/dd"));
                    parameters.Add("@phone", user.Phone);
                    parameters.Add("@mobile", user.Mobile);
                    parameters.Add("@email", user.Email);
                    parameters.Add("@isDealer", user.IsDealer);
                    parameters.Add("@Address", user.Address);
                    parameters.Add("@SecurityStamp", user.SecurityStamp);
                    parameters.Add("@ConfirmationToken", user.ConfirmationToken);
                    connection.Execute("vehicle.usp_UpsertUser", parameters, commandType: CommandType.StoredProcedure);
                }
            });
        }

        public virtual Task ConfirmEmailAsync(string userId, string confirmationToken)
        {
            if (string.IsNullOrEmpty(confirmationToken)) throw new ArgumentNullException("confirmationToken");

            return Task.Factory.StartNew(() =>
            {
                using (var connection = new SqlConnection(connectionString))
                {
                    var user = connection.Query<User>("select * from app.customer where confirmationToken = @confirmationToken",
                        confirmationToken);
                }
            });
        }

        public virtual Task DeleteAsync(User user)
        {
            if (user == null)
                throw new ArgumentNullException("user");

            return Task.Factory.StartNew(() =>
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                    connection.Execute("delete from app.customer where UserId = @userId", new { user.UserId });
            });
        }

        public virtual Task<User> FindByIdAsync(string userId)
        {
            if (string.IsNullOrWhiteSpace(userId))
                throw new ArgumentNullException("userId");

            Guid parsedUserId;
            if (!Guid.TryParse(userId, out parsedUserId))
                throw new ArgumentOutOfRangeException("userId", string.Format("'{0}' is not a valid GUID.", new { userId }));

            return Task.Factory.StartNew(() =>
            {
                using (SqlConnection connection = new SqlConnection(connectionString)) 
                {
                    var result  = connection.Query<User>("select * from app.customer where UserId = @userId", new { @userId = parsedUserId }).SingleOrDefault();
                    return result;
                }
                    
            });
        }

        public virtual Task<User> FindByNameAsync(string userName)
        {
            if (string.IsNullOrWhiteSpace(userName))
                throw new ArgumentNullException("userName");

            return Task.Factory.StartNew(() =>
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                    return connection.Query<User>("select * from app.customer where lower(UserName) = lower(@username)", new { userName }).SingleOrDefault();
            });
        }

        public virtual Task UpdateAsync(User user)
        {
            if (user == null)
                throw new ArgumentNullException("user");

            return Task.Factory.StartNew(() =>
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    var parameters = new DynamicParameters();
                    parameters.Add("@userId", user.UserId);
                    parameters.Add("@username", user.UserName);
                    parameters.Add("@password", user.PasswordHash);
                    parameters.Add("@company", user.Company);
                    parameters.Add("@name", user.Name);
                    parameters.Add("@lastname", user.Lastname);
                    parameters.Add("@birthday", user.Birthday.ToString("yyyy/MM/dd"));
                    parameters.Add("@phone", user.Phone);
                    parameters.Add("@mobile", user.Mobile);
                    parameters.Add("@email", user.Email);
                    parameters.Add("@isDealer", user.IsDealer);
                    parameters.Add("@Address", user.Address);
                    parameters.Add("@SecurityStamp", user.SecurityStamp);
                    parameters.Add("@ConfirmationToken", user.ConfirmationToken);
                    parameters.Add("@IsConfirmed", user.IsConfirmed);
                    connection.Execute("vehicle.usp_UpsertUser", parameters, commandType: CommandType.StoredProcedure);
                }
                    
            });
        }
        #endregion

        #region IUserLoginStore
        public virtual Task AddLoginAsync(User user, UserLoginInfo login)
        {
            if (user == null)
                throw new ArgumentNullException("user");

            if (login == null)
                throw new ArgumentNullException("login");

            return Task.Factory.StartNew(() =>
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                    connection.Execute("insert into ExternalLogins(ExternalLoginId, UserId, LoginProvider, ProviderKey) values(@externalLoginId, @userId, @loginProvider, @providerKey)",
                        new { externalLoginId = Guid.NewGuid(), userId = user.UserId, loginProvider = login.LoginProvider, providerKey = login.ProviderKey });
            });
        }

        public virtual Task<User> FindAsync(UserLoginInfo login)
        {
            if (login == null)
                throw new ArgumentNullException("login");

            return Task.Factory.StartNew(() =>
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                    return connection.Query<User>("select u.* from Users u inner join ExternalLogins l on l.UserId = u.UserId where l.LoginProvider = @loginProvider and l.ProviderKey = @providerKey",
                        login).SingleOrDefault();
            });
        }

        public virtual Task<IList<UserLoginInfo>> GetLoginsAsync(User user)
        {
            if (user == null)
                throw new ArgumentNullException("user");

            return Task.Factory.StartNew(() =>
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                    return (IList<UserLoginInfo>)connection.Query<UserLoginInfo>("select LoginProvider, ProviderKey from ExternalLogins where UserId = @userId", new { user.UserId }).ToList();
            });
        }

        public virtual Task RemoveLoginAsync(User user, UserLoginInfo login)
        {
            if (user == null)
                throw new ArgumentNullException("user");

            if (login == null)
                throw new ArgumentNullException("login");

            return Task.Factory.StartNew(() =>
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                    connection.Execute("delete from ExternalLogins where UserId = @userId and LoginProvider = @loginProvider and ProviderKey = @providerKey",
                        new { user.UserId, login.LoginProvider, login.ProviderKey });
            });
        }
        #endregion

        #region IUserPasswordStore
        public virtual Task<string> GetPasswordHashAsync(User user)
        {
            if (user == null)
                throw new ArgumentNullException("user");

            return Task.FromResult(user.PasswordHash);
        }

        public virtual Task<bool> HasPasswordAsync(User user)
        {
            return Task.FromResult(!string.IsNullOrEmpty(user.PasswordHash));
        }

        public virtual Task SetPasswordHashAsync(User user, string passwordHash)
        {
            if (user == null)
                throw new ArgumentNullException("user");

            user.PasswordHash = passwordHash;

            return Task.FromResult(0);
        }

        #endregion

        #region IUserSecurityStampStore
        public virtual Task<string> GetSecurityStampAsync(User user)
        {
            if (user == null)
                throw new ArgumentNullException("user");

            return Task.FromResult(user.SecurityStamp);
        }

        public virtual Task SetSecurityStampAsync(User user, string stamp)
        {
            if (user == null)
                throw new ArgumentNullException("user");

            user.SecurityStamp = stamp;
           


            return Task.FromResult(0);
        }

        #endregion
      
    }

}