﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Web.Configuration;
using ImageProcessor;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using ImageProcessor.Processors;
using ImageProcessor.Imaging.Formats;
using ImageProcessor.Imaging;
namespace eCars.WebUI.Service
{
    public class ImageService
    {
        public static IEnumerable<string> GetFiles(string path, string[] searchPatterns, SearchOption searchOption = SearchOption.AllDirectories)
        {
            return searchPatterns.AsParallel()
                   .SelectMany(searchPattern =>
                          Directory.EnumerateFiles(path, searchPattern, searchOption));
        }

        public static void SetWaterMark(IEnumerable<string> path)
        {
            foreach (var item in path)
            {
                AddWatermark(item);
            }
        }

        public static void AddWatermark(string path)
        {
            var watermarkPath = HttpRuntime.AppDomainAppPath + WebConfigurationManager.AppSettings["WatermarkLogo"];
            var watermark = new WebImage(watermarkPath);
            new WebImage(path).AddImageWatermark(watermark).Save();
        }

        public static void ProcessImage(string filePath)
        {
            //ResizeImage(file);
            SetWaterMark(new List<string> { filePath });
        }

        public static void ResizeImage(string filepath)
        {
            byte[] photoBytes = File.ReadAllBytes(filepath);
            ISupportedImageFormat format = new JpegFormat { Quality = 70 };
            var resizeLayer = new ResizeLayer(new Size(1024, 768), ResizeMode.Max);

            using (MemoryStream inStream = new MemoryStream(photoBytes))
            {
                using (MemoryStream outStream = new MemoryStream())
                {
                    // Initialize the ImageFactory using the overload to preserve EXIF metadata.
                    using (ImageFactory imageFactory = new ImageFactory(preserveExifData: true))
                    {
                        // Load, resize, set the format and quality and save an image.
                        imageFactory.Load(inStream)
                                    .Resize(resizeLayer)
                                    .Format(format)
                                    .Save(outStream);
                    }
                    // Do something with the stream.
                    Image.FromStream(outStream).Save(filepath);
                }
            }
        }
    }
}
