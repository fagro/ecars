﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using eCars.WebUI.Models;
using Postal;
using System.Configuration;
using Microsoft.AspNet.Identity;

namespace eCars.WebUI.Service
{
    public class MailService : IIdentityMessageService
    {
        public Task SendRegistrationEmail(string userId, string to, string username, string confirmationToken, string options = "")
        {
            var emailFrom = ConfigurationManager.AppSettings["emailFrom"];
            var emailFromPassword = ConfigurationManager.AppSettings["emailFromPassword"];
            dynamic email = new Email("Welcome");
            email.To = to;
            email.Subject = "Bienvenid@ a EnAutos.com";
            email.From = emailFrom;
            email.Username = username;
            email.ConfirmationToken = confirmationToken;
            email.CallbackUrl = options;
            email.UserId = userId;
 
           var  emailViewRenderer = new EmailViewRenderer(ViewEngines.Engines);
           var emailParser = new EmailParser(emailViewRenderer);

           var client2 = new SmtpClient("smtp.gmail.com", 587)
            {
                Credentials = new NetworkCredential(emailFrom, emailFromPassword),
                EnableSsl = true
            };
            var rawMessage = emailViewRenderer.Render(email);
            var mailMessage = emailParser.Parse(rawMessage, email);
            client2.SendMailAsync(mailMessage);
            return Task.FromResult(0);

        }


        public Task SendContactEmail(string name, string clientEmail, string phone, string suject, string message )
        {
            var emailFrom = ConfigurationManager.AppSettings["emailFrom"];
            var emailFromPassword = ConfigurationManager.AppSettings["emailFromPassword"];
            dynamic email = new Email("ContactEnAutos");
            email.To = "Miauto@enautos.com";
            email.Subject = suject;
            email.Phone = phone;
            email.Name = name;
            email.ClientEmail = clientEmail;
            email.Message = message;
            email.From = "Miauto@enautos.coms";
            

            var emailViewRenderer = new EmailViewRenderer(ViewEngines.Engines);
            var emailParser = new EmailParser(emailViewRenderer);

            var client2 = new SmtpClient("smtp.gmail.com", 587)
            {
                Credentials = new NetworkCredential(emailFrom, emailFromPassword),
                EnableSsl = true
            };
            var rawMessage = emailViewRenderer.Render(email);
            var mailMessage = emailParser.Parse(rawMessage, email);
            client2.SendMailAsync(mailMessage);
            return Task.FromResult(0);

        }

        public Task SendAsync(IdentityMessage message)
        {
            throw new NotImplementedException();
        }
    }
}
