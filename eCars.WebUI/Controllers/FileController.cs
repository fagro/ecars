﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using eCars.WebUI.Service;
using eCars.Domain.Entities;
using eCars.Domain.Repository;
using eCars.Domain.Interfaces;
namespace eCars.WebUI.Controllers
{
    /// <summary>
    /// File Controller
    /// </summary>
    public class FileController : Controller
    {
        #region Actions

        readonly string _photoFolder = WebConfigurationManager.AppSettings["photosFolder"];
        private readonly IVehiclePhotoRepository _repoVehiclePhoto;
        //static readonly string Thumbnail = WebConfigurationManager.AppSettings["thumbsFolder"];

        public FileController()
        {
            _repoVehiclePhoto = new VehiclePhotoRepository();
        }

        public virtual ActionResult UploadFile()
        {
            var myFile = Request.Files["MyFile"];
            var vehicleId = Request.Form["vehicleID"];
            var isUploaded = false;
            var message = "Oh! Ocurrio un error subiendo la imagen";

            if (myFile == null || myFile.ContentLength == 0) return Json(new { isUploaded, message }, "text/html");
            if (!ValidateImageFileType(myFile)) return Json(new { isUploaded, message = "Formato invalido" }, "text/html");


            var pathForSaving = Server.MapPath(_photoFolder + vehicleId);
            if (!CreateFolderIfNeeded(pathForSaving)) return Json(new { isUploaded, message }, "text/html");
            try
            {
                myFile.SaveAs(Path.Combine(pathForSaving, myFile.FileName));
                //ImageService.ReziseImage(Path.Combine(pathForSaving, myFile.FileName), pathForSaving);
                ImageService.ProcessImage(Path.Combine(pathForSaving, myFile.FileName));
                //System.IO.File.Delete(Path.Combine(pathForSaving, myFile.FileName));
                SaveVehicleImage(Path.Combine(_photoFolder+vehicleId, myFile.FileName), vehicleId);
                isUploaded = true;
                message = "Imagen subida con éxito!";
            }
            catch (Exception ex)
            {
                message = string.Format("Oh! ocurrio un error: {0}", ex.Message);
            }
            return Json(new { isUploaded, message }, "text/html");
        }

        private void SaveVehicleImage(string filePath, string vehicleId)
        {
            var list = new List<string>();
            list.Add(filePath);
            var photo = new VehiclePhoto
            {
                  ActualPhoto = list,
                  VehicleId = int.Parse(vehicleId),
                  PhotoThumbnail = list
             };
            try
            {
                _repoVehiclePhoto.Insert(photo);
            }
            catch (Exception)
            {
                throw;
            }


        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Creates the folder if needed.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <returns></returns>
        private static bool CreateFolderIfNeeded(string path)
        {
            bool result = true;
            if (!Directory.Exists(path))
            {
                try
                {
                    Directory.CreateDirectory(path);
                    //Directory.CreateDirectory(path +@"\"+ Thumbnail);
                }
                catch (Exception)
                {
                    /*TODO: You must process this exception.*/
                    result = false;
                }
            }
            return result;
        }

        private static bool ValidateImageFileType(HttpPostedFileBase postedFile)
        {
            return postedFile.ContentType.ToLower() == "image/jpg" ||
                    postedFile.ContentType.ToLower() == "image/jpeg" ||
                    postedFile.ContentType.ToLower() == "image/pjpeg" ||
                    postedFile.ContentType.ToLower() == "image/x-png" ||
                    postedFile.ContentType.ToLower() == "image/png";
        }

        #endregion
    }
}