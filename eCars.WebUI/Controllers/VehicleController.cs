﻿using System;
using System.Collections.Generic;
using Microsoft.AspNet.Identity;
using System.Linq;
using System.Web.Mvc;
using eCars.Domain.Entities;
using eCars.Domain.Interfaces;
using eCars.Domain.Repository;
using eCars.WebUI.Models;
using PagedList;
using Postal;
using System.Configuration;

namespace eCars.WebUI.Controllers
{
    [Authorize]
    public class VehicleController : Controller
    {
        #region Instance Variables
        private IVehicleRepository _repoVehicle;
        private readonly IMakeAndModelRepository _repoMakeAndModel;
        private readonly IMotorRepository _repoMotor;
        private readonly ITransmissionRepository _repoTransmission;
        private readonly IFuelRepository _repoFuel;
        private readonly IDriveTrainRepository _repoDriveTrain;
        private readonly IConditionRepository _repoCondition;
        private readonly IPublicationRepository _repoPublication;
        private readonly ICurrencyRepository _repoCurrency;
        private readonly IVehiclePhotoRepository _repoVehiclePhoto;
        private readonly IStateRepository _repoState;
        private readonly ICustomerRepository _repoCustomer;
        private readonly IVehiclePlanRepository _repoPlan;
        private readonly IColorRepository _repoColor;
        #endregion

        #region Constructor

        public VehicleController()
        {
            _repoVehicle = new VehicleRepository();
            _repoMakeAndModel = new MakeAndModelRepository();
            _repoMotor = new MotorRepository();
            _repoTransmission = new TransmissionRepository();
            _repoFuel = new FuelRepository();
            _repoDriveTrain = new DriveTrainRepository();
            _repoCondition = new ConditionRepository();
            _repoCurrency = new CurrencyRepository();
            _repoPublication = new PublicationVehicleRepository();
            _repoVehiclePhoto = new VehiclePhotoRepository();
            _repoState = new StateRepository();
            _repoCustomer = new CustomerRepository();
            _repoPlan = new VehiclePlanRepository();
            _repoColor = new ColorRepository();

        }

        #endregion

        #region Actions


        [AllowAnonymous]
        public ActionResult Search()
        {
            var vehicles = _repoVehicle.GetAll();
            var vehiclePhotos = _repoVehiclePhoto.GetAll();
            var model = new VehicleViewModel();
            var list = model.BindVehicleAndPhotos(vehicles, vehiclePhotos);
            Session["Data"] = null;
            var result = GeneratePagination(list, 1);
            return View(result);
        }


        [AllowAnonymous]
        public ActionResult Detail(int id)
        {
            if (id == 0) return RedirectToAction("Search");

            var vehicle = _repoVehicle.GetById(id);
            var customer = _repoCustomer.GetCustomerByVehicleId(vehicle.VehicleId);
            var customerModel = new CustomerViewModel();
            var list = new List<Vehicle>();
            var model = new VehicleViewModel();
            list.Add(vehicle);
            var photos = _repoVehiclePhoto.SearchFor(list);
            var vehicleModel = model.BindVehicleAndPhotos(list, photos);
            var result = vehicleModel.FirstOrDefault();
            var data = new List<Customer>();
            data.Add(customer);
            result.CustomerViewModel = customerModel.Map(data).FirstOrDefault();
            return View(result);
        }


        [AllowAnonymous]
        public ActionResult Similiar(int id)
        {
            var vehicleList = _repoVehicle.GetSimilarVehicles(id);
            if (!vehicleList.Any()) return PartialView(new List<VehicleViewModel>());
            var model = new VehicleViewModel();
            var photos = _repoVehiclePhoto.SearchFor(vehicleList);
            var result = model.BindVehicleAndPhotos(vehicleList, photos);
            return PartialView(result);
        }

        #endregion

        #region Vehicle CRUD

        [HttpGet]
        public ActionResult Create()
        {
            var model = InitializeVehicleModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult Create(VehicleViewModel vehicleViewModel)
        {
            var isValidCreate = CreateValidation();
            if (isValidCreate) return View(InitializeVehicleModel());
            try
            {
                // TODO: Add insert logic here
                var vehicle = vehicleViewModel.MapVehicle(vehicleViewModel);
                var vehicleId = _repoVehicle.Insert(vehicle);
                var userId = User.Identity.GetUserId();
                var user = _repoCustomer.GetById(userId);

                //Automatic assign publication
                _repoPublication.Insert(new Publication { Active = false, CustomerId = user.User_Id_performance, VehicleId = vehicleId, PublicationId = 0, Expired = false });
                return RedirectToAction("Create");
            }
            catch
            {
                return View();
            }
        }

        #endregion

        #region Ajax Calls

        [HttpGet]
        [AllowAnonymous]
        public JsonResult GetModels(string makes)
        {
            var result = _repoMakeAndModel.GetModelByMake(makes).ToSelectListItems(makes);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [AllowAnonymous]
        public JsonResult GetMakesAndModels()
        {
            var result = _repoMakeAndModel.GetMakesAndModels();
            var data = from a in result select a.Make + " " + a.Model;
            return Json(data.ToArray(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetClientVehiclesByCustomer()
        {
            var userId = User.Identity.GetUserId();
            var result = _repoVehicle.GetVehiclesByCustomerId(userId);
            var data = from a in result select new { Text = a.Make + " " + a.Model + " " + a.Year, Value = a.VehicleId };
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Filters

        [AllowAnonymous]
        public ActionResult SearchFor(string makeAndModel)
        {
            var list = SearchVehicleByMakeAndModel(makeAndModel);
            Session["Data"] = list;
            var result = GeneratePagination(list, 1);
            return PartialView("ProductGrid", result);
        }

        [AllowAnonymous]
        public ActionResult FilterByYear(short yearFrom, short yearTo, string makeAndModel = "")
        {
            if (Session["Data"] == null)
            {
                var vehicles = _repoVehicle.GetAll();
                var vehiclePhotos = _repoVehiclePhoto.GetAll();
                var model = new VehicleViewModel();
                var list = model.BindVehicleAndPhotos(vehicles, vehiclePhotos);
                list = list.Where(a => (a.Year >= yearFrom && a.Year <= yearTo));
                Session["Data"] = list;
                var result = GeneratePagination(list, 1);
                return PartialView("ProductGrid", result);
            }
            else
            {
                var list = Session["Data"] as IEnumerable<VehicleViewModel>;
                var filteredList = list.Where(a => (a.Year >= yearFrom && a.Year <= yearTo));
                Session["Data"] = filteredList;
                var result = GeneratePagination(filteredList, 1);

                return PartialView("ProductGrid", result);
            }
        }

        [AllowAnonymous]
        public ActionResult FilterByState(short stateId, string makeAndModel = "")
        {
            if (Session["Data"] != null)
            {
                var data = Session["Data"] as IEnumerable<VehicleViewModel>;
                var filteredList = data.Where(a => a.StateId == stateId);
                Session["Data"] = filteredList;
                var result = GeneratePagination(filteredList, 1);
                return PartialView("ProductGrid", result);
            }
            else
            {
                var vehicles = _repoVehicle.GetAll();
                var vehiclePhotos = _repoVehiclePhoto.GetAll();
                var model = new VehicleViewModel();
                var list = model.BindVehicleAndPhotos(vehicles, vehiclePhotos);
                list = list.Where(a => a.StateId == stateId);
                Session["Data"] = list;
                var result = GeneratePagination(list, 1);
                return PartialView("ProductGrid", result);

            }
        }
        
        [AllowAnonymous]
        public ActionResult FilterByFuelId(short fuelId, string makeAndModel = "")
        {
            if (Session["Data"] != null)
            {
                var data = Session["Data"] as IEnumerable<VehicleViewModel>;
                var filteredList = data.Where(a => a.FuelId == fuelId);
                Session["Data"] = filteredList;
                var result = GeneratePagination(filteredList, 1);
                return PartialView("ProductGrid", result);
            }
            else
            {
                var vehicles = _repoVehicle.GetAll();
                var vehiclePhotos = _repoVehiclePhoto.GetAll();
                var model = new VehicleViewModel();
                var list = model.BindVehicleAndPhotos(vehicles, vehiclePhotos);
                list = list.Where(a => a.FuelId == fuelId);
                Session["Data"] = list;
                var result = GeneratePagination(list, 1);
                return PartialView("ProductGrid", result);

            }
        }

        [AllowAnonymous]
        public ActionResult FilterByConditionId(short conditionId, string makeAndModel = "")
        {
            if (Session["Data"] != null)
            {
                var data = Session["Data"] as IEnumerable<VehicleViewModel>;
                var filteredList = data.Where(a => a.VehicleConditionId == conditionId);
                Session["Data"] = filteredList;
                var result = GeneratePagination(filteredList, 1);
                return PartialView("ProductGrid", result);
            }
            else
            {
                var vehicles = _repoVehicle.GetAll();
                var vehiclePhotos = _repoVehiclePhoto.GetAll();
                var model = new VehicleViewModel();
                var list = model.BindVehicleAndPhotos(vehicles, vehiclePhotos);
                list = list.Where(a => a.VehicleConditionId == conditionId);
                Session["Data"] = list;
                var result = GeneratePagination(list, 1);
                return PartialView("ProductGrid", result);

            }
        }

        [AllowAnonymous]
        public ActionResult FilterByPrice(decimal priceFrom, decimal priceTo)
        {
            if (Session["Data"] != null)
            {
                var data = Session["Data"] as IEnumerable<VehicleViewModel>;
                var filteredList = data.Where(a => a.Price >= priceFrom && a.Price <= priceTo);
                Session["Data"] = filteredList;
                var result = GeneratePagination(filteredList, 1);
                return PartialView("ProductGrid", result);
            }
            else
            {
                var vehicles = _repoVehicle.GetAll();
                var vehiclePhotos = _repoVehiclePhoto.GetAll();
                var model = new VehicleViewModel();
                var list = model.BindVehicleAndPhotos(vehicles, vehiclePhotos);
                list = list.Where(a => a.Price >= priceFrom && a.Price <= priceTo);
                Session["Data"] = list;
                var result = GeneratePagination(list, 1);
                return PartialView("ProductGrid", result);

            }
        }

        [AllowAnonymous]
        public ActionResult OrderBy(short orderById)
        {
            var tasa = decimal.Parse(ConfigurationManager.AppSettings["tasa"]);
            
            if (Session["Data"] != null)
            {
                var listResult = Session["Data"] as IEnumerable<VehicleViewModel>;
                if (orderById == (int)OrderByValue.Year)
                {
                    var filteredList = listResult.OrderBy(a => a.Year);
                    Session["Data"] = filteredList;
                    var result = GeneratePagination(filteredList, 1);
                    return PartialView("ProductGrid", result);
                }
                else
                {
                    var filteredList = listResult.OrderBy(a => ((a.CurrencyId >= 2 ) ? a.Price * tasa : a.Price));
                    Session["Data"] = filteredList;
                    var result = GeneratePagination(filteredList, 1);
                    return PartialView("ProductGrid", result);
                }
            }
            else
            {
                var vehicles = _repoVehicle.GetAll();
                var vehiclePhotos = _repoVehiclePhoto.GetAll();
                var model = new VehicleViewModel();
                var listResult = model.BindVehicleAndPhotos(vehicles, vehiclePhotos);

                if (orderById == (int)OrderByValue.Year)
                {
                    var filteredList = listResult.OrderBy(a => a.Year);
                    Session["Data"] = filteredList;
                    var result = GeneratePagination(filteredList, 1);
                    return PartialView("ProductGrid", result);
                }
                else
                {
                    var filteredList = listResult.OrderBy(a => ((a.CurrencyId >= 2) ? a.Price * tasa : a.Price));
                    Session["Data"] = filteredList;
                    var result = GeneratePagination(filteredList, 1);
                    return PartialView("ProductGrid", result);
                }
            }
        }

        private IEnumerable<VehicleViewModel> SearchVehicleByMakeAndModel(string makeAndModel)
        {
            if (string.IsNullOrEmpty(makeAndModel)) return null;

            var vehicles = _repoVehicle.SearchFor(makeAndModel);
            var vehiclePhotos = _repoVehiclePhoto.SearchFor(vehicles);
            var model = new VehicleViewModel();
            var list = model.BindVehicleAndPhotos(vehicles, vehiclePhotos);
            return list;
        }

        #endregion

        #region Utilities

        private VehicleViewModel InitializeVehicleModel()
        {
            var model = new VehicleViewModel
            {
                Makes = _repoMakeAndModel.GetMakes().ToSelectListItems(),
                Motors = _repoMotor.GetMotors().ToSelectListItems(),
                Transmission = _repoTransmission.GetTransmission().ToSelectListItems(),
                Fuels = _repoFuel.GetFuels().ToSelectListItems(),
                DriveTrains = _repoDriveTrain.GetDriveTrain().ToSelectListItems(),
                Conditions = _repoCondition.GetCondition().ToSelectListItems(),
                Currencies = _repoCurrency.GetCurrency().ToSelectListItems(),
                States = _repoState.GetStates().ToSelectListItems(),
                Colors = _repoColor.GetColors().ToSelectListItems()
            };

            return model;
        }

        private enum OrderByValue
        {
            Year = 1,
            Price = 2
        }

        private Plan GetUserPlan()
        {
            var customer = _repoCustomer.GetById(User.Identity.GetUserId());
            return _repoPlan.GetPlanByUser(customer.User_Id_performance);
        }

        private CustomPlan GetCustomPlanByUser() 
        {
            var customer = _repoCustomer.GetById(User.Identity.GetUserId());
            return _repoPlan.GetCustomPlanByUser(customer.User_Id_performance);
        }

        private bool CreateValidation()
        {
            var plan = GetUserPlan();
            var customPlan = GetCustomPlanByUser();
            var userVehicles = _repoVehicle.GetVehiclesByCustomerId(User.Identity.GetUserId());

            //validations
            if (plan == null)
            {
                ModelState.AddModelError("", "Debe Seleccionar un plan para crear vehículos");
                {
                    return false;
                }
            }

            if (ModelState != null && !ModelState.IsValid)
            {
                ModelState.AddModelError("", "Verifique los campos");
                {
                    return false;
                }
            }

            if (customPlan != null)
            {
                if (userVehicles.Count() > customPlan.NumberVehiclesAllowed)
                {
                    ModelState.AddModelError("", "Excedió el límite de vehículos por plan.");
                    {
                        return false;
                    }
                }
            }
            else if (userVehicles.Count() > plan.NumberVehiclesAllowed)
            {
                ModelState.AddModelError("", "Excedió el límite de vehículos por plan.");
                {
                    return false;
                }
            }
            
            return true;
        }

        private IPagedList<VehicleViewModel> GeneratePagination(object o, int? page) 
        {
            int pageSize = 5;
            int pageNumber = (page ?? 1);
            var data = o as IEnumerable<VehicleViewModel>;
            return data.ToPagedList(pageNumber, pageSize);
        }

        #endregion
    }
}