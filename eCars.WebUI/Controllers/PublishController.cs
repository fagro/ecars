﻿using System;
using System.Collections.Generic;
using Microsoft.AspNet.Identity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using eCars.Domain.Interfaces;
using eCars.Domain.Repository;
using eCars.WebUI.Models;
namespace eCars.WebUI.Controllers
{
    public class PublishController : Controller
    {
        private readonly IPublicationRepository _repoPublication;
        private PublicationViewModel _publicationView;

        public PublishController()
        {
            _publicationView = new PublicationViewModel();
            _repoPublication = new PublicationVehicleRepository();
        }

        // GET: Publish
        public ActionResult Index()
        {
            return View();
        }

        public PartialViewResult GetPublications()
        {
            var userId = User.Identity.GetUserId();
            var data = _repoPublication.SearchFor(userId);

            var model = _publicationView.Map(data);
            return PartialView(model);
        }

        [HttpGet]
        public JsonResult PublishVehicle(int publishId)
        {
            var userId = User.Identity.GetUserId();
            var result = _repoPublication.Publish(publishId, userId);
            if (result != 0)
            {
                return Json("Publicado", JsonRequestBehavior.AllowGet);    
            }
            return Json("No se pudo publicar", JsonRequestBehavior.AllowGet);
        }

    }
}