﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using eCars.Domain.Interfaces;
using eCars.Domain.Repository;
using Microsoft.AspNet.Identity;
using eCars.WebUI.Models;

namespace eCars.WebUI.Controllers
{
    [Authorize]
    public class CustomerController : Controller
    {
        #region Instance Variables
        private readonly ICustomerRepository _repoCustomer;
        private readonly IVehiclePlanRepository _repoPlan;
        #endregion

        public CustomerController()
        {
            _repoCustomer = new CustomerRepository();
            _repoPlan = new VehiclePlanRepository();
        }

        [AllowAnonymous]
        public ActionResult Dealers()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult GetDealers()
        {
            var dealers = _repoCustomer.GetDealers();
            var data = new CustomerViewModel().Map(dealers);
            return PartialView(data);
        }

        [AllowAnonymous]
        public ActionResult Plans()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult GetPlan()
        {
            var plans = _repoPlan.GetPlans();
            return PartialView("PlansPartial", plans);
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult ChoosePlan(string planId)
        {
            if (int.Parse(planId) > 1)
            {
                return RedirectToAction("ChoosePaymentPlan");
            }
            var userId = User.Identity.GetUserId();
            if (string.IsNullOrEmpty(userId)) return View("UserNotFound");
            
            var done = _repoPlan.Update(int.Parse(planId), userId);
            if (int.Parse(planId) > 1) RedirectToAction("");

            if (done == -1)
            {
                return View("BasicPlanActivated");
            }
            else
            {
                return Json("El plan no pudo ser seleccionado, por favor llamenos.", JsonRequestBehavior.AllowGet);
            }

        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult ChoosePaymentPlan()
        {
            return View();
        }


    }
}