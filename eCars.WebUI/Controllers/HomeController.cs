﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using eCars.Domain.Entities;
using eCars.Domain.Interfaces;
using eCars.Domain.Repository;
using eCars.WebUI.Models;
using eCars.WebUI.Service;

namespace eCars.WebUI.Controllers
{
    public class HomeController : Controller
    {
        #region Instance Variable

        private IVehicleRepository _repoVehicle;
        private MailService _mailService;
        private readonly IVehiclePhotoRepository _repoVehiclePhoto;

        public HomeController()
        {
            _repoVehicle = new VehicleRepository();
            _repoVehiclePhoto = new VehiclePhotoRepository();
            _mailService = new MailService();
        }
        #endregion
        // GET: Home
        public ActionResult Index()
        {
            ViewBag.Title = "EnAutos.com :: Inicio";
            return View();
        }

        public ActionResult AboutUs()
        {
            ViewBag.Title = "EnAutos.com :: Sobre Nosotros";
            return View();
        }

        public FileResult Robotstxt()
        {
            var path = Server.MapPath("~/Views/Shared/Robots.txt");
            var file = File(path, "application/text");
            return file;
        }

        public ActionResult Highlights()
        {
            var vehicleList = _repoVehicle.GetVehicleHighlights();
            var model = new VehicleViewModel();
            var photos = _repoVehiclePhoto.SearchFor(vehicleList);
            var result = model.BindVehicleAndPhotos(vehicleList, photos);
            return PartialView(result);
        }
        [HttpPost]
        public ActionResult SendEmail(string name, string email, string phone, string subject, string message)
        {
            var req = Request["subjecttxt"];
            Task.Factory.StartNew(() =>
            {
                _mailService.SendContactEmail(name, email, phone, subject, message);
            });
            return Json("Enviado", JsonRequestBehavior.AllowGet);
        }

        public ActionResult PrivacyPolicy()
        {
            return View();
        }
    }
}