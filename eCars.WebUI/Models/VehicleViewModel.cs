﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using eCars.Domain.Entities;
using eCars.Domain.Repository;
using Microsoft.Ajax.Utilities;

namespace eCars.WebUI.Models
{
    public class VehicleViewModel
    {
        #region Properties
        public int VehicleId { get; set; }

        [Required]
        public string MakeAndModelId { get; set; }

        [Display(Name = "Marca")]
        public string Make { get; set; }

        [Required(ErrorMessage = "Modelo es requerido.")]
        [StringLength(100, ErrorMessage = "El {0} debe de tener al menos {2} caracteres.", MinimumLength = 2)]
        [Display(Name = "Modelo")]
        public string Model { get; set; }

        [Required(ErrorMessage = "Motor es requerido.")]
        [Display(Name = "Motor")]
        public short MotorId { get; set; }

        public string MotorName { get; set; }

        [Required(ErrorMessage = "Transmisión es requerido.")]
        [Display(Name = "Transmisión")]
        public short TransmissionId { get; set; }
        public string TransmissionName { get; set; }
        
        [Required(ErrorMessage = "Combustible es requerido.")]
        [Display(Name = "Combustible")]
        public short FuelId { get; set; }

        public string FuelName { get; set; }

        [Required(ErrorMessage = "Tracción es requerido.")]
        [Display(Name = "Tracción")]
        public short DriveTrainId { get; set; }
        public string DriveTrainName { get; set; }

        [Required(ErrorMessage = "Condición es requerido.")]
        [Display(Name = "Condición")]
        public short VehicleConditionId { get; set; }
        public string VehicleConditionName { get; set; }
        
        [Required(ErrorMessage = "Cilindros es requerido.")]
        [Display(Name = "Cilindros")]
        public short Cilinders { get; set; }


        [Required(ErrorMessage = "Precio es requerido.")]
        [Range(20000, 9999999, ErrorMessage = "Debe agregar un precio")]
        [Display(Name = "Precio")]
        public decimal Price { get; set; }

        [Required(ErrorMessage = "Pasajeros es requerido.")]
        [Display(Name = "Pasajeros")]
        public short TotalSeating { get; set; }

        [Required(ErrorMessage = "Color Exterior es requerido.")]
        [Range(1, 15, ErrorMessage = "Debe agregar un Color")]
        [Display(Name = "Color Exterior")]
        public short ColorExteriorId { get; set; }

        public string ColorInteriorValue { get; set; }
        public string ColorExteriorValue { get; set; }

        [Required(ErrorMessage = "Color Interior es requerido.")]
        [Range(1, 15, ErrorMessage = "Debe agregar un Color")]
        [Display(Name = "Color Interior")]
        public short ColorInteriorId { get; set; }


        [Required(ErrorMessage = "Año es requerido.")]
        [Display(Name = "Año")]
        [Range( 1970, 2015, ErrorMessage = "Debe agregar un año valido")]
        public short Year { get; set; }

        
        [Display(Name = "Accesorios")]
        public string Tags { get; set; }
        
        [Display(Name = "Descripción")]
        public string Description { get; set; }
        
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }

        [Required(ErrorMessage = "Moneda es requerido.")]
        [Display(Name= "Moneda")]
        public short CurrencyId { get; set; }
        
        //Vehicle Object property
        public VehiclePhoto VehiclePhotos { get; set; }
        [Display(Name = "Lugar")]
        public short StateId { get; set; }
        public string StateName { get; set; }

        public CustomerViewModel CustomerViewModel { get; set; }

        #endregion

        #region Selecte List
        //Selected list
        public IEnumerable<SelectListItem> Makes { get; set; }
        public IEnumerable<SelectListItem> Models { get; set; }
        public IEnumerable<SelectListItem> Years { get; set; }
        public IEnumerable<SelectListItem> Motors { get; set; }
        public IEnumerable<SelectListItem> Transmission { get; set; }
        public IEnumerable<SelectListItem> Fuels { get; set; }
        public IEnumerable<SelectListItem> DriveTrains { get; set; }
        public IEnumerable<SelectListItem> Conditions { get; set; }
        public IEnumerable<SelectListItem> Currencies { get; set; }
        public IEnumerable<SelectListItem> States { get; set; }
        public IEnumerable<SelectListItem> Colors { get; set; }
        #endregion

        #region Mappers And Binders

        public Vehicle MapVehicle(VehicleViewModel viewModel)
        {
            var vehicle = new Vehicle
            {
                VehicleId = 0,
                MakeAndModelId = short.Parse(viewModel.MakeAndModelId),
                Cilinders = viewModel.Cilinders,
                ColorExteriorId =  viewModel.ColorExteriorId,
                ColorInteriorId = viewModel.ColorInteriorId,
                ColorExteriorValue = viewModel.ColorExteriorValue,
                ColorInteriorValue = viewModel.ColorInteriorValue,
                Created = DateTime.Now,
                DriveTrainId = viewModel.DriveTrainId,
                FuelId = FuelId,
                Year = viewModel.Year,
                TotalSeating = viewModel.TotalSeating,
                VehicleConditionId = viewModel.VehicleConditionId,
                TransmissionId = viewModel.TransmissionId,
                MotorId = viewModel.MotorId,
                Price = viewModel.Price,
                Tags = viewModel.Tags,
                CurrencyId = viewModel.CurrencyId,
                Description = viewModel.Description,
                StateId = viewModel.StateId,
                StateName = viewModel.StateName
            };
            return vehicle;
        }

        public IEnumerable<VehicleViewModel> BindVehicleAndPhotos(IEnumerable<Vehicle> vehicles, IEnumerable<VehiclePhoto> vehiclePhotoList)
        {
            #region Load Filters
            //Objects
            var statesModel = new StateRepository();
            var conditionsModel = new ConditionRepository();
            var fuelModel = new FuelRepository();
           
            //Select List.
            var states = statesModel.GetStates().ToSelectListItems();
            var conditions = conditionsModel.GetCondition().ToSelectListItems();
            var yearFilters = Enumerable.Range(1950, (DateTime.Now.Year - 1950) + 2).ToList().ToSelectListItems();
            var fuels = fuelModel.GetFuels().ToSelectListItems();
            #endregion

            #region Binder
            var model = new List<VehicleViewModel>();
            foreach (var vehicle in vehicles)
            {
                foreach (var photo in vehiclePhotoList)
                {
                    if (vehicle.VehicleId == photo.VehicleId)
                    {
                        model.Add(new VehicleViewModel
                        {
                            Make = vehicle.Make,
                            Model = vehicle.Model,
                            Price = vehicle.Price,
                            Year = vehicle.Year,
                            MotorId = vehicle.MotorId,
                            MotorName = vehicle.MotorName,
                            Cilinders = vehicle.Cilinders,
                            TransmissionId = vehicle.TransmissionId,
                            TransmissionName = vehicle.TransmissionName,
                            DriveTrainId = vehicle.DriveTrainId,
                            DriveTrainName = vehicle.DriveTrainName,
                            FuelId = vehicle.FuelId,
                            FuelName = vehicle.FuelName,
                            Fuels = fuels,
                            VehicleConditionId = vehicle.VehicleConditionId,
                            VehicleConditionName = vehicle.VehicleConditionName,
                            Conditions = conditions,
                            ColorExteriorId = vehicle.ColorExteriorId,
                            ColorInteriorId = vehicle.ColorInteriorId,
                            ColorExteriorValue = vehicle.ColorExteriorValue,
                            ColorInteriorValue =  vehicle.ColorInteriorValue,
                            TotalSeating = vehicle.TotalSeating,
                            Tags = vehicle.Tags,
                            VehicleId = vehicle.VehicleId,
                            Description = vehicle.Description,
                            VehiclePhotos = photo,
                            CurrencyId =  vehicle.CurrencyId,
                            Years = yearFilters,
                            StateId = vehicle.StateId,
                            States = states,
                            StateName = vehicle.StateName,
                            Created = vehicle.Created
                        });
                    }
                }
            }
            #endregion

            return model;
        }


        #endregion
    }
}