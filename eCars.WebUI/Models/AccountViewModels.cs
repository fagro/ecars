﻿using System.ComponentModel.DataAnnotations;
using System;

namespace eCars.WebUI.Models
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }
    }

    public class ManageUserViewModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Current password")]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "El {0} debe de tener al menos {2} caracteres.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword", ErrorMessage = "La contraseña y la confirmación de contraseña no son iguales.")]
        public string ConfirmPassword { get; set; }
    }

    public class LoginViewModel
    {
        [Required]
        [Display(Name = "Usuario")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Contraseña")]
        public string Password { get; set; }

        [Display(Name = "Recordar?")]
        public bool RememberMe { get; set; }
    }

    public class RegisterViewModel
    {
        [Required]
        [StringLength(60, ErrorMessage = "El {0} debe de tener al menos {2} caracteres.", MinimumLength = 5)]
        [Display(Name = "Usuario")]
        public string UserName { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "El {0} debe de tener al menos {2} caracteres.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Contraseña")]
        public string Password { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Confirmar Contraseña")]
        [Compare("Password", ErrorMessage = "La contraseña y la confirmacion de contraseña no son iguales.")]
        public string ConfirmPassword { get; set; }

        
        [Display(Name = "Compañia")]
        public string Company { get; set; }

        [Required(ErrorMessage = "Nombre es requerido.")]
        [Display(Name = "Nombre")]
        public string Name { get; set; }

        
        [Required(ErrorMessage = "Apellido es requerido.")]
        [Display(Name = "Apellido")]
        public string Lastname { get; set; }

        //[Required]
        //[Display(Name = "Identificación")]
        //public string Identification { get; set; }

        public DateTime Birthday { get; set; }

        [Required(ErrorMessage = "Teléfono es requerido.")]
        [Display(Name = "Teléfono")]
        [DataType(DataType.PhoneNumber)]
        public string Phone { get; set; }

        [Required(ErrorMessage = "Movíl es requerido.")]
        [Display(Name = "Movíl")]
        [DataType(DataType.PhoneNumber)]
        public string Mobile { get; set; }


        [Display(Name = "Correo Electrónico")]
        [Required(ErrorMessage = "Correo Electrónico es requerido.")]
        [EmailAddress(ErrorMessage = "Email no valido.")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Required]
        [Display(Name = "Dirección")]
        public string Address { get; set; }

        [Display(Name = "Eres Dealer/Concesionario")]
        public bool IsDealer { get; set; }

    }
}
