﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using eCars.Domain.Entities;


namespace eCars.WebUI.Models
{
    public class CustomerViewModel
    {
        public Guid UserId { get; set; }
        [Display(Name="Compañia")]
        public string CompanyName { get; set; }
        [Display(Name = "Nombre")]
        public string Name { get; set; }
        [Display(Name = "Apellido")]
        public string Lastname { get; set; }
        [Display(Name = "Identificación")]
        public string Identification { get; set; }
        [Display(Name = "Nacimiento")]
        public DateTime Birthday { get; set; }
       
        [Display(Name = "Teléfono")]
        [DisplayFormat(DataFormatString = "{0:###-###-####}")]
        public long Phone { get; set; }
        [Display(Name = "Movíl")]
        [DisplayFormat(DataFormatString = "{0:###-###-####}")]
        public long Mobile { get; set; }
        [Display(Name = "Correo")]
        public string Email { get; set; }
        public bool IsDealer { get; set; }
        [Display(Name="Dirección")]
        public string Address { get; set; }

        [Display(Name = "Usuario")]
        public string UserName { get; set; }
        [Display(Name = "Contraseña")]
        public string PasswordHash { get; set; }
        public bool ConfirmEmail { get; set; }
        public string SecurityStamp { get; set; }


        public IEnumerable<CustomerViewModel> Map(IEnumerable<Customer> customer)
        {
            var list = new List<CustomerViewModel>();
            foreach (var item in customer)
            {
                list.Add(new CustomerViewModel 
                {
                    Address = item.Address,
                    Birthday = item.Birthday,
                    CompanyName = item.CompanyName,
                    ConfirmEmail = item.isConfirmed,
                    Email = item.Email,
                    Identification = item.Identification,
                    IsDealer = item.IsDealer,
                    Lastname = item.Lastname,
                    Phone = item.Phone,
                    Mobile = item.Mobile,
                    Name = item.Name
                });
            }
          
            return list;
        }
    }
}