﻿using System;
using Microsoft.AspNet.Identity;

namespace eCars.WebUI.Models
{
    public class User : IUser
    {
        public Guid UserId { get; set; }
        public string UserName { get; set; }
        public string PasswordHash { get; set; }
        public string SecurityStamp { get; set; }
        public string Company { get; set; }
        public string Name { get; set; }
        public string Lastname { get; set; }
        public string Identification { get; set; }
        public DateTime Birthday { get; set; }
        public string Phone { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public bool IsDealer { get; set; }
        public string Address { get; set; }
        public string ConfirmationToken { get; set; }
        public bool IsConfirmed { get; set; }
        


        public string Id
        {
            get { return UserId.ToString() ; }
        }
    }
}