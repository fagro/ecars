﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using eCars.Domain.Entities;
using System.ComponentModel.DataAnnotations;
namespace eCars.WebUI.Models
{
    public class PublicationViewModel
    {

        public int PublicationId { get; set; }
        public int CustomerId { get; set; }
        public int VehicleId { get; set; }
        [Display(Name = "Marca")]
        public string Make { get; set; }
        [Display(Name = "Modelo")]
        public string Model { get; set; }
        [Display(Name = "Año")]
        public short Year { get; set; }

        public decimal Price { get; set; }
        public short CurrencyId { get; set; }

        public int PlanId { get; set; }
        [Display(Name = "Activo")]
        public bool Active { get; set; }
        public string PhotoThumbnail { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }


        public IEnumerable<PublicationViewModel> Map(IEnumerable<Publication> publications)
        {
            var list = new List<PublicationViewModel>() ;
            foreach (var item in publications)
            {
                list.Add(new PublicationViewModel
                {
                    PublicationId = item.PublicationId,
                    CustomerId = item.CustomerId,
                    VehicleId = item.VehicleId,
                    Make = item.Make,
                    Model = item.Model,
                    Active = item.Active,
                    Modified = item.Modified,
                    Created = item.Created,
                    Price = item.Price,
                    CurrencyId = item.CurrencyId,
                    Year = item.Year,
                    PhotoThumbnail = item.PhotoThumbnail,
                    PlanId = item.PlanId    
                });
            }
        
            return list;
        }
        
    }
}