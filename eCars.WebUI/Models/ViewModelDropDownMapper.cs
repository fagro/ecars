﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using eCars.Domain.Entities;

namespace eCars.WebUI.Models
{
    public static class ViewModelDropDownMapper
    {
        public static IEnumerable<SelectListItem> ToSelectListItems(this IEnumerable<MakeAndModel> makes)
        {
            return makes.Select(make => new SelectListItem
                        {
                            Text = make.Make,
                            Value = make.Make
                        });
        }

        public static IEnumerable<SelectListItem> ToSelectListItems(this IEnumerable<MakeAndModel> models, string selectedName)
        {
            return models.Select(model => new SelectListItem
                        {
                            Selected = (model.Make == selectedName),
                            Text = model.Model,
                            Value = model.MakeAndModelId.ToString()
                        });
        }

        public static IEnumerable<SelectListItem> ToSelectListItems(this IEnumerable<Motor> motors)
        {
            return motors.Select(model => new SelectListItem
            {
                Text = model.Name,
                Value = model.MotorId.ToString()
            });
        }


        public static IEnumerable<SelectListItem> ToSelectListItems(this IEnumerable<Transmission> transmissions)
        {
            return transmissions.OrderBy(transmission => transmission.Name).Select(transmission => new SelectListItem
            {
                Text = transmission.Name,
                Value = transmission.TransmissionId.ToString()
            });
        }


        public static IEnumerable<SelectListItem> ToSelectListItems(this IEnumerable<Fuel> fuels)
        {
            return fuels.Select(fuel => new SelectListItem
            {
                Text = fuel.Name,
                Value = fuel.FuelId.ToString()
            });
        }

        public static IEnumerable<SelectListItem> ToSelectListItems(this IEnumerable<DriveTrain> driveTrains)
        {
            return driveTrains.Select(driveTrain => new SelectListItem
            {
                Text = driveTrain.Name,
                Value = driveTrain.DriveTrainId.ToString()
            });
        }

        public static IEnumerable<SelectListItem> ToSelectListItems(this IEnumerable<Condition> conditions)
        {
            return conditions.Select(condition => new SelectListItem
            {
                Text = condition.Name,
                Value = condition.ConditionId.ToString()
            });
        }

        public static IEnumerable<SelectListItem> ToSelectListItems(this IEnumerable<Currency> currencies)
        {
            return currencies.Select(currency => new SelectListItem
            {
                Text = currency.Name,
                Value = currency.CurrencyId.ToString()
            });
        }

        public static IEnumerable<SelectListItem> ToSelectListItems(this IEnumerable<int> years)
        {
            return years.OrderByDescending(a => a).Select(year => new SelectListItem
            {
                Text = year.ToString(),
                Value = year.ToString()
            });
        }
        public static IEnumerable<SelectListItem> ToSelectListItems(this IEnumerable<States> states)
        {
            return states.OrderBy(a => a.Value).Select(state => new SelectListItem
            {
                Text = state.Name,
                Value = state.StateId.ToString()
            });
        }

        public static IEnumerable<SelectListItem> ToSelectListItems(this IEnumerable<Color> colors)
        {
            return colors.OrderBy(c => c.Name).Select(color => new SelectListItem
            {
                Text = color.Name,
                Value = color.ColorId.ToString()
            });
        }
    }
}